import { useState, useEffect } from 'react';

import Layout from '../components/shared/Layout';
import BannerSection from '../components/Home/BannerSection';
import NewsVideoSection from './../components/Home/NewsVideoSection';
import DesktopLinkSection from '../components/Home/DesktopLinkSection';
import ChartImageSliderSection from '../components/Home/ChartImageSliderSection';
import AdditionalSection from '../components/Home/AdditionalSection';

import Hidden from '@material-ui/core/Hidden';
import { fetchData } from '../api/helper';

const Index = (props) => {
  const { resData } = props;
  const [reWidth, setReWidth] = useState({ width: 460, height: 450 });

  useEffect(() => {
    const handlerResize = () => {
      console.log(window.innerWidth, reWidth.width);
      if (window.innerWidth <= 500 && reWidth.width === 460) {
        setReWidth({ width: 300, height: 400 });
      } else if (window.innerWidth >= 550) {
        setReWidth({ width: 460, height: 450 });
      }
    };
    handlerResize();
    window.addEventListener('resize', handlerResize);
    return () => {
      window.removeEventListener('resize', handlerResize);
    };
  }, []);

  return (
    <Layout noPadding>
      <BannerSection resData={resData} />
      <NewsVideoSection resData={resData} />
      <Hidden mdDown>
        <DesktopLinkSection resData={resData} />
      </Hidden>
      <ChartImageSliderSection resData={resData} />
      <AdditionalSection reWidth={reWidth} resData={resData} />
    </Layout>
  );
};

export async function getStaticProps() {
  const resData = await fetchData('/api/index');

  return {
    props: {
      resData,
    },
  };
}

export default Index;
