import App from 'next/app';
import Router from 'next/router';
import NProgress from 'nprogress';
import UserContext from '../components/context/UserContext';
/* Page Routing Animation */
Router.onRouteChangeStart = (url) => {
  // console.log(url); // checking path routed to
  // Start NProgress loaders on route change
  NProgress.start();
};

// check when route change is complete, then end NProgress
Router.onRouteChangeComplete = () => NProgress.done();
// also check for errors and stop the progress bar if there is one
Router.onRouteChangeError = () => NProgress.done();

// importing global css
import '../public/static/css/assets/slick.min.css';
import '../public/static/css/assets/slick-theme.min.css';
import 'react-image-lightbox/style.css';

//
import 'bootstrap/dist/css/bootstrap.min.css';
//import "jquery/dist/jquery.min.js";
// import 'bootstrap/dist/js/bootstrap.min.js';
import '../public/static/css/main.css';
import '../public/static/css/assets/aos.css';

export default class MyApp extends App {
  state = {
    fontSize: '62.5%',
  };
  changeFont = (newSize) => {
    this.setState({
      fontSize: newSize,
    });
    document.querySelector('html').style.fontSize = newSize;
  };

  render() {
    // getting props via extending next's App
    const { Component, pageProps } = this.props;
    const { fontSize } = this.state;
    // return <Component {...pageProps} />;
    return (
      <UserContext.Provider
        value={{
          // 改變字體
          fontSize: fontSize,
          changeFont: this.changeFont,
        }}
      >
        <Component {...pageProps} />
      </UserContext.Provider>
    );
  }
}
