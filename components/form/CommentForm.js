import { useState, useEffect, useRef, memo } from 'react';
import Router from 'next/router';
import Link from 'next/link';
import { withFormik } from 'formik';

import { fetchData, postFileData } from '../../api/helper';
import DialogCheck from '../shared/Dialog/DialogCheck';
import { CategorySelect, CategorySubSelect } from './CommentCategorySelect';
import { categoryList } from '../../data/category-data';
import CommentCategoryInfoDialog from '../shared/Dialog/CommentCategoryInfoDialog';
import CommentTable from '../shared/Table/CommentTable';
import EmptyLinkList from '../shared/Layout/MainContent/EmptyLinkList';

import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
// 縣市api
// import TwCitySelector from 'tw-city-selector';
// 不判斷會window會出錯
let TwCitySelector;
if (typeof window !== 'undefined') {
  TwCitySelector = require('tw-city-selector');
}

const useStyles = makeStyles((theme) => ({
  infoTitle: {
    fontSize: '1.8rem',
    display: 'flex',
    alignItems: 'center',
  },
  infoButton: {
    minWidth: '40px',
    padding: '12px',
    marginLeft: '4px',
    borderRadius: '50px',
    '& .MuiButton-label': {
      width: '24px',
      height: '24px',
      borderRadius: '50px',
      backgroundColor: 'rgba(254,204,70,1)',
      color: 'white',
    },
  },
  categorySelectedButton: {
    fontWeight: '400',
    display: 'inline-flex',
    margin: '16px 0px 0px 0px',
    border: '1px solid rgba(0,0,0,0.2)',
    padding: '20px',
    fontSize: '1.6rem',
    color: 'rgba(0,0,0,0.8)',
    height: 'calc(47px + 1rem)',
    '& .info': {
      color: 'rgba(0,0,0,0.4)',
    },
  },
}));
// 沒有子分類的主分類
const emptyCategory = ['Sign', 'Other'];

const CommentForm = (props) => {
  const classes = useStyles();
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting,
    setFieldValue,
    handleReset,
    isOpen,
    setIsOpen,
    directUrl,
  } = props;

  // 分類暫存
  const [selectMainCategory, setSelectMainCategory] = useState('');
  const [selectSubCategory, setSelectSubCategory] = useState('');
  const [infoDialogDisplay, setInfoDialogDisplay] = useState(false); //意見類別說明的開關
  const [targetSubCategoryData, setTargetSubCategoryData] = useState({});

  const [ticketNumberList, setTicketNumberList] = useState(['TicketNumber0']);
  const [ticketNumberIndex, setTicketNumberIndex] = useState(1);

  const [showTable, setShowTable] = useState(false);
  const [loadingTable, setLoadingTable] = useState(false);
  const [choiceData, setChoiceData] = useState('未選擇任何檔案');
  const [choiceVideo, setChoiceVideo] = useState('未選擇任何檔案');

  const handleChangeChoiceData = (e) => {
    if (e.target.name == 'file-data') {
      setFieldValue('PostedFiles', [...values.PostedFiles, e.target.files[0]]);
    } else if (e.target.name == 'file-video') {
      setFieldValue('PostedFiles', [...values.PostedFiles, e.target.files[0]]);
    }
  };
  const handleExitForm = () => {
    Router.back();
  };
  // 主分類fn
  const handleChangeMainCategory = (e) => {
    const targetCategory = e.target.value;
    const targetSubCategoryData = categoryList.find(
      (category) => category.id === targetCategory
    );
    if (targetSubCategoryData) {
      setTargetSubCategoryData(targetSubCategoryData);
    } else {
      setTargetSubCategoryData({});
    }
    setFieldValue('SelectSubCategory', '');
    setSelectSubCategory('');
    setSelectMainCategory(targetCategory);

    handleChange(e);
    if (targetCategory === 'Fine') setFieldValue('IsPublic', 'false');
  };
  // 次分類fn
  const handleChangeSubCategory = (e) => {
    setSelectSubCategory(e.target.value);
    handleChange(e);
  };
  //分類確定fn
  const handleChangeCategoryDetermined = () => {
    setLoadingTable(true);
    loadingAndDisplay();
  };
  //讀取中fn
  const loadingAndDisplay = () => {
    setTimeout(() => {
      setLoadingTable(false);
      setShowTable(true);
    }, 1000);
  };
  //移除讀取fn
  useEffect(() => {
    return () => clearTimeout(loadingAndDisplay);
  }, []);

  // 清除時執行
  const handleClear = (e) => {
    handleReset(e);
    // 保留類別值
    setFieldValue('selectMainCategory', selectMainCategory);
    setFieldValue('selectSubCategory', selectSubCategory);
  };
  useEffect(() => {
    // 縣市api
    new TwCitySelector({
      el: '.city-selector-set',
      elCounty: '.county', // 在 el 裡查找 element
      elDistrict: '.district', // 在 el 裡查找 element
      // elZipcode: '.zipcode', // 在 el 裡查找 element
    });

    //error scroll
  }, []);
  const handleScroll = () => {
    const keys = Object.keys(errors);
    console.log('keys', keys);
    if (keys.length === 0) return;
    const firstErrorSelector = `#${keys[0]}`;
    const firstErrorElement = document.querySelector(firstErrorSelector);
    const firstErrorTop = firstErrorElement.offsetTop;

    animationScrollY();
    // 移至錯誤的input
    function animationScrollY() {
      let current = window.scrollY;
      // window.scrollTo(0, {
      //   top: 300,
      //   behavior: 'smooth',
      // });
      let time = setInterval(() => {
        // console.log('run');
        window.scrollTo(0, current);
        if (current > firstErrorTop) {
          current -= 100;
          if (current < firstErrorTop) {
            clearInterval(time);
            firstErrorElement.focus();
            // console.log('stop');
          }
        }
      }, 16);
    }
  };

  const handlerAddTicketNumber = (e) => {
    e.preventDefault();
    setTicketNumberList((prev) => [
      'TicketNumber' + ticketNumberIndex,
      ...prev,
    ]);
    setTicketNumberIndex((prev) => prev + 1);
  };

  const handlerRemoveTicketNumber = (e, index) => {
    e.preventDefault();
    if (ticketNumberList.length < 2) return;
    let newArr = [...ticketNumberList];
    const targetIndex = newArr.indexOf(index);
    if (targetIndex < 0) return;
    newArr.splice(targetIndex, 1);
    setTicketNumberList(newArr);
  };

  const handlerChangeTicketNumber = (e, item) => {
    values.TicketNumbers[item] = e.target.value;
    setFieldValue('TicketNumbers', values.TicketNumbers);
  };
  console.log('values', values);
  return (
    <>
      <form onSubmit={handleSubmit} className="login-form">
        <div id="feedback">
          <div className="opinions-choice">
            <div className={classes.infoTitle}>
              請選擇意見類別
              <Button
                className={classes.infoButton}
                onClick={() => setInfoDialogDisplay((state) => !state)}
              >
                ？
              </Button>
              <CommentCategoryInfoDialog
                dialogDisplay={infoDialogDisplay}
                setDialogDisplay={setInfoDialogDisplay}
              />
            </div>

            <CategorySelect
              menuData={categoryList}
              selectMainCategory={selectMainCategory}
              handleChangeMainCategory={handleChangeMainCategory}
              disabled={showTable}
              values={values}
            />

            {targetSubCategoryData.subCategoryList?.length > 0 && (
              <CategorySubSelect
                menuData={targetSubCategoryData.subCategoryList}
                selectSubCategory={selectSubCategory}
                handleChangeSubCategory={handleChangeSubCategory}
                disabled={showTable}
                values={values}
              />
            )}

            {(selectMainCategory && selectSubCategory && (
              <Button
                className={classes.categorySelectedButton}
                onClick={handleChangeCategoryDetermined}
                disabled={showTable}
              >
                確定
              </Button>
            )) ||
              ((selectMainCategory == 'Sign' ||
                selectMainCategory == 'Other') && (
                <Button
                  className={classes.categorySelectedButton}
                  onClick={handleChangeCategoryDetermined}
                  disabled={showTable}
                >
                  確定
                </Button>
              ))}
          </div>
        </div>
        {loadingTable && <EmptyLinkList limit={10} />}
        {showTable && (
          <CommentTable
            values={values}
            errors={errors}
            handleChange={handleChange}
            touched={touched}
            selectMainCategory={selectMainCategory}
            selectSubCategory={selectSubCategory}
            handleClear={handleClear}
          />
        )}

        <DialogCheck
          isOpen={isOpen}
          setIsOpen={setIsOpen}
          directUrl={directUrl}
        />
      </form>
    </>
  );
};

const CommentFormValidated = withFormik({
  // allows Formik to reinitialize it's forms when props change
  enableReinitialize: true,
  // Makes values props that holds the form state

  mapPropsToValues: (props) => {
    return {
      SelectMainCategory: '',
      SelectSubCategory: '',
      // Category: '',
      ApplicantName: '',
      ApplicantTitle: '女士',
      Email: '',
      Phone: '',
      county: '',
      district: '',
      DetailAddress: '',
      Title: '',
      Description: '',
      PlateNumber: '',
      TicketNumbers: { TicketNumber0: '' },
      // TotalAddress: '',
      IsPublic: 'true',
      RelatedLinks: '',
      PostedFiles: [],
      IsChiefMailbox: false,
      isReadOk: false,
    };
  },

  // Custom validation
  validate: (values) => {
    const errors = {};
    console.log('Values in validation:', values);

    if (!values.SelectMainCategory) {
      errors.SelectSubCategory = '必填欄位';
    }
    if (
      !values.SelectSubCategory &&
      !emptyCategory.includes(values.SelectMainCategory)
    ) {
      errors.SelectSubCategory = '必填欄位';
    }
    if (!values.ApplicantName) {
      errors.ApplicantName = '必填欄位';
    }

    if (!values.Email) {
      errors.Email = '必填欄位';
    }
    if (!values.Phone) {
      errors.Phone = '必填欄位';
    }
    if (!/^\+?[0-9]*$/.test(values.Phone) && values.Phone.length < 7) {
      errors.Phone = '格式有誤';
    }
    if (!values.DetailAddress || !values.county || !values.district) {
      errors.DetailAddress = '必填欄位';
    }
    if (!values.Title) {
      errors.Title = '必填欄位';
    }
    if (!values.Description) {
      errors.Description = '必填欄位';
    }

    if (values.SelectMainCategory === 'Fine' && !values.PlateNumber) {
      errors.PlateNumber = '必填欄位';
    }
    // if (values.SelectMainCategory === 'Fine' && !values.TicketNumber) {
    //   errors.TicketNumber = '必填欄位';
    // }
    return errors;
  },

  // Submitting Form
  handleSubmit: async (
    values,
    { props, setSubmitting, resetForm },
    handleReset
  ) => {
    console.log(
      'Post Values Before:',
      values,
      `${values.SelectMainCategory}|${values.SelectSubCategory}`
    );
    console.log('handleSubmit carried over Props:', props);
    const { isOpen, setIsOpen } = props;

    // const tickNumberList = Object.keys(values.TicketNumbers).map((item) => ({
    //   ['TicketNumber']: values.TicketNumbers[item],
    // }));
    const tickNumberList = Object.keys(values.TicketNumbers).map(
      (item) => values.TicketNumbers[item]
    );
    console.log('tickNumberList', tickNumberList);
    let postObj = {
      Category: `${values.SelectMainCategory}|${values.SelectSubCategory}`,
      Title: values.Title,
      ApplicantName: values.ApplicantName,
      ApplicantTitle: values.ApplicantTitle,
      Email: values.Email,
      Phone: values.Phone,
      // City: values.City,
      // District: values.District,
      // DetailAddress: values.DetailAddress,
      Address: values.county + values.district + values.DetailAddress,
      IsPublic: values.IsPublic === 'true' ? true : false,
      RelatedLinks: values.RelatedLinks,
      // PostedFiles: values.PostedFiles,
      Description: values.Description,
      // PlateNumber: values.PlateNumber,
      // TicketNumber: tickNumberList,
      IsChiefMailbox: values.IsChiefMailbox,
    };

    // 裁罰相關需填車牌跟單號
    if (values.SelectMainCategory === 'Fine') {
      postObj = {
        ...postObj,
        PlateNumber: values.PlateNumber,
        TicketNumber: tickNumberList,
      };
    }
    console.log('postObj', postObj);
    setSubmitting(true);
    let res = await postFileData('/api/opinions', postObj);
    console.log('postObjres', res);

    if (res.status === 201) {
      // props.setSuccessModal({
      //   msg: '新增成功！',
      //   back: true,
      // });
      // setSubmitting(false);
    }
    if (res.status === 200) {
      console.log('success');
      setIsOpen((prev) => !prev);
      // resetForm({});
      // props.setSuccessModal({
      //   msg: '編輯成功！',
      //   back: true,
      // });
      setSubmitting(false);
    }
  },

  displayName: 'BasicForm',
})(CommentForm);

export default CommentFormValidated;
