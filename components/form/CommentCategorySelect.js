import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';

const categoryUseStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(2, 2, 0, 0),
    minWidth: 140,
    '& label': {
      fontSize: '1.6rem',
    },
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  selectRoot: {
    fontSize: '1.6rem',
    lineHeight: '20px',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  menuItem: {
    fontSize: '1.6rem',
  },
}));

export const CategorySelect = ({
  menuData,
  handleChangeMainCategory,
  disabled,
  values,
}) => {
  const classes = categoryUseStyles();
  return (
    <FormControl
      variant="outlined"
      className={classes.formControl}
      disabled={disabled}
    >
      <InputLabel htmlFor="category-select" id="category-select-label">
        分類
      </InputLabel>
      <Select
        labelId="category-select-label"
        id="category-select"
        onChange={handleChangeMainCategory}
        name="SelectMainCategory"
        label="Category"
        classes={{ root: classes.selectRoot }}
        value={values.SelectMainCategory}
      >
        <MenuItem value="" className={classes.menuItem}>
          <em>請選擇</em>
        </MenuItem>

        {menuData.map((item, index) => (
          <MenuItem key={index} value={item.id} className={classes.menuItem}>
            {item.category}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export const CategorySubSelect = ({
  menuData,
  handleChangeSubCategory,
  disabled,
  values,
}) => {
  const classes = categoryUseStyles();
  return (
    <FormControl
      variant="outlined"
      className={classes.formControl}
      disabled={disabled}
    >
      <InputLabel htmlFor="category-subSelect" id="category-subSelect-label">
        次分類
      </InputLabel>
      <Select
        labelId="category-subSelect-label"
        id="category-subSelect"
        onChange={handleChangeSubCategory}
        name="SelectSubCategory"
        label="次分類"
        classes={{ root: classes.selectRoot }}
        value={values.SelectSubCategory}
      >
        <MenuItem value="" className={classes.menuItem}>
          <em>請選擇</em>
        </MenuItem>
        {menuData.map((item, index) => (
          <MenuItem key={index} value={item.id} className={classes.menuItem}>
            {item.category}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};
