import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Link from 'next/link';
const useStyles = makeStyles((theme) => ({
  pannelButton: {
    margin: theme.spacing(1, 1),
    fontSize: '2rem',
    // background: 'linear-gradient(180deg,#b7f0e7  3%,#b7f0e7 )',
    background: '#b7f0e7',
    fontWeight: 'normal',
    boxShadow: 'unset',
    padding: theme.spacing(1, 4),
    '&:hover': {
      boxShadow: 'unset',
      background: '#A4D8CF',
    },
    [theme.breakpoints.down('md')]: {
      fontSize: '1.8rem',
      padding: theme.spacing(1, 3),
    },
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.6rem',
      padding: theme.spacing(1, 2),
    },
  },
  active: {
    background: 'linear-gradient(180deg,#0fd5d8 3%,#0191b4)',
    color: '#FFF',
    '&:hover': {
      background: 'linear-gradient(180deg,#0fd5d8 3%,#0191b4)',
    },
  },
}));
const PannelButton = ({ active, title, path }) => {
  const classes = useStyles();
  return (
    <Link href={path}>
      <Button
        variant="contained"
        className={`${classes.pannelButton} ${
          title == active ? classes.active : ''
        }`}
      >
        {title}
      </Button>
    </Link>
  );
};
export default PannelButton;
