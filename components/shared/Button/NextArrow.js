const NextArrow = ({ onClick }) => {
  return (
    <a href="javascript:;" className="right-arrow-icon" onClick={onClick}>
      <img src="/static/img/icons/right-icon.png" alt="下一張" />
    </a>
  );
};

export default NextArrow;
