import AnchorLink from 'react-anchor-link-smooth-scroll';

const BackTopButton = () => {
  return (
    <div className="back-to-top-btn">
      <AnchorLink href="#app" title="回到最上方">
        <img src="/static/img/home/up-icon.png" alt="回到最上方" />
      </AnchorLink>
    </div>
  );
};

export default BackTopButton;
