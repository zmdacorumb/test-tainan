import Link from 'next/link';

const MoreButton = ({ link }) => {
  return (
    <Link href={link}>
      <a className="more-btn" title="顯示更多">More</a>
    </Link>
  );
};

export default MoreButton;
