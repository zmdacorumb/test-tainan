const PrevArrow = ({ onClick }) => {
  return (
    <a href="javascript:;" className="left-arrow-icon" onClick={onClick}>
      <img src="/static/img/icons/left-icon.png" alt="前一張" />
    </a>
  );
};

export default PrevArrow;
