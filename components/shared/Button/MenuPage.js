import Link from 'next/link';

const MenuPage = ({ url }) => {
  return (
    <div className="menu-page">
      <Link href="#">
        <a>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="115"
            height="36.122"
            viewBox="0 0 115 36.122"
          >
            <g id="btn_last" transform="translate(-1380.95 -823.882)">
              <path
                id="Path_5179"
                fill="#00a49b"
                d="M5.362 0h104.276A5.362 5.362 0 0 1 115 5.362v25.276A5.362 5.362 0 0 1 109.638 36H5.362A5.362 5.362 0 0 1 0 30.638V5.362A5.362 5.362 0 0 1 5.362 0z"
                data-name="Path 5179"
                transform="translate(1380.95 824.004)"
              />
              <text
                id="上一則"
                fill="#fff"
                font-family="NotoSansCJKtc-DemiLight, Noto Sans CJK TC"
                font-size="22px"
                font-weight="300"
                transform="translate(1413.115 823.882)"
              >
                <tspan x="0" y="26">
                  上一則
                </tspan>
              </text>
              <path
                id="Path_1"
                fill="#fff"
                d="M134.668 419.527l-11.774-8.168 12-7.454z"
                data-name="Path 1"
                transform="translate(1271.884 431.04)"
              />
            </g>
          </svg>
        </a>
      </Link>
      <Link href={url ? url : '#'}>
        <a>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="100"
            height="36.122"
            viewBox="0 0 100 36.122"
          >
            <g id="btn_list" transform="translate(-1521.229 -823.882)">
              <rect
                id="Rectangle_1"
                width="100"
                height="36"
                fill="#00a49b"
                data-name="Rectangle 1"
                rx="5"
                transform="translate(1521.229 824.004)"
              />
              <text
                id="回列表"
                fill="#fff"
                font-family="NotoSansCJKtc-DemiLight, Noto Sans CJK TC"
                font-size="22px"
                font-weight="300"
                transform="translate(1538.393 823.882)"
              >
                <tspan x="0" y="26">
                  回列表
                </tspan>
              </text>
            </g>
          </svg>
        </a>
      </Link>
      <Link href="#">
        <a>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="115"
            height="36.122"
            viewBox="0 0 115 36.122"
          >
            <g id="btn_next" transform="translate(-1643.598 -823.882)">
              <rect
                id="Rectangle_3"
                width="115"
                height="36"
                fill="#00a49b"
                data-name="Rectangle 3"
                rx="5.362"
                transform="translate(1643.598 824.004)"
              />
              <text
                id="下一則"
                fill="#fff"
                font-family="NotoSansCJKtc-DemiLight, Noto Sans CJK TC"
                font-size="22px"
                font-weight="300"
                transform="translate(1660.763 823.882)"
              >
                <tspan x="0" y="26">
                  下一則
                </tspan>
              </text>
              <path
                id="Path_2"
                fill="#fff"
                d="M461.7 419.527l11.774-8.168-12-7.454z"
                data-name="Path 2"
                transform="translate(1271.884 431.04)"
              />
            </g>
          </svg>
        </a>
      </Link>
    </div>
  );
};
export default MenuPage;
