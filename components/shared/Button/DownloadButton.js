const DownloadButton = ({ title, newWindow }) => {
  console.log('New Window', title);

  if (newWindow === true) {
    return (
      <a
        href="javascript:;"
        className="download-btn"
        target="_blank"
        title="(另開視窗)"
        rel="noreferrer noopener"
      >
        <span>{title}</span>
        <img src="/static/img/icons/download-icon-green.png" alt="" />
      </a>
    );
  } else {
    return (
      <a href="javascript:;" className="download-btn">
        <span>{title}</span>
        <img src="/static/img/icons/download-icon-green.png" alt="" />
      </a>
    );
  }
};

export default DownloadButton;
