import React from 'react';
// use nextjs router for back functionality
import Router from 'next/router';

const BackButton = ({ text }) => {
  return (
    <div className="publish-title">
      <a
        href="#"
        onClick={(e) => {
          e.preventDefault();
          Router.back();
        }}
      >
        <img src="/static/img/home/icon_back.png" alt="" />
      </a>
      <span>{text}</span>
    </div>
    // <div className="back-button-wrap" onClick={() => Router.back()}>
    //   <a href="javascript:;" className="back-button" title="回上頁">
    //     <img src="/static/img/icons/back-icon.png" alt="" />
    //     {text ? text : '回上頁'}
    //   </a>
    // </div>
  );
};

export default BackButton;
