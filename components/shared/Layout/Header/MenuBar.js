import Link from 'next/link';
import { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import MUButton from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

import navlist from '../../../../data/navlist-data';
// import CC from '../../../../data/centralizedConsoleLog';

const useStyles = makeStyles((theme) => ({
  menuBar: {
    flexGrow: '1',
    justifyContent: 'flex-end',
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  menuList: {
    margin: '0px 8px',
    // 第一層滑到第二層間的小區塊，讓第二層不會突然消失
    '&::after': {
      zIndex: '2',
      content: '""',
      width: '100px',
      height: '50px',
      position: 'absolute',
      display: 'block',
      background: 'blue',
      opacity: '0',
    },
  },
  secondHeader: {
    borderTop: '2px transparent solid',
    position: 'absolute',
    left: '0px',
    top: '100%',
    width: '100%',
    backgroundColor: 'rgb(73,122,130)',
    opacity: '0',
    transform: 'scaleY(0)',
    transition: '0.26s',
    overflow: ' hidden',
    transformOrigin: 'top',
    transitionTimingFunction: 'easeInOut',
    padding: theme.spacing(1, 0),
    '&.active': {
      display: 'block',
      opacity: '1',
      borderTop: '2px white solid',
      transform: 'scaleY(1)',
    },
  },
  gridContainer: {
    maxWidth: '1300px',
    margin: '0px auto',
    width: '100%',
  },
  menuListButton: {
    background: 'none',
    border: '2px solid #b3e9dd',
    fontSize: '1.6rem',
    padding: '5px 5px',
    cursor: 'pointer',
    [theme.breakpoints.up('vlg')]: {
      padding: '5px 10px',
      fontSize: '1.8rem',
    },

    '&.active': {
      position: 'relative',
      backgroundColor: 'white',
      color: '#231815',
      border: '2px solid #231815',
      borderRadius: '5px',
      '&:before': {
        borderRadius: '2px',
        zIndex: '0',
        position: 'absolute',
        display: 'block',
        top: ' 100%',
        left: '50%',
        transform: 'translate(-50%,-52%) rotate(45deg)',
        content: '""',
        width: '14px',
        height: '14px',
        backgroundColor: 'white',
      },
      '&::after': {
        zIndex: '-1',
        position: 'absolute',
        display: 'block',
        top: '100%',
        left: ' 50%',
        transform: 'translate(-50%,-50%) rotate(45deg)',
        borderRadius: '5px',
        content: '""',
        width: '18px',
        height: '18px',
        backgroundColor: '#231815',
      },
    },
  },
  menuSecondListButton: {
    width: 'calc(100% - 40px)',
    cursor: 'default',
    position: 'relative',
    margin: '5px 20px 15px 20px',
    color: 'rgb(249, 255, 207)',
    fontWeight: '300',
    fontSize: '1.6rem',
    padding: '6px 8px 0px 13px',
    '&:hover': {
      background: 'rgba(249, 255, 207,0.1)',
    },
    '& .MuiButton-label': {
      justifyContent: 'left',
    },
    '&::before': {
      position: 'absolute',
      top: 'calc(100% - 2.5px )',
      left: '-5px',
      content: '""',
      width: '5px',
      height: '5px',
      borderRadius: '50%',
      backgroundColor: 'rgba(249,255,207,0.3)',
      display: 'block',
    },
    '&::after': {
      position: 'absolute',
      top: 'calc(100% - 2.5px )',
      right: '-5px',
      content: '""',
      width: '5px',
      height: '5px',
      borderRadius: '50%',
      backgroundColor: 'rgba(249,255,207,0.3)',
      display: 'block',
    },
  },
  menuSecondListLine: {
    position: 'absolute',
    top: '100%',
    left: '0px',
    width: '100%',
    height: '1px',
    backgroundColor: 'rgba(249, 255, 207,0.5)',
    display: 'block',
  },
}));

const MenuBar = () => {
  const classes = useStyles();
  return (
    <nav id="menu-bar" className={classes.menuBar}>
      {navlist.map((item, index) => (
        <MenuList data={item} key={item.title} />
      ))}
    </nav>
  );
};

const MenuList = ({ data }) => {
  const [menuOpen, setMenuOpen] = useState(false);

  const toggleMenu = (open) => (event) => {
    setMenuOpen(open);
  };
  const classes = useStyles();

  return (
    <div
      className={classes.menuList}
      onMouseEnter={toggleMenu(true)}
      onMouseLeave={toggleMenu(false)}
      onFocus={toggleMenu(true)}
      onBlur={toggleMenu(false)}
    >
      <Link href={data.path}>
        <button className={`${classes.menuListButton} ${menuOpen && 'active'}`}>
          {data.title}
        </button>
      </Link>
      <div className={`${classes.secondHeader} ${menuOpen && 'active'}`}>
        <Grid container className={classes.gridContainer}>
          {data.subNav.map((innerItem, innerIndex) => (
            <Grid item xs={2} className={classes.gridItem} key={innerIndex}>
              {innerItem.externalLink ? (
                <a
                  href={innerItem.externalLink}
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  <MUButton
                    className={classes.menuSecondListButton}
                    // tabIndex="-1"
                    key={innerItem.title}
                  >
                    {innerItem.title}
                    <span className={classes.menuSecondListLine} />
                  </MUButton>
                </a>
              ) : (
                <Link href={innerItem.path}>
                  <MUButton
                    className={classes.menuSecondListButton}
                    // tabIndex="-1"
                    key={innerItem.title}
                  >
                    {innerItem.title}
                    <span className={classes.menuSecondListLine} />
                  </MUButton>
                </Link>
              )}
            </Grid>
          ))}
        </Grid>
      </div>
    </div>
  );
};
export default MenuBar;
