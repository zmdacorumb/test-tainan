import Link from 'next/link';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  menuLogo: {
    maxWidth: '320px',
    [theme.breakpoints.down('sm')]: {
      maxWidth: '270px',
    },
    [theme.breakpoints.down('xs')]: {
      maxWidth: '220px',
    },
  },
}));
const Logo = () => {
  const classes = useStyles();

  return (
    <Link href="/">
      <a title="臺南市政府交通局 回首頁">
        <h1>
          <img
            src="/static/img/header/main-logo.png"
            alt="臺南市政府交通局"
            className={classes.menuLogo}
          />
        </h1>
      </a>
    </Link>
  );
};

export default Logo;
