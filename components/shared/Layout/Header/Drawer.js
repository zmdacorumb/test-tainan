import { useState } from 'react';
import Link from 'next/link';

import { fade, makeStyles } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

import ChangeFontSize from './ChangeFontSize';
import navlist from '../../../../data/navlist-data';
//樣式管理
const useStyles = makeStyles((theme) => ({
  arrowDropDownIcon: {
    fontSize: '2.2rem',
    color: 'white',
    transition: '0.2s',
    transitionTimingFunction: 'ease-in-out',
  },
  folderOpen: {
    transform: 'rotate(180deg)',
  },
  listItemText: {
    color: 'white',
    fontSize: '1.4rem',
    '&.second-level': {
      paddingLeft: '13px',
    },
  },
  paperAnchorRight: {
    padding: '15px 5px',
    backgroundColor: 'rgb(73,122,130)',
  },
  menuButton: {
    display: 'none',
    [theme.breakpoints.down('sm')]: {
      display: 'block',
    },
  },
  menuIcon: {
    fontSize: '30px',
  },
  list: {
    width: 250,
  },

  search: {
    padding: '0px 10px',
    position: 'relative',
    borderRadius: '20px',
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    margin: '0px 8px 20px 8px',
  },
  searchIcon: {
    color: 'white',
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    color: 'white',
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));

//按鈕及觸發Mobile Menu
const MuDrawer = () => {
  const classes = useStyles();
  const [openState, setOpenState] = useState(false);

  const toggleDrawer = (open) => (event) => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }
    setOpenState(!openState);
  };

  return (
    <>
      <IconButton
        edge="start"
        className={classes.menuButton}
        color="inherit"
        aria-label="open drawer"
        onClick={toggleDrawer()}
      >
        <MenuIcon className={classes.menuIcon} />
      </IconButton>
      <SwipeableDrawer
        classes={{ paperAnchorRight: classes.paperAnchorRight }}
        anchor={'right'}
        open={openState}
        onClose={toggleDrawer()}
        onOpen={toggleDrawer()}
      >
        <DrawerList toggleDrawer={toggleDrawer} />
      </SwipeableDrawer>
    </>
  );
};

//Mobile Menu
const DrawerList = ({ toggleDrawer }) => {
  const classes = useStyles();
  return (
    <div
      className={classes.list}
      role="presentation"
      // onClick={toggleDrawer()}
      // onKeyDown={toggleDrawer()}
    >
      <List>
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder="Search…"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            inputProps={{ 'aria-label': 'search' }}
          />
        </div>

        {navlist.map((item, index) => (
          <MenuList key={item.title} menuData={item} />
        ))}
      </List>
      <Divider />
      <List>
        <ListItem button>
          <ChangeFontSize />
        </ListItem>
      </List>
      <Divider />
      <List>
        <ListItem button>
          <a href="/" className="mobile-nav-link">
            <img src="/static/img/icons/facebook-bg.svg" alt="facebook" />
          </a>
          <a href="/" className="mobile-nav-link">
            <img src="/static/img/icons/youtube-bg.svg" alt="youtube" />
          </a>
          <Link href="/">
            <a className="mobile-nav-link">
              <img src="/static/img/icons/sitemap-bg.svg" alt="sitemap" />
            </a>
          </Link>
          <Link href="/">
            <a className="mobile-nav-link">
              <img src="/static/img/icons/english-bg.svg" alt="english" />
            </a>
          </Link>
        </ListItem>
      </List>
    </div>
  );
};

const MenuList = ({ menuData }) => {
  const [folderSwitch, setFolderSwitch] = useState(false);
  const classes = useStyles();
  const handleSwitch = () => {
    setFolderSwitch((folderSwitch) => !folderSwitch);
  };
  return (
    <>
      <ListItem button key={menuData.title} onClick={handleSwitch}>
        <ListItemIcon>
          <ArrowDropDownIcon
            className={`${classes.arrowDropDownIcon} ${
              folderSwitch && classes.folderOpen
            }`}
          />
        </ListItemIcon>
        <ListItemText
          classes={{ primary: classes.listItemText }}
          primary={menuData.title}
        />
      </ListItem>
      <Collapse in={folderSwitch}>
        {menuData.subNav.map((item, index) =>
          item.externalLink ? (
            <a
              href={item.externalLink}
              key={item.title}
              target="_blank"
              rel="noreferrer noopener"
            >
              <ListItem button>
                <ListItemIcon></ListItemIcon>
                <ListItemText
                  classes={{ primary: `${classes.listItemText} second-level` }}
                  primary={`${item.title}`}
                />
              </ListItem>
            </a>
          ) : (
            <Link href={item.path} key={item.title}>
              <ListItem button>
                <ListItemIcon></ListItemIcon>
                <ListItemText
                  classes={{ primary: `${classes.listItemText} second-level` }}
                  primary={`${item.title}`}
                />
              </ListItem>
            </Link>
          )
        )}
      </Collapse>
    </>
  );
};

export default MuDrawer;
