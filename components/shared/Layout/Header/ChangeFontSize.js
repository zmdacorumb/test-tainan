import { useContext } from 'react';
import UserContext from '../../../context/UserContext';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  displaySize: {
    margin: '0px 3px',
    width: '25px',
    height: '25px',
    backgroundColor: '#231815',
    padding: '3px',
    borderRadius: '50%',
    '& a': {
      fontWeight: '500',
      fontSize: '1.5rem',
      color: ' white',
      lineHeight: '19px',
      textAlign: 'center',
      display: 'block',
    },
    '&.active': {
      backgroundColor: 'white',
      '& a': {
        color: '#231815',
      },
    },
  },
}));
const ChangeFontSize = () => {
  const { fontSize, changeFont } = useContext(UserContext);
  const classes = useStyles();
  return (
    <Grid container wrap="nowrap">
      <Grid
        className={`${classes.displaySize} ${
          fontSize == '50%' ? 'active' : ''
        } mobile-margin`}
        onClick={() => changeFont('50%')}
      >
        <a href="#">
          A<sup>-</sup>
        </a>
      </Grid>

      <Grid
        className={`${classes.displaySize} ${
          fontSize == '62.5%' ? 'active' : ''
        } mobile-margin`}
        onClick={() => changeFont('62.5%')}
      >
        <a href="#">A</a>
      </Grid>

      <Grid
        className={`${classes.displaySize} ${
          fontSize == '80%' ? 'active' : ''
        } mobile-margin`}
        onClick={() => changeFont('80%')}
      >
        <a href="#">
          A<sup>+</sup>
        </a>
      </Grid>
    </Grid>
  );
};

export default ChangeFontSize;
