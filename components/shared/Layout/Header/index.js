import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import InputBase from '@material-ui/core/InputBase';
import { fade, makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';

import Logo from './Logo';
import MenuBar from './MenuBar';
import SiteMap from './SiteMap';
import ChangeFontSize from './ChangeFontSize';
// import CC from '../../../../data/centralizedConsoleLog';
import Drawer from './Drawer';

function HideOnScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

const useStyles = makeStyles((theme) => ({
  appBar: {
    backgroundColor: '#b3e9dd',
  },
  othetFunction: {
    textAlign: 'center',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  englishPage: {
    fontSize: '1.8rem',
    color: '#333333',
  },
  root: {
    flexGrow: 1,
  },
  toolbar: {
    display: 'flex',
    position: 'relative',
    padding: '30px 80px 30px 80px',
    [theme.breakpoints.down('md')]: {
      padding: '30px 20px',
      justifyContent: 'space-between',
    },
    [theme.breakpoints.down('xs')]: {
      padding: '10px 20px',
    },
  },
  accessKey: {
    display: 'block',
    minWidth: '25px',
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    display: 'none',
    [theme.breakpoints.down('sm')]: {
      display: 'block',
    },
  },

  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    margin: '0px 8px',
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  searchIcon: {
    color: '#333333',
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: '#333333',
    fontSize: '1.2rem',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));

export default function SearchAppBar() {
  const classes = useStyles();
  return (
    <HideOnScroll className={classes.root} fullWidth>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar className={classes.toolbar} disableGutters>
          <Logo />
          <MenuBar />
          <Button
            href="#U"
            accessKey="U"
            name="U"
            title="上方區塊"
            component="a"
            className={classes.accessKey}
          >
            :::
          </Button>
          <SiteMap />
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>
          <div className={classes.othetFunction}>
            <Link href="/">
              <a className={classes.englishPage}>ENGLISH</a>
            </Link>
            <ChangeFontSize />
          </div>
          <Drawer />
        </Toolbar>
      </AppBar>
    </HideOnScroll>
  );
}
