import Link from 'next/link';
import IconButton from '@material-ui/core/IconButton';
import Hidden from '@material-ui/core/Hidden';

const SiteMap = () => {
  return (
    <Hidden smDown>
      <IconButton>
        <Link href="/other/site-map">
          <img
            className="site-map-image"
            src="/static/img/icons/sitemap.svg"
            alt="sitemap"
          />
        </Link>
      </IconButton>
    </Hidden>
  );
};
export default SiteMap;
