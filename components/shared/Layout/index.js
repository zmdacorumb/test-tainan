import React, { Component } from 'react';
import Head from 'next/head';
import Router from 'next/router';
import NProgress from 'nprogress';

import MainContent from './MainContent';
import BackTopButton from '../Button/BackTopButton';
import Header from './Header';
import Footer from './Footer/index';
import Notification from '../../shared/Notification/PopUpMessage';

import { ThemeProvider } from '@material-ui/styles';
import theme from '../../../data/theme';

Router.onRouteChangeStart = (url) => {
  // Start NProgress loaders on route change
  NProgress.start();
};

// check when route change is complete, then end NProgress
Router.onRouteChangeComplete = () => NProgress.done();
// also check for errors and stop the progress bar if there is one
Router.onRouteChangeError = () => NProgress.done();

class Layout extends Component {
  render() {
    return (
      <React.Fragment>
        <Head>
          {/* Dynamic version placed in ContentLayout for now */}
          <title>臺南市政府交通局</title>
          <link
            href="/static/img/icons/favicon.ico"
            rel="shortcut icon"
            type="image/x-icon"
          />
        </Head>

        <div id="app" className="app">
          <a
            href="#C"
            id="gotocenter"
            title="跳到主要內容"
            tabIndex="1"
            className="sr-only sr-only-focusable"
          >
            跳到主要內容
          </a>
          <noscript>
            您的瀏覽器已關閉Javascript語法，開啟後即可正常瀏覽！ JavaScript has
            been deactivated on your browser, once turned on, you can browse
            normally!
          </noscript>
          <ThemeProvider theme={theme}>
            <div className="page-wrap">
              <Header />
              <Notification />
              {/* Spreading props to give access to them all for MainContent 
                Props
                -------
                title - to generate title bar for page with provided title
                noPadding - whether or not to remove default content padding
            */}
              <MainContent content={this.props.children} {...this.props} />
              <Footer />
            </div>
          </ThemeProvider>

          {/* Back to Top Button */}
          <BackTopButton />
        </div>
      </React.Fragment>
    );
  }
}

export default Layout;
