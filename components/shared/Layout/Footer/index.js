import Link from 'next/link';
import { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';

import navlist from '../../../../data/navlist-data';
import footerInfoCard from '../../../../data/footerInfoCard';
import { footerInfosSettings } from '../../../../data/slick-settings';
import Slider from 'react-slick';
import Hidden from '@material-ui/core/Hidden';

const Footer = () => {
  return [<FolderFooter key="1" />, <DesktopFooter key="2" />];
};
const useStyles = makeStyles((theme) => ({
  folderFooter: {
    backgroundColor: '#89a5b1',
  },
  accordion: {
    boxShadow: 'none',
    backgroundColor: '#89a5b1',
    margin: '0px auto',
    maxWidth: '1200px',
    color: 'white',
  },
  expanded: {
    margin: '0px auto !important',
  },
  expandIcon: {
    color: 'white',
    fontSize: '3rem',
  },
  accordionSummarycontent: {
    display: 'none',
  },
  accordionDetails: {
    padding: '8px 16px 60px 16px',
  },
  menuListTitle: {
    textAlign: 'center',
    fontSize: '1.8rem',
    cursor: 'default',
    position: 'relative',
    marginBottom: '12px',
    '&::after': {
      content: "''",
      bottom: '-10px',
      left: '10%',
      position: 'absolute',
      width: '80%',
      height: '1px',
      backgroundColor: 'rgba(255,255,255,0.6)',
    },
  },
  listItemButton: {
    padding: '6px 16px',
  },
  menuListLink: {
    textAlign: 'center',
    fontSize: '1.6rem',
    fontWeight: '300',
    color: 'white',
  },
  desktopFooter: {
    padding: '60px 0px 60px 0px',
  },
  container: {
    maxWidth: '1600px',
    margin: '0px auto',
  },
  cardContent: { padding: theme.spacing(2, 3) },
  cardTitle: { color: '#737171', flex: '1', fontSize: '1.8rem' },
  cardUnitBlock: {
    minWidth: '100px',
  },
  cardInfo: {
    color: '#737171',
    fontSize: '1.3rem',
  },
  iconButton: { width: '50px !important', padding: theme.spacing(1) },
  footerInfoBlock: {
    [theme.breakpoints.down('sm')]: {
      margin: '60px 0px 20px 0px ',
      padding: '20px 15px',
    },
  },
  footerInfo: {
    color: '#737171',
    fontSize: '1.3rem',
  },
  accessibilityButton: {
    width: '110px',
    padding: '0',
    margin: theme.spacing(0, 1),
    [theme.breakpoints.down('sm')]: {
      margin: theme.spacing(0, 1, 0, 0),
    },
    [theme.breakpoints.down('xs')]: {
      width: '80px',
    },
  },
  govButton: {
    width: '38px',
    padding: '0',
    margin: theme.spacing(0, 1),
    [theme.breakpoints.down('xs')]: {
      width: '26px',
      margin: 0,
    },
  },
  footerRightBlock: {
    [theme.breakpoints.down('sm')]: {
      margin: '80px 0px',
      padding: theme.spacing(2),
    },
    [theme.breakpoints.down('xs')]: {
      margin: '0px 0px 60px 0px',
      padding: theme.spacing(2),
    },
  },
  footerLinkeBlock: {
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(0, 0, 0, 12),
    },
    [theme.breakpoints.down('xs')]: {
      padding: 0,
    },
  },
  footerOtherBlock: {
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(2, 0, 0, 8),
    },
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(2, 0, 0, 0),
    },
  },
  accessibilityBlock: {
    [theme.breakpoints.down('sm')]: {
      order: 2,
      alignContent: 'flex-start',
    },
  },
  otherInfoBlock: {
    [theme.breakpoints.down('sm')]: {
      order: 1,
      alignContent: 'flex-start',
    },
  },
}));
const FolderFooter = () => {
  const classes = useStyles();
  const [expanded, setExpanded] = useState(false);

  const handleChange = (event, isExpanded) => {
    setExpanded((expanded) => !expanded);
  };
  return (
    <Hidden smDown>
      <div className={classes.folderFooter}>
        <Accordion
          expanded={expanded}
          onChange={handleChange}
          classes={{ root: classes.accordion, expanded: classes.expanded }}
        >
          <AccordionSummary
            expandIcon={
              <ExpandMoreIcon
                classes={{ fontSizeLarge: classes.expandIcon }}
                fontSize="large"
              />
            }
            aria-controls="panel1bh-content"
            id="panel1bh-header"
            classes={{
              content: classes.accordionSummarycontent,
            }}
          />
          <AccordionDetails className={classes.accordionDetails}>
            <Grid container justify="center">
              {navlist.map((item, index) => {
                return <FolderMenuList key={index} menuData={item} />;
              })}
            </Grid>
          </AccordionDetails>
        </Accordion>
      </div>
    </Hidden>
  );
};
const FolderMenuList = ({ menuData }) => {
  const classes = useStyles();

  return (
    <Grid xs={6} sm={2} md={2} item>
      <List>
        <ListItem key={menuData.title}>
          <ListItemText
            classes={{ primary: classes.menuListTitle }}
            primary={menuData.title}
          />
        </ListItem>
        {menuData.subNav.map((item, index) =>
          item.externalLink ? (
            <a
              href={item.externalLink}
              key={index}
              target="_blank"
              rel="noreferrer noopener"
            >
              <ListItem button className={classes.listItemButton}>
                <ListItemText
                  classes={{ primary: classes.menuListLink }}
                  primary={item.title}
                />
              </ListItem>
            </a>
          ) : (
            <Link href={item.path} key={index}>
              <ListItem button className={classes.listItemButton}>
                <ListItemText
                  classes={{ primary: classes.menuListLink }}
                  primary={item.title}
                />
              </ListItem>
            </Link>
          )
        )}
      </List>
    </Grid>
  );
};
const DesktopFooter = () => {
  const classes = useStyles();

  return (
    <div className={classes.desktopFooter}>
      <Grid classes={{ container: classes.container }} container>
        <Grid item xs={12} sm={6} md={3}>
          <Slider {...footerInfosSettings}>
            {footerInfoCard.map((item, index) => (
              <Card
                variant="outlined"
                raised
                classes={{ root: classes.card }}
                key={index}
              >
                <CardContent classes={{ root: classes.cardContent }}>
                  <Typography
                    classes={{ root: classes.cardTitle }}
                    gutterBottom
                  >
                    {item.authorityUnit}
                  </Typography>
                  <Grid container spacing={3}>
                    <Grid xs={5} item className={classes.cardUnitBlock}>
                      <Typography className={classes.cardInfo}>
                        {item.name}
                      </Typography>
                    </Grid>
                    <Grid xs={7} item>
                      <Typography
                        className={classes.cardInfo}
                      >{`地址：${item.postalCode} ${item.address}`}</Typography>
                      <Typography className={classes.cardInfo}>
                        電話：{item.phone}
                      </Typography>
                      <Typography className={classes.cardInfo}>
                        傳真：{item.fax}
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid container justify="flex-end">
                    <IconButton className={classes.iconButton}>
                      <img src="/static/img/icons/phone.png" alt="播打電話" />
                    </IconButton>
                    <IconButton className={classes.iconButton}>
                      <img src="/static/img/icons/map.png" alt="地圖" />
                    </IconButton>
                    <IconButton className={classes.iconButton}>
                      <img src="/static/img/icons/bus.png" alt="路線" />
                    </IconButton>
                  </Grid>
                </CardContent>
              </Card>
            ))}
          </Slider>
        </Grid>
        <Grid
          item
          xs={12}
          sm={6}
          md={6}
          className={classes.footerInfoBlock}
          container
          alignContent="flex-end"
          justify="center"
        >
          <Typography className={classes.footerInfo} gutterBottom>
            一般上班時間：上午08:00-12:00；下午13:30-17:30；彈性上班時間：08:00-08:30；17:30-18:00
          </Typography>
          <Typography className={classes.footerInfo} gutterBottom>
            建議瀏覽器：IE10.0 以上、Firefox、Chrome
            (螢幕設定最佳顯示效果為1980*1080)
          </Typography>
        </Grid>
        <Grid
          item
          xs={12}
          sm={12}
          md={3}
          container
          className={classes.footerRightBlock}
        >
          <Grid item sm={false} md={6} />
          <Grid item xs={6} sm={6} className={classes.footerLinkeBlock}>
            <List>
              <ListItem button>
                <Link href="/other/agency-communication">
                  <Typography className={classes.footerInfo}>
                    本局各單位通訊方式
                  </Typography>
                </Link>
              </ListItem>
              <ListItem button>
                <Link href="/other/privacy-security-policy">
                  <Typography className={classes.footerInfo}>
                    隱私權及安全政策
                  </Typography>
                </Link>
              </ListItem>
              <ListItem button>
                <Link href="/other/personal-data">
                  <Typography className={classes.footerInfo}>
                    保有及管理個人資料
                  </Typography>
                </Link>
              </ListItem>
              <ListItem button>
                <Link href="/other/government-website-information">
                  <Typography className={classes.footerInfo}>
                    政府網站資料開放宣告
                  </Typography>
                </Link>
              </ListItem>
            </List>
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            md={12}
            container
            spacing={1}
            className={classes.footerOtherBlock}
          >
            <Grid
              item
              sm={12}
              md={6}
              container
              alignItems="flex-end"
              className={classes.accessibilityBlock}
            >
              <IconButton className={classes.accessibilityButton}>
                <img
                  src="/static/img/icons/accessibility.png"
                  alt="無障礙標章"
                />
              </IconButton>
              <IconButton className={classes.govButton}>
                <img src="/static/img/icons/gov.png" alt="政府標章" />
              </IconButton>
            </Grid>
            <Grid
              item
              sm={12}
              md={6}
              container
              alignContent="flex-end"
              className={classes.otherInfoBlock}
            >
              <Typography className={classes.footerInfo}>
                最新更新日期：109-10-31
              </Typography>
              <Typography className={classes.footerInfo}>
                您是第 26565310位訪客
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default Footer;
