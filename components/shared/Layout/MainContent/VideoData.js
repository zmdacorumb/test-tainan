const VideoData = ({ resData }) => {
  return (
    <div id="videos-detail" className="row">
      <div className="video-iframe col-12 offset-lg-2 col-lg-8">
        <iframe
          width="100%"
          src={`https://www.youtube.com/embed/${resData && resData.VideoUrl}`}
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>
        {/* <p className="">影片長度: 30秒</p> */}

        <p>{resData && resData.VideoDescription}</p>
        {/* <hr /> */}
      </div>
      {/* <div className="col-12">
        <p className="update-time">
          更新日期：
          {moment(resData && resData.UpdateTime).format('YYYY-MM-DD')}
        </p>
      </div> */}
    </div>
  );
};
export default VideoData;
