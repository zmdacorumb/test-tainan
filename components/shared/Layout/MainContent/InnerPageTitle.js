import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  box: {
    display: 'flex',
    alignItems: 'center',
    paddingTop: '1rem',
    marginBottom: '20px',
  },
  icon: {
    height: '30px',
    [theme.breakpoints.down('md')]: { height: '25px' },
    [theme.breakpoints.down('sm')]: { height: '20px' },
  },
  iconButton: {
    [theme.breakpoints.down('md')]: { padding: '10px' },
    [theme.breakpoints.down('sm')]: { padding: '8px' },
  },
  divider: {
    display: 'inline-block',
    backgroundColor: '#98eadd',
    height: '40px',
    width: '5px',
    margin: '5px 8px',
    [theme.breakpoints.down('md')]: { height: '36px' },
    [theme.breakpoints.down('sm')]: { height: '32px' },
  },
  title: {
    userSelect: 'none',
    marginLeft: '10px',
    fontSize: '2.8rem',
    color: '#333333',
    [theme.breakpoints.down('md')]: { fontSize: '2.4rem' },
    [theme.breakpoints.down('sm')]: { fontSize: '2rem' },
  },
}));
const SubTitle = ({ title }) => {
  const classes = useStyles();
  const router = useRouter();
  const handleBack = () => {
    router.back();
  };
  return (
    <Box className={classes.box}>
      <IconButton
        className={classes.iconButton}
        aria-label="search"
        onClick={handleBack}
      >
        <img
          src="/static/img/icons/inner-page-back.png"
          alt="return image"
          className={classes.icon}
        />
      </IconButton>
      <div className={classes.divider} />
      <h4 className={classes.title}>{title || '-'}</h4>
    </Box>
  );
};

export default SubTitle;
