import { useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
  textField: {
    fontSize: '1.6rem',
    margin: theme.spacing(2, 0, 2),
    color: '#000',
    '& label': {
      fontSize: '1.6rem',
    },
    '& input': {
      fontSize: '1.6rem',
      padding: '18.5px 50px 18.5px 14px',
    },
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  iconButton: {
    position: 'absolute',
    top: '50%',
    right: '5%',
    padding: 10,
    transform: 'translateY(-50%)',
  },
}));
const ApiSearchInput = ({
  searchKeyword,
  setSearchKeyword,
  disabled,
  setCurrentPage,
  setOffset,
}) => {
  const inputRef = useRef(null);
  const classes = useStyles();
  const handleChange = () => {
    setSearchKeyword(inputRef.current.value);
    setCurrentPage(1);
    setOffset(0);
  };
  const handleKeyDown = (e) => {
    if (e.keyCode != 13) return;
    handleChange();
  };
  return (
    <div className="api-search-input-box">
      <TextField
        id="api-search-input"
        label="請輸入關鍵字"
        variant="outlined"
        className={classes.textField}
        inputRef={inputRef}
        onKeyDown={handleKeyDown}
        disabled={disabled}
      />
      <IconButton
        className={classes.iconButton}
        aria-label="search"
        onClick={handleChange}
      >
        <SearchIcon />
      </IconButton>
    </div>
  );
};

export default React.memo(ApiSearchInput);
