import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  tag: {
    backgroundColor: '#98eadd',
    width: 'fit-content',
    padding: '6px 24px',
    color: '#333',
    fontSize: '2rem',
    borderRadius: '5px',
    marginBottom: '12px',
  },
  small: {
    fontSize: '1.4rem',
    padding: '6px 16px',
  },
}));
const BlueTitleTag = ({ title, small }) => {
  const classes = useStyles();
  return (
    <div className={`${classes.tag} ${small ? classes.small : ''}`}>
      {title}
    </div>
  );
};

export default BlueTitleTag;
