import { useState } from 'react';

const ExpandBox = ({ question, answer }) => {
  const [show, toggleShow] = useState(false);

  return (
    <div className="expand-box-wrap mt-4">
      <div className="expand-box-question d-flex align-items-center">
        {show ? (
          <img
            onClick={() => toggleShow(!show)}
            src="/static/img/icons/minus.png"
            alt="plus-icon"
            className="mr-4"
            style={{ cursor: 'pointer' }}
          />
        ) : (
          <img
            onClick={() => toggleShow(!show)}
            src="/static/img/icons/plus.png"
            alt="plus-icon"
            className="mr-4"
            style={{ cursor: 'pointer' }}
          />
        )}
        {question}
      </div>
      <div
        className={
          show ? 'expand-box-answer mt-4 active' : ' expand-box-answer mt-4'
        }
      >
        {answer}
      </div>
      <div className="section-line-solid"></div>
    </div>
  );
};

export default ExpandBox;
