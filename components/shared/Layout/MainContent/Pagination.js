import ReactPaginate from 'react-paginate';

const Pagination = ({ data, handlePageClick, limit, pageNum }) => {
  const dataPerPage = limit && limit;
  const noOfPages = data && Math.ceil(data.total / dataPerPage);
  // console.log('pageNum', pageNum);
  // console.log('noOfPages', noOfPages);

  return (
    <section id="pagination" className="">
      <div className="pagination-box">
        {/* web版 */}
        <div className="row ">
          <div className="col-12 d-flex justify-content-center">
            <ul
              className="pagination"
              // onClick={() => {
              //   handlePageClick({ selected: 0 });
              // }}
            >
              {/* <li className={`previous ${pageNum == 1 && 'disabled'}`}> */}
              <li className={`previous disabled`}>
                <a tabIndex="0" role="button" aria-disabled="true">
                  最前頁
                </a>
              </li>
            </ul>
            {/* web版 */}
            <div className="d-none d-lg-block">
              <ReactPaginate
                // forcePage={pageNum - 1}
                previousLabel={'上一頁'}
                disabledClassName={'disabled'}
                nextLabel={'下一頁'}
                pageCount={noOfPages}
                marginPagesDisplayed={2} //頭尾顯視數量
                pageRangeDisplayed={2} //中間顯視數量
                // onPageChange={handlePageClick}
                containerClassName={'pagination'}
                subContainerClassName={'pages pagination'}
                activeClassName={'active'}
              />
            </div>
            {/* mobile */}
            <div className="d-block d-lg-none">
              <ReactPaginate
                // forcePage={pageNum - 1}
                previousLabel={'上一頁'}
                disabledClassName={'disabled'}
                nextLabel={'下一頁'}
                pageCount={noOfPages}
                marginPagesDisplayed={0} //頭尾顯視數量
                pageRangeDisplayed={0} //中間顯視數量
                // onPageChange={handlePageClick}
                containerClassName={'pagination'}
                subContainerClassName={'pages pagination'}
                activeClassName={'active'}
              />
            </div>
            <ul
              className="pagination"
              // onClick={() => {
              //   handlePageClick({ selected: noOfPages - 1 });
              // }}
            >
              {/* <li className={`previous ${pageNum == noOfPages && 'disabled'}`}> */}
              <li className={`previous disabled`}>
                <a tabIndex="0" role="button" aria-disabled="true">
                  最終頁
                </a>
              </li>
            </ul>
          </div>
        </div>

        <div className="row d-md-none">
          <div className="col-12 text-center previous">
            <p className="previous">頁數【1/12】資料筆數【118】</p>
            {/* {data && (
              <p>
                頁數【{pageNum ? pageNum : 1}/
                {noOfPages}】資料筆數【{data.total}】
              </p>
            )} */}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Pagination;
