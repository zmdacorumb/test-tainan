import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  title: {
    display: 'inline-flex',
    color: 'white',
    fontSize: '2.8rem',
    background: 'linear-gradient(to bottom, #0fd5d8 3%, #0191b4 100%);',
    padding: theme.spacing(1, 2),
    borderRadius: '6px',
    alignItems: 'center',
    '&:after': {
      display: 'inline-block',
      marginLeft: '20px',
      width: '38px',
      height: '38px',
      content: "''",
      background:
        "url('/static/img/icons/subtitle.png') center center/100% 100% no-repeat",
    },
    [theme.breakpoints.down('sm')]: {
      fontSize: '2rem',
    },
  },

  divider: {
    backgroundColor: '#0fd5d8',
    height: '2px',
    margin: theme.spacing(2, 'auto'),
  },
}));
const SubTitle = ({ title }) => {
  const classes = useStyles();
  return (
    <Box>
      <Typography className={classes.title} component="h3">
        {title}
      </Typography>
      <Divider className={classes.divider} />
    </Box>
  );
};

export default SubTitle;
