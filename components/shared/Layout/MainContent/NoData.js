import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  noData: {
    fontSize: '6.4rem',
    display: 'flex',
    minHeight: '620px',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'rgba(0,0,0,0.08)',
    fontWeight: '900',
    userSelect: 'none',
    [theme.breakpoints.down('sm')]: {
      minHeight: '400px',
      fontSize: '4rem',
    },
  },
}));
const NoData = () => {
  const classes = useStyles();

  return <div className={classes.noData}>No Data Found</div>;
};

export default NoData;
