import Link from 'next/link';

const Breadcrumbs = ({ bcLinks }) => {
  const getBreadcrumbsLink = (bc) => {
    let breadcrumbs = '';
    if (bc.path) {
      breadcrumbs = (
        <span className="breadcrumbs-link" key={bc.name}>
          <Link href={bc.path}>
            <a>{bc.name}</a>
          </Link>
          <img src="/static/img/icons/right-icon-gray.png" alt="" />
        </span>
      );
    } else {
      breadcrumbs = (
        <span className="breadcrumbs-link active" key={bc.name}>
          {bc.name}
        </span>
      );
    }

    return breadcrumbs;
  };

  return (
    <div id="breadcrumbs" className="row">
      <div className="col-12 ">
        {bcLinks ? bcLinks.map((bc) => getBreadcrumbsLink(bc)) : null}
      </div>
    </div>
  );
};

export default Breadcrumbs;
