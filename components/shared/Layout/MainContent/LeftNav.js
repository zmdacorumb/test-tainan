import Link from 'next/link';
import { leftNav } from '../../../../data/leftNav-data';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import { Fragment } from 'react';
const useStyles = makeStyles((theme) => ({
  box: {
    maxWidth: '220px',
    background: '#f3f0ec',
    borderRadius: '5px 5px 0 0',
    padding: '0',
    margin: '0 0 80px',
    position: 'relative',
    '&:before': {
      content: "''",
      width: '100%',
      height: '80px',
      display: 'block',
      background:
        "url('/static/img/bg/menu_bottom.png') center bottom / 100% 100% no-repeat",
      position: 'absolute',
      left: '0',
      bottom: ' -80px',
    },
  },
  blueTitle: {
    userSelect: 'none',
    width: '85%',
    margin: '0 auto 30px',
    fontSize: '2.2rem',
    fontWeight: '500',
    textAlign: 'center',
    background:
      "url('/static/img/bg/menu_title.png') center center / 100% 100% no-repeat",
    padding: '2rem 0 3.5rem',
  },
  listButton: {
    padding: theme.spacing(1, 4, 1, 3),
    color: '#333333',
    fontSize: '1.8rem',
  },
  externalLink: {
    background:
      "url('/static/img/icons/icon_link_menu.png') right 15px center no-repeat",
    paddingRight: '50px',
    // padding: '8px 50px 8px 8px',
  },
  currentLevel: {
    background:
      "url('/static/img/bg/menu_choice.png') center center / 100% 100% no-repeat",
  },
  divider: {
    margin: '5px 0px',
    backgroundColor: 'rgba(0,0,0,0.05)',
  },
}));
const LeftNav = ({ title, activeNav }) => {
  const classes = useStyles();
  const router = useRouter();
  const routerPathSplit = router.route.split('/');
  const levelAmount = routerPathSplit.length - 1; //所在層級，不包含首頁
  const levelOne = routerPathSplit[1];
  const levelTwo = routerPathSplit[2];
  const currentPageData = leftNav.find((item) => item.path == `/${levelOne}`);
  const titleName = currentPageData.title;

  if (levelAmount < 2) return null;
  return (
    <Box className={classes.box}>
      <h2 className={classes.blueTitle}>{titleName}</h2>
      <List>
        {currentPageData.subNav.map((item, index) => (
          <Fragment key={item.title}>
            {(item.path && (
              <Link href={item.path}>
                <ListItem
                  button
                  className={`${classes.listButton} ${
                    item.path.split('/')[2] == levelTwo
                      ? classes.currentLevel
                      : ''
                  }`}
                >
                  {item.title}
                </ListItem>
              </Link>
            )) || (
              <a
                href={item.externalLink}
                target="_blank"
                rel="noreferrer noopener"
              >
                <ListItem
                  button
                  className={`${classes.listButton} ${classes.externalLink}`}
                >
                  {item.title}
                </ListItem>
              </a>
            )}
            <Divider className={classes.divider} />
          </Fragment>
        ))}
      </List>
    </Box>
  );
  // return (
  //   <>
  //     <div className="left-nav text-left">
  //       <h2>{title}</h2>
  //       <ul>
  //         {leftNav
  //           .find((item) => item.title == title)
  //           .subNav.map((subItem) => (
  //             <li key={subItem.path}>
  //               <Link href={subItem.path}>
  //                 <a
  //                   className={
  //                     subItem.title == activeNav
  //                       ? subItem.title == '公共政策平臺' ||
  //                         subItem.title == '臺南市陳情系統'
  //                         ? 'file active'
  //                         : 'active'
  //                       : subItem.title == '公共政策平臺' ||
  //                         subItem.title == '臺南市陳情系統'
  //                       ? 'file'
  //                       : null
  //                   }
  //                 >
  //                   {subItem.title}
  //                 </a>
  //               </Link>
  //             </li>
  //           ))}
  //       </ul>
  //     </div>
  //   </>
};

export default LeftNav;
