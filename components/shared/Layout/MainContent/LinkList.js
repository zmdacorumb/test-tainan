import Link from 'next/link';
import { useRouter } from 'next/router';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    overflowX: 'scroll',
    width: '100%',
    minHeight: '620px',
    '& a': {
      display: 'block',
      minWidth: '600px',
    },
    '& a:nth-of-type(odd) .MuiListItem-button': {
      '&:before': {
        position: 'absolute',
        top: '0',
        left: '0',
        display: 'block',
        width: '104%',
        content: '""',
        height: '100%',
        backgroundSize: 'cover',
        backgroundPosition: 'right',
        backgroundRepeat: 'no-repeat',
        backgroundImage: 'url("/static/img/home/transport-black.png")',
      },
    },
    '& a:nth-of-type(even) .MuiListItem-button': {
      '&:before': {
        position: 'absolute',
        top: '0',
        left: '0',
        display: 'block',
        width: '104%',
        content: '""',
        height: '100%',
        backgroundSize: 'cover',
        backgroundPosition: 'right',
        backgroundRepeat: 'no-repeat',
        backgroundImage: 'url("/static/img/home/transport-light.png")',
      },
    },
  },
  listItem: {
    padding: theme.spacing(1, 10, 1, 2),
    color: 'rgba(0,0,0,0.5)',
    margin: theme.spacing(1, 0),
    width: '96%',
    position: 'relative',
    '&.Mui-focusVisible': {
      '&:after': {
        outline: '-webkit-focus-ring-color auto 1px',
        zIndex: '2',
        content: "''",
        position: 'absolute',
        width: '104%',
        height: '100%',
        top: '0',
        left: '0',
      },
    },
    '&:hover': {
      '&:after': {
        zIndex: '2',
        content: "''",
        position: 'absolute',
        width: '100%',
        height: '100%',
        top: '0',
        left: '0',
        backgroundColor: 'rgba(0,0,0,0.1)',
      },
    },
  },
  listItemText: {
    color: '#333333',
    zIndex: '2',
    '& span': {
      fontSize: '1.8rem',
      [theme.breakpoints.down('sm')]: {
        fontSize: '1.6rem',
      },
    },
  },
  externalLinkIcon: {
    position: 'absolute',
    right: '40px',
    opacity: '0.8',
  },
}));
const LinkList = ({ linkData }) => {
  const classes = useStyles();
  const router = useRouter();

  // console.log('router', router);
  console.log('linkData', linkData);
  return (
    <List
      component="nav"
      aria-labelledby="nested-list"
      className={classes.root}
      style={{ minHeight: '500px' }}
    >
      {linkData.map((item) =>
        item.Links || item.LinkUrl ? (
          <a
            key={item.ID}
            href={item.Links || item.LinkUrl}
            target="_blank"
            rel="noreferrer noopener"
          >
            <ListItem
              tabIndex="-1"
              button
              className={classes.listItem}
              // onKeyDown={enterRouter(item.externalLink)}
            >
              <ListItemText
                primary={item.Title || item.Name}
                className={classes.listItemText}
              />
              <img
                src="/static/img/home/ic_openlink_2.svg"
                alt="外部連結圖示"
                className={classes.externalLinkIcon}
              />
            </ListItem>
          </a>
        ) : item.innerLinks ? (
          <Link href={item.innerLinks}>
            <a>
              <ListItem tabIndex="-1" button className={classes.listItem}>
                <ListItemText
                  primary={item.Title}
                  className={classes.listItemText}
                />
              </ListItem>
            </a>
          </Link>
        ) : (
          <Link href={`${router.route}/detail?id=${item.ID}`}>
            <a>
              <ListItem tabIndex="-1" button className={classes.listItem}>
                <ListItemText
                  primary={item.Title || item.Name}
                  className={classes.listItemText}
                />
              </ListItem>
            </a>
          </Link>
        )
      )}
    </List>
  );
};
export default LinkList;
