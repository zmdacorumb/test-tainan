import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import selectData from '../../../../data/select-data';
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(2, 2, 0, 0),
    minWidth: 140,
    '& label': {
      fontSize: '1.6rem',
    },
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  selectRoot: {
    fontSize: '1.6rem',
    lineHeight: '20px',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  menuItem: {
    fontSize: '1.6rem',
  },
}));
const unitData = [
  { ID: '92B706A2-5F1F-4C43-9073-25D250EF46D0', title: '資訊部' },
  { ID: '682F5FA7-BADB-44ED-AAB6-FF2022FACC45', title: '人事室' },
  { ID: '73C20C9F-7972-46C2-9B32-440AC3E48B35', title: '公共運輸處' },
  { ID: '583CF429-CC10-4095-8EAC-80E60951BBD0', title: '公共關係室' },
  { ID: 'B94B5B84-03C6-48FF-97BA-486E8C516144', title: '交通工程科' },
  { ID: 'CCBA693D-987E-4BAD-9918-F3758C1517D7', title: '車鑑會' },
  { ID: 'E653DDA1-9EF1-4680-93B7-F42623E46395', title: '其他' },
  { ID: '8EAC8071-38D8-426E-835F-745289415106', title: '政風室' },
  { ID: '3538D3B6-6B2B-4456-BB35-4BF270DDF872', title: '秘書室' },
  { ID: 'A3CD068E-A564-4B0D-95F5-FE3F840D01F4', title: '停車管理部' },
  { ID: '2AB23F52-B33F-4FC9-A8AF-3CC3C3781FAB', title: '捷運工程處' },
  { ID: '3495874D-2763-462F-BA2F-BEB9D8927A18', title: '裁決中心' },
  { ID: 'A4FCA2B6-8983-4579-92CB-A0E1FA81DB08', title: '會計室' },
  { ID: 'CD963105-F3EA-4D81-99D4-2562B3081F93', title: '運輸管理科' },
  { ID: '88C9D9E7-9BD5-475F-84B5-C6687BAA151D', title: '綜合規劃科' },
  { ID: 'B8582AF6-1EA0-49AF-AEA7-3E746DC4434E', title: '網站管理' },
];
const ApiUnitSearchSelect = ({
  title,
  setCurrentPage,
  setOffset,

  departmentID,
  setDepartmentID,
}) => {
  const classes = useStyles();
  const handleChange = (event) => {
    setDepartmentID(event.target.value);
  };

  const selectList = selectData.find((item) => item.title == title);
  // console.log('title', title);
  // console.log('selectData', selectData);
  console.log('departmentID', departmentID);
  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel id="select-outlined-label">單位</InputLabel>
      <Select
        labelId="select-outlined-label"
        id="demo-simple-select-outlined"
        // value={age}
        onChange={(e) => handleChange(e)}
        label="Age"
        classes={{ root: classes.selectRoot }}
      >
        <MenuItem value="" className={classes.menuItem}>
          <em>全部</em>
        </MenuItem>

        {unitData.map((item, index) => (
          <MenuItem key={index} value={item.ID} className={classes.menuItem}>
            {item.title}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};
export default React.memo(ApiUnitSearchSelect);
