import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
// import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import selectData from '../../../../data/select-data';
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(2, 2, 0, 0),
    minWidth: 140,
    '& label': {
      fontSize: '1.6rem',
    },
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  selectRoot: {
    fontSize: '1.6rem',
    lineHeight: '20px',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  menuItem: {
    fontSize: '1.6rem',
  },
}));
const ApiSearchSelect = ({
  title,
  setCurrentPage,
  setOffset,
  categoryData,
  setCategoryData,
}) => {
  const classes = useStyles();
  const handleChange = (event) => {
    setCategoryData(event.target.value);
  };

  const selectList = selectData.find((item) => item.title == title);

  // console.log('categoryData', categoryData);
  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel id="select-outlined-label">分類</InputLabel>
      <Select
        labelId="select-outlined-label"
        id="demo-simple-select-outlined"
        // value={age}
        onChange={(e) => handleChange(e)}
        label="分類"
        classes={{ root: classes.selectRoot }}
      >
        <MenuItem value="" className={classes.menuItem}>
          <em>全部</em>
        </MenuItem>

        {selectList &&
          selectList.subNav.map((item, index) => (
            <MenuItem
              key={index}
              value={item.category}
              className={classes.menuItem}
            >
              {item.title}
            </MenuItem>
          ))}
      </Select>
    </FormControl>
  );
};
export default React.memo(ApiSearchSelect);
