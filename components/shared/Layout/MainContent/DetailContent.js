import { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import useSWR from 'swr';
import { fetchData } from '../../../../api/helper';

const useStyles = makeStyles((theme) => ({
  subTitle: {
    fontSize: '2.2rem',
    color: '#666666',
  },
  subContent: { fontSize: '1.8rem', margin: '10px 0px', color: '#666666' },
  imageBox: {
    margin: '20px 0px',
    padding: '20px',
  },
  tableContainer: {
    margin: theme.spacing(5, 0),
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',

      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
}));
const DetailContent = ({ data }) => {
  const classes = useStyles();
  return (
    <Box>
      {data.Title == '裁罰窗口等待人數查詢' && (
        <CurrentNumberAndWaitingNumber />
      )}
      {data.TrafficInfoContents?.map((item, index) => (
        <>
          <div className={classes.subTitle}>{item.SubTitle}</div>
          <div className={classes.subContent}>{item.Description}</div>
        </>
      ))}
      {data.ImagePath && (
        <div className="ImageBox" className={classes.imageBox}>
          <img src={`${process.env.URL}${data.ImagePath}`} alt="文章圖片" />
        </div>
      )}
      {data.CustomTable && (
        <TableContainer className={classes.tableContainer}>
          <Table aria-label="collapsible table" className={classes.table}>
            <TableHead>
              <TableRow className={classes.headRow}>
                {data.CustomTable.Head.map((item, index) => (
                  <TableCell align="center" key={index}>
                    {item}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {data.CustomTable.Row.map((item, index) => (
                <TableRow className={classes.bodyRow} key={index}>
                  {item.map((innerItem, innerIndex) => (
                    <TableCell align="center" key={innerIndex}>
                      {innerItem}
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
      {data.HtmlContent && (
        <div dangerouslySetInnerHTML={{ __html: data.HtmlContent }} />
      )}
    </Box>
  );
};
const CurrentNumberAndWaitingNumber = () => {
  const classes = useStyles();
  const { data, error } = useSWR(
    `http://211.79.183.82:8082/wait1.txt`,
    fetchData
  );
  useEffect(() => {
    const getData = async () => {
      let data;
      data = await fetchData(`211.79.183.82:8082/wait1.txt`);
      console.log('waiting effect', data);
    };
    getData();
  }, []);
  console.log('waiting data', data);

  // console.log('waiting data error', error);
  return (
    <TableContainer className={classes.tableContainer}>
      <Table aria-label="collapsible table" className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRow}>
            <TableCell align="center">辦公室名稱 </TableCell>
            <TableCell align="center">目前叫號</TableCell>
            <TableCell align="center">等待人數</TableCell>
            <TableCell align="center">資料更新時間</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow className={classes.bodyRow}>
            <TableCell align="center">臺南辦公室</TableCell>
            <TableCell align="center">-</TableCell>
            <TableCell align="center">-</TableCell>
            <TableCell align="center">-</TableCell>
          </TableRow>
          <TableRow className={classes.bodyRow}>
            <TableCell align="center">麻豆辦公室</TableCell>
            <TableCell align="center">-</TableCell>
            <TableCell align="center">-</TableCell>
            <TableCell align="center">-</TableCell>
          </TableRow>
          <TableRow className={classes.bodyRow}>
            <TableCell align="center">新營辦公室</TableCell>
            <TableCell align="center">-</TableCell>
            <TableCell align="center">-</TableCell>
            <TableCell align="center">-</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
};
export default DetailContent;
