import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

import GetAppIcon from '@material-ui/icons/GetApp';
const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
    '&:hover': {
      color: '#fff',
    },
  },
}));
const DownloadButton = ({ resData }) => {
  const classes = useStyles();
  return (
    <div className="row justify-content-center">
      <div className="col-lg-10">
        {resData.AttachedFiles.map((item) => (
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            endIcon={<GetAppIcon />}
            key={item.ParentID}
            href={item.FilePath}
            download
          >
            {item.FileName}
          </Button>
        ))}
      </div>
    </div>
  );
};
export default DownloadButton;
