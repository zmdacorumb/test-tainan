import Link from 'next/link';
import { useRouter } from 'next/router';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import selectData from '../../../../data/select-data';

const useStyles = makeStyles((theme) => ({
  root: {
    overflowX: 'scroll',
    width: '100%',
    minHeight: '620px',
    '& a': {
      flex: '1',
    },
  },
  flexBox: {
    minWidth: '1200px',
    display: 'flex',
    alignItems: 'center',
    '&:nth-of-type(odd) .MuiListItem-button': {
      '&:before': {
        position: 'absolute',
        top: '0',
        left: '0',
        display: 'block',
        width: '104%',
        content: '""',
        height: '100%',
        backgroundSize: 'cover',
        backgroundPosition: 'right',
        backgroundRepeat: 'no-repeat',
        backgroundImage: 'url("/static/img/home/transport-black.png")',
      },
    },
    '&:nth-of-type(even) .MuiListItem-button': {
      '&:before': {
        position: 'absolute',
        top: '0',
        left: '0',
        display: 'block',
        width: '104%',
        content: '""',
        height: '100%',
        backgroundSize: 'cover',
        backgroundPosition: 'right',
        backgroundRepeat: 'no-repeat',
        backgroundImage: 'url("/static/img/home/transport-light.png")',
      },
    },
    '&:nth-of-type(even) > div': {
      backgroundColor: 'rgba(248,248,248,1)',
    },
    '&:nth-of-type(odd) > div': {
      backgroundColor: 'rgba(236,236,236,1)',
    },
    [theme.breakpoints.down('sm')]: {
      minWidth: '1000px',
    },
  },
  category: {
    userSelect: 'none',
    width: '160px',
    display: 'inline-block',
    fontSize: '1.8rem',
    padding: '11px 16px',
    margin: '0px 8px 0px 0px',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.6rem',
    },
  },
  listItem: {
    padding: theme.spacing(1, 10, 1, 2),
    color: 'rgba(0,0,0,0.5)',
    margin: theme.spacing(1, 0),
    width: '96%',
    position: 'relative',
    '&.Mui-focusVisible': {
      '&:after': {
        outline: '-webkit-focus-ring-color auto 1px',
        zIndex: '2',
        content: "''",
        position: 'absolute',
        width: '104%',
        height: '100%',
        top: '0',
        left: '0',
      },
    },
    '&:hover': {
      '&:after': {
        zIndex: '2',
        content: "''",
        position: 'absolute',
        width: '100%',
        height: '100%',
        top: '0',
        left: '0',
        backgroundColor: 'rgba(0,0,0,0.1)',
      },
    },
  },
  listItemText: {
    color: '#333333',
    zIndex: '2',
    '& span': {
      fontSize: '1.8rem',
      [theme.breakpoints.down('sm')]: {
        fontSize: '1.6rem',
      },
    },
  },
  externalLinkIcon: {
    position: 'absolute',
    right: '40px',
    opacity: '0.8',
  },
}));
const LinkList = ({ linkData, title }) => {
  const selectList = selectData.find((item) => item.title == title);

  const classes = useStyles();
  const router = useRouter();
  console.log('selectList', selectList);
  return (
    <List
      component="nav"
      aria-labelledby="nested-list"
      className={classes.root}
      style={{ minHeight: '500px' }}
    >
      {linkData.map((item, index) =>
        item.Links || item.LinkUrl ? (
          <div className={classes.flexBox} key={index}>
            <div className={classes.category}>
              {
                selectList.subNav.find((itm) => itm.category == item.Category)
                  .title
              }
            </div>
            <a
              href={item.Links || item.LinkUrl}
              target="_blank"
              rel="noreferrer noopener"
            >
              <ListItem
                tabIndex="-1"
                button
                className={classes.listItem}
                // onKeyDown={enterRouter(item.externalLink)}
              >
                <ListItemText
                  primary={item.Title || item.Name}
                  className={classes.listItemText}
                />
                <img
                  src="/static/img/home/ic_openlink_2.svg"
                  alt="外部連結圖示"
                  className={classes.externalLinkIcon}
                />
              </ListItem>
            </a>
          </div>
        ) : item.innerLinks ? (
          <div className={classes.flexBox} key={index}>
            <div className={classes.category}>類別</div>
            <Link href={item.innerLinks}>
              <a>
                <ListItem tabIndex="-1" button className={classes.listItem}>
                  <ListItemText
                    primary={item.Title}
                    className={classes.listItemText}
                  />
                </ListItem>
              </a>
            </Link>
          </div>
        ) : (
          <div className={classes.flexBox} key={index}>
            <div className={classes.category}>
              {
                selectList.subNav.find(
                  (selectorItem) => selectorItem.category == item.Type
                ).title
              }
            </div>
            <Link href={`${router.route}/detail?id=${item.ID}`}>
              <a>
                <ListItem tabIndex="-1" button className={classes.listItem}>
                  <ListItemText
                    primary={item.Title || item.Name}
                    className={classes.listItemText}
                  />
                </ListItem>
              </a>
            </Link>
          </div>
        )
      )}
    </List>
  );
};
export default LinkList;
