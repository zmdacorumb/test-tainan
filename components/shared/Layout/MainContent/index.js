import LeftNav from './LeftNav';
import Breadcrumbs from './Breadcrumbs';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';

const useStyles = makeStyles((theme) => ({
  contentItem: {
    padding: '8px 16px',
  },
}));
const MainContent = ({
  content,
  title,
  noPadding,
  bcLinks,
  imgUrl = '',
  noLeftNav,
}) => {
  const classes = useStyles();
  return (
    <>
      <section className="main-content-wrap col-md-12 col-12 p-0">
        {noPadding ? (
          <div className="main-content p-0">{content}</div>
        ) : noLeftNav ? (
          <div className="main-content">
            <div
              className="banner-inner"
              style={{
                background: `url(${imgUrl}) no-repeat center center`,
                backgroundSize: 'cover',
                height: '30vw',
              }}
            ></div>

            <div className="container inner">
              <div className="row">
                <div className="col-12">
                  <Breadcrumbs bcLinks={bcLinks} />
                </div>

                <div className="col-12">{content}</div>
              </div>
            </div>
          </div>
        ) : (
          <div className="main-content">
            <div
              className="banner-inner"
              style={{
                background: `url(${imgUrl}) no-repeat center center`,
                backgroundSize: 'cover',
                height: '30vw',
              }}
            ></div>

            <div className="container inner">
              <Grid container>
                <Grid item xs={12}>
                  <Breadcrumbs bcLinks={bcLinks} />
                </Grid>
                <Hidden smDown>
                  <Grid item xs={false} sm={false} md={2} lg={2}>
                    <LeftNav title={title} activeNav={bcLinks[2]?.name} />
                  </Grid>
                </Hidden>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={10}
                  lg={10}
                  className={classes.contentItem}
                >
                  {content}
                </Grid>
              </Grid>
            </div>
          </div>
        )}
      </section>
    </>
  );
};

export default MainContent;
