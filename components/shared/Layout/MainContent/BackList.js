import Link from 'next/link';

const BackList = ({ }) => {
  return (
    <>
      <ul className="back-list">
        <li>
          <Link href="#">
            <a><b>上一則</b></a>
          </Link>
        </li>
        <li>
          <Link href="#">
            <a><b>回列表</b></a>
          </Link>
        </li>
        <li>
          <Link href="#">
            <a><b>下一則</b></a>
          </Link>
        </li>
      </ul>
    </>
  );
};

export default BackList;
