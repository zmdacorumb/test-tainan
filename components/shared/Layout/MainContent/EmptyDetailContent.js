import { makeStyles } from '@material-ui/core/styles';
import Skeleton from '@material-ui/lab/Skeleton';
const useStyles = makeStyles((theme) => ({
  skeleton: { margin: theme.spacing(3, 0) },
}));

const EmptyDetailContent = () => {
  const classes = useStyles();
  return (
    <Skeleton
      variant="rect"
      width={'100%'}
      height={620}
      className={classes.skeleton}
    />
  );
};
export default EmptyDetailContent;
