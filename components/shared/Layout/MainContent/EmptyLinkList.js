import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { makeStyles } from '@material-ui/core/styles';

import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  listItem: {
    padding: '0px',
    margin: theme.spacing(1, 0),
    width: '96%',
    position: 'relative',
    '&:nth-of-type(odd) .MuiSkeleton-root': {
      backgroundColor: 'rgba(236,236,236,0.8)',
    },
    '&:nth-of-type(even) .MuiSkeleton-root': {
      backgroundColor: 'rgba(248,248,248,0.8)',
    },
  },
}));
const EmptyLinkList = ({ limit }) => {
  const classes = useStyles();

  return (
    <List
      component="nav"
      aria-labelledby="nested-list"
      className={classes.root}
    >
      {Array(limit)
        .fill('')
        .map((item, index) => (
          <ListItem
            tabIndex="-1"
            button
            className={classes.listItem}
            key={index}
          >
            <Skeleton
              variant="rect"
              width={'100%'}
              height={51}
              animation="wave"
              // className={classes.skeleton}
            />
          </ListItem>
        ))}
    </List>
  );
};
export default EmptyLinkList;
