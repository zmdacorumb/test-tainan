import Link from 'next/link';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
const useStyles = makeStyles((theme) => ({
  gridItem: {
    padding: '10px 20px',
    [theme.breakpoints.down('md')]: {
      padding: '10px 20px',
    },
  },
  button: {
    width: '100%',
    background:
      "url('/static/img/home/gray-arrow.png') center center/100% 100% no-repeat",
    fontSize: '1.8rem',
    position: 'relative',
    '&:before': {
      height: '100%',
      content: "''",
      position: 'absolute',
      display: 'block',
      top: '0px',
      left: '0px',
      width: '10px',
      backgroundColor: '#ffcc34',
    },
    '& .MuiButton-label': {
      justifyContent: 'left',
      padding: theme.spacing(0, 4, 0, 4),
      textAlign: 'left',
    },
  },
}));
const ArrowLink = ({ data }) => {
  const classes = useStyles();
  return (
    <Grid item xs={12} sm={6} md={4} lg={3} className={classes.gridItem}>
      {data.externalLink ? (
        <a href={data.externalLink} target="_blank" rel="noreferrer noopener">
          <Button className={classes.button}>{data.title}</Button>
        </a>
      ) : (
        <Link href={data.path}>
          <Button className={classes.button}>{data.title}</Button>
        </Link>
      )}
    </Grid>
  );
};
export default ArrowLink;
