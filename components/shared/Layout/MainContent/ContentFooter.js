import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import unit from '../../../../data/unit-data';
const useStyles = makeStyles((item) => ({
  root: {
    minWidth: '3rem',
  },
}));
const ContentFooter = ({ update, publishUnit }) => {
  const handleShate = (url) => {
    window.open(`https://www.facebook.com/sharer.php?u=${url}`);
  };
  const classes = useStyles();

  let unitData;
  if (publishUnit) {
    publishUnit = publishUnit.toUpperCase(); //小寫轉大寫
    unitData = unit.find((item) => item.id == publishUnit);
  }

  return (
    <div className="fb-print-box">
      <Button
        classes={{ root: classes.root }}
        onClick={() => handleShate(document.URL)}
      >
        <img src="/static/img/icons/Component 15 – 3.png" alt="" />
      </Button>
      <Button
        classes={{ root: classes.root }}
        onClick={() => {
          print();
        }}
      >
        <img src="/static/img/icons/Component 16 – 3.png" alt="" />
      </Button>
      <hr />
      {update && (
        <div className="table-update">
          {update && (
            <span>更新日期:{moment(update).format('YYYY-MM-DD')}</span>
          )}
        </div>
      )}
      {publishUnit && <div>發佈單位:{unitData && unitData.title}</div>}
    </div>
  );
};
export default ContentFooter;
