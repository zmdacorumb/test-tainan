import { useState } from 'react';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
// import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import FAQDialog from '../../../components/shared/Dialog/FAQDialog';
const useStyles = makeStyles((theme) => ({
  tableContainer: {
    margin: theme.spacing(5, 0),
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',
      '&:nth-child(1)': {
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:nth-child(2)': {
        minWidth: '130px',
        width: '130px',
        maxWidth: '130px',
      },
      '&:nth-child(3)': {
        minWidth: '150px',
        width: '150px',
        maxWidth: '150px',
      },
      '&:nth-child(4)': {
        minWidth: '300px',
        position: 'relative',
        lineHeight: '26px',
        padding: '27px 54px 27px 18px',
      },
      '&:nth-child(5)': {
        padding: '15.5px 16px',
        height: '80px',
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
  yellowFrame: {
    padding: theme.spacing(0.5, 2),
    border: '1px solid #fecc46',
    display: 'inline-block',
    borderRadius: '5px',
  },
  downloadButton: {
    width: '49px',
  },
  collapse: {
    marginTop: '12px',
  },
  collapseButton: {
    position: 'absolute',
    top: '22px',
    right: '18px',
    width: '36px',
    height: '36px',
  },
  relativeLinks: {
    marginTop: '12px',
  },
  relativeLinksItem: {
    '&::after': {
      content: '"、"',
    },
    '&:last-child::after': {
      content: '""',
    },
  },
  dialogDiv: { marginTop: '12px', textAlign: 'right' },
  dialogButton: {},
}));

const FAQTable = ({ FAQData }) => {
  const classes = useStyles();
  return (
    <TableContainer className={classes.tableContainer}>
      <Table aria-label="collapsible table" className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRow}>
            <TableCell align="center">序號</TableCell>
            <TableCell align="center">上架日期</TableCell>
            <TableCell align="center">分類</TableCell>
            <TableCell align="center">標題</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {FAQData.map((item, index) => (
            <React.Fragment key={index}>
              <CustomizedRow row={item} />
            </React.Fragment>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const CustomizedRow = ({ row }) => {
  const [open, setOpen] = React.useState(false);
  const [dialogDisplay, setDialogDisplay] = useState(false);
  const [dialogID, setDialogID] = useState('');
  const classes = useStyles();
  const handleDialog = (ID) => (e) => {
    setDialogDisplay(true);
    setDialogID(ID);
  };
  const dataCategory = [
    '公共運輸',
    '交通工程',
    '停車管理',
    '無人機管理',
    '違規裁罰',
    '綜合規劃',
  ];
  return (
    <TableRow className={classes.bodyRow}>
      <TableCell align="center">{row.SequenceNo}</TableCell>
      <TableCell align="center">
        {moment(row.UpdateTime).format('YYYY-MM-DD')}
      </TableCell>
      <TableCell align="center">
        <Typography variant="span" className={classes.yellowFrame}>
          {dataCategory[Number(row.Category) - 1]}
        </Typography>
      </TableCell>
      <TableCell>
        {row.Question}
        <IconButton
          className={classes.collapseButton}
          onClick={() => setOpen(!open)}
        >
          {open ? (
            <img src="/static/img/icons/collapse-open.png" alt="已打開的圖示" />
          ) : (
            <img
              src="/static/img/icons/collapse-close.png"
              alt="已關閉的圖示"
            />
          )}
        </IconButton>
        <Collapse
          in={open}
          timeout="auto"
          unmountOnExit
          className={classes.collapse}
        >
          {row.Answer}
          {row.Links && (
            <div className={classes.relativeLinks}>
              相關連結：
              {row.Links.map((item, index) => (
                <a
                  className={classes.relativeLinksItem}
                  href={item.LinkUrl}
                  key={index}
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  <span>{item.Name}</span>
                </a>
              ))}
            </div>
          )}
          {row.AttachedFiles && (
            <div className={classes.dialogDiv}>
              <Button
                className={classes.dialogButton}
                onClick={handleDialog(row.ID)}
              >
                more
              </Button>
              <FAQDialog
                dialogTitle={row.Question}
                dialogID={dialogID}
                dialogDisplay={dialogDisplay}
                setDialogDisplay={setDialogDisplay}
              />
            </div>
          )}
        </Collapse>
      </TableCell>
    </TableRow>
  );
};

export default FAQTable;
