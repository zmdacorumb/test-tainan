import { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import BlueTitleTag from '../../../components/shared/Layout/MainContent/BlueTitleTag';
const useStyles = makeStyles((theme) => ({
  tableBox: {
    paddingTop: '40px',
  },
  blueTitleTag: {
    marginTop: '40px',
  },
  tableContainer: {
    margin: theme.spacing(4, 0, 10, 0),
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',
      wordBreak: 'break-all',
      '&:nth-child(1)': {
        minWidth: '200px',
        width: '200px',
        maxWidth: '200px',
      },
      '&:nth-child(2)': {
        minWidth: '130px',
        width: '130px',
        maxWidth: '130px',
      },
      '&:nth-child(3)': {
        minWidth: '250px',
        width: '250px',
        maxWidth: '250px',
      },

      '&:nth-child(4)': {
        minWidth: '200px',
        width: '200px',
        maxWidth: '200px',
      },
      '&:nth-child(5)': {
        minWidth: '120px',
        width: '120px',
        maxWidth: '120px',
      },
      '&:nth-child(6)': {
        minWidth: '120px',
        width: '120px',
        maxWidth: '120px',
      },
      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
  yellowFrame: {
    padding: theme.spacing(0.5, 2),
    border: '1px solid #fecc46',
    display: 'inline-block',
    borderRadius: '5px',
    marginTop: '0 !important',
  },
  downloadButton: {
    width: '49px',
  },
  linkButton: {
    width: '49px',
  },
  collapseButton: {
    position: 'absolute',
    top: '22px',
    right: '18px',
    width: '36px',
    height: '36px',
  },
}));

const RecruitDiversion = ({ resData }) => {
  const [categoryMoney, setCategoryMoney] = useState([]);
  const [categorySupplies, setCategorySupplies] = useState([]);
  const [categoryRealEstate, setCategoryRealEstate] = useState([]);
  useEffect(() => {
    if (resData.length == 0) return;
    setCategoryMoney(resData.filter((item) => item.Category == '1'));
    setCategorySupplies(resData.filter((item) => item.Category == '2'));
    setCategoryRealEstate(resData.filter((item) => item.Category == '3'));
  }, [resData]);
  const classes = useStyles();

  return (
    <div className={classes.tableBox}>
      {categoryMoney.length > 0 && (
        <CategoryMoneyTable resData={categoryMoney} />
      )}
      {categorySupplies.length > 0 && (
        <CategorySuppliesTable resData={categorySupplies} />
      )}
      {categoryRealEstate.length > 0 && (
        <CategoryRealEstateTable resData={categoryRealEstate} />
      )}
    </div>
  );
};

const CategoryMoneyTable = ({ resData }) => {
  const classes = useStyles();
  return (
    <>
      <BlueTitleTag
        title="臺南市政府交通局捐贈清冊(金錢)"
        className={classes.blueTitleTag}
      />
      <TableContainer className={classes.tableContainer}>
        <Table aria-label="collapsible table" className={classes.table}>
          <TableHead>
            <TableRow className={classes.headRow}>
              <TableCell align="center">捐贈者名稱</TableCell>
              <TableCell align="center">捐贈日期</TableCell>
              <TableCell align="center">捐贈用途</TableCell>
              <TableCell align="center">捐贈金額</TableCell>
              <TableCell align="center">收據編號</TableCell>
              <TableCell align="center">辦理情形</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {resData.map((item) => (
              <CustomizedRow key={item.name} row={item} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};
const CategorySuppliesTable = ({ resData }) => {
  const classes = useStyles();
  return (
    <>
      <BlueTitleTag title="臺南市政府交通局捐贈清冊(物資或動產)" />
      <TableContainer className={classes.tableContainer}>
        <Table aria-label="collapsible table" className={classes.table}>
          <TableHead>
            <TableRow className={classes.headRow}>
              <TableCell align="center">捐贈者名稱</TableCell>
              <TableCell align="center">捐贈日期</TableCell>
              <TableCell align="center">捐贈用途</TableCell>
              <TableCell align="center">捐贈物資</TableCell>
              <TableCell align="center">物資數量</TableCell>
              <TableCell align="center">物資時價</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {resData.map((item) => (
              <CustomizedRow key={item.name} row={item} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};
const CategoryRealEstateTable = ({ resData }) => {
  const classes = useStyles();
  return (
    <>
      <BlueTitleTag title="臺南市政府交通局捐贈清冊(不動產)" />
      <TableContainer className={classes.tableContainer}>
        <Table aria-label="collapsible table" className={classes.table}>
          <TableHead>
            <TableRow className={classes.headRow}>
              <TableCell align="center">捐贈者名稱</TableCell>
              <TableCell align="center">捐贈日期</TableCell>
              <TableCell align="center">捐贈用途</TableCell>
              <TableCell align="center">座落位置</TableCell>
              <TableCell align="center">面積</TableCell>
              <TableCell align="center">權利範圍</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {resData.map((item, index) => (
              <React.Fragment key={index}>
                <CustomizedRow row={item} />
              </React.Fragment>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

const CustomizedRow = ({ row }) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  return (
    <TableRow className={classes.bodyRow}>
      <TableCell align="center">{row.Donor}</TableCell>
      <TableCell align="center">
        {moment(row.UpdateTime).format('YYYY-MM-DD')}
      </TableCell>
      <TableCell align="center">{row.DonationUse}</TableCell>
      <TableCell align="center">{row.DonationData1}</TableCell>
      <TableCell align="center">{row.DonationData2}</TableCell>
      <TableCell align="center">{row.DonationData3}</TableCell>
    </TableRow>
  );
};

export default RecruitDiversion;
