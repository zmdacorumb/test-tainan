import React from 'react';
import { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ContactMethodDialog from '../Dialog/ContactMethodDialog';
const useStyles = makeStyles((theme) => ({
  tableContainer: {
    margin: theme.spacing(5, 0),
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',
      '&:nth-child(1)': {
        minWidth: '200px',
        width: '200px',
        maxWidth: '200px',
      },
      '&:nth-child(2)': {
        minWidth: '200px',
        width: '200px',
        maxWidth: '200px',
      },
      '&:nth-child(3)': {
        minWidth: '240px',
        width: '240px',
        maxWidth: '240px',
      },
      '&:nth-child(4)': {
        minWidth: '240px',
        width: '240px',
        maxWidth: '240px',
      },
      '&:nth-child(5)': {
        minWidth: '300px',
        position: 'relative',
        lineHeight: '26px',
        padding: '27px 54px 27px 18px',
      },
      '&:nth-child(6)': {
        padding: '15.5px 16px',
        height: '80px',
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:nth-child(7)': {
        minWidth: '110px',
        width: '110px',
        maxWidth: '110px',
      },
      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
  yellowFrame: {
    padding: theme.spacing(0.5, 2),
    border: '1px solid #fecc46',
    display: 'inline-block',
    borderRadius: '5px',
  },
  downloadButton: {
    width: '49px',
  },
  linkButton: {
    width: '49px',
  },
  collapseButton: {
    position: 'absolute',
    top: '22px',
    right: '18px',
    width: '36px',
    height: '36px',
  },
  dialogButton: {
    fontSize: '1.8rem',
    wordBreak: 'keep-all',
  },
}));

const CMTable = ({ CMData }) => {
  const [dialogDisplay, setDialogDisplay] = useState(false);
  const [contactID, setContactID] = useState('');
  const [dialogTitle, setDialogTitle] = useState('');
  const classes = useStyles();
  return (
    <>
      <TableContainer className={classes.tableContainer}>
        <Table aria-label="collapsible table" className={classes.table}>
          <TableHead>
            <TableRow className={classes.headRow}>
              <TableCell align="center">單位名稱</TableCell>
              <TableCell align="center">主管姓名</TableCell>
              <TableCell align="center">電話</TableCell>
              <TableCell align="center">傳真</TableCell>
              <TableCell align="center">地址</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {CMData.map((item, index) => (
              <React.Fragment key={index}>
                <CustomizedRow
                  row={item}
                  setContactID={setContactID}
                  setDialogDisplay={setDialogDisplay}
                  setDialogTitle={setDialogTitle}
                />
              </React.Fragment>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <ContactMethodDialog
        dialogDisplay={dialogDisplay}
        setDialogDisplay={setDialogDisplay}
        contactID={contactID}
        dialogTitle={dialogTitle}
      />
    </>
  );
};

const CustomizedRow = ({
  row,
  setContactID,
  setDialogDisplay,
  setDialogTitle,
}) => {
  const classes = useStyles();
  const handleButton = (ID, DepartmentName) => (e) => {
    setContactID(ID);
    setDialogTitle(DepartmentName);
    setDialogDisplay(true);
  };
  return (
    <TableRow className={classes.bodyRow}>
      <TableCell align="center">
        <div dangerouslySetInnerHTML={{ __html: row.DepartmentName }} />
      </TableCell>
      <TableCell align="center">
        <div dangerouslySetInnerHTML={{ __html: row.LeaderName }} />
      </TableCell>
      <TableCell align="center">
        <div
          dangerouslySetInnerHTML={{
            __html: row.Phone.replace(/\//g, '<br />'),
          }}
        />
      </TableCell>
      <TableCell align="center">
        <div dangerouslySetInnerHTML={{ __html: row.Fax }} />
      </TableCell>
      <TableCell>
        <Button
          className={classes.dialogButton}
          onClick={handleButton(row.ID, row.DepartmentName)}
        >
          {row.OrganDirectoryName}
        </Button>
      </TableCell>
    </TableRow>
  );
};

export default CMTable;
