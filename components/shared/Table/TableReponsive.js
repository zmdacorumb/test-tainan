const TableResponsive = ({ data }) => {
  console.log('Table Data', data);

  return (
    <div className="table-responsive">
      <table className="table table-custom">
        <thead>
          <tr>
            <th rowSpan="2">起站</th>
            <th rowSpan="2">訖站</th>
            <th rowSpan="2">單程票票價</th>
            <th rowSpan="2">電子票證</th>
            <th rowSpan="2">敬老卡、愛心卡、愛心陪伴卡</th>
            <th rowSpan="2">乘車時間(分鐘)</th>
          </tr>
        </thead>

        <tbody>
          {data.length !== 0 ? (
            data.map((rowData) => (
              <tr>
                <td>{rowData.StartSiteName}</td>
                <td>{rowData.StopSiteName}</td>
                <td>{rowData.GeneralFare}</td>
                <td>{rowData.ElectronicFare}</td>
                <td>{rowData.RespectFare}</td>
                <td>{rowData.SpendTime}</td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan="22">沒有資料</td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default TableResponsive;
