import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
// import Box from '@material-ui/core/Box';
// import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
// import Typography from '@material-ui/core/Typography';
import moment from 'moment';
const useStyles = makeStyles((theme) => ({
  tableContainer: {
    margin: '20px 0px 40px 0px',
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',
      '&:nth-child(1)': {
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:nth-child(2)': {
        minWidth: '200px',
        width: '200px',
        maxWidth: '200px',
      },
      '&:nth-child(3)': {
        minWidth: '240px',
        width: '240px',
        maxWidth: '240px',
      },
      '&:nth-child(4)': {
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
  yellowFrame: {
    padding: theme.spacing(0.5, 2),
    border: '1px solid #fecc46',
    display: 'inline-block',
    borderRadius: '5px',
  },
  downloadButton: {
    width: '49px',
  },
  linkButton: {
    width: '49px',
  },
  collapseButton: {
    position: 'absolute',
    top: '22px',
    right: '18px',
    width: '36px',
    height: '36px',
  },
}));

const ICSTable = ({ ICSData }) => {
  const classes = useStyles();
  return (
    <TableContainer className={classes.tableContainer}>
      <Table aria-label="collapsible table" className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRow}>
            <TableCell align="center">序號</TableCell>
            <TableCell align="center">發布日期</TableCell>
            <TableCell align="center">檔案名稱</TableCell>
            <TableCell align="center">下載</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {ICSData.map((item, index) => (
            <React.Fragment key={index}>
              <CustomizedRow row={item} index={index} />
            </React.Fragment>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const CustomizedRow = ({ row, index }) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  return (
    <TableRow className={classes.bodyRow}>
      <TableCell align="center">
        <div dangerouslySetInnerHTML={{ __html: row.SequenceNo }} />
      </TableCell>
      <TableCell align="center">
        {moment(row.UpdateTime).format('YYYY-MM-DD')}
      </TableCell>
      <TableCell align="center">
        <div dangerouslySetInnerHTML={{ __html: row.Title }} />
      </TableCell>
      <TableCell align="center">
        {row.FilePath && (
          <IconButton className={classes.downloadButton}>
            <a
              href={`${process.env.URL}/${row.FilePath}`}
              download
              target="_blank"
              rel="noreferrer noopener"
            >
              <img src="/static/img/icons/download-icon.png" alt="下載圖示" />
            </a>
          </IconButton>
        )}
      </TableCell>
    </TableRow>
  );
};

export default ICSTable;
