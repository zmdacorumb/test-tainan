import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
// import Box from '@material-ui/core/Box';
// import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
const useStyles = makeStyles((theme) => ({
  tableContainer: {
    margin: theme.spacing(5, 0),
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',
      '&:nth-child(1)': {
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:nth-child(2)': {
        minWidth: '130px',
        width: '130px',
        maxWidth: '130px',
      },
      // '&:nth-child(3)': {
      //   minWidth: '180px',
      //   width: '180px',
      //   maxWidth: '180px',
      // },

      '&:nth-child(3)': {
        minWidth: '300px',
        position: 'relative',
        lineHeight: '26px',
        padding: '27px 54px 27px 18px',
      },
      '&:nth-child(4)': {
        padding: '15.5px 16px',
        height: '80px',
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      // '&:nth-child(6)': {
      //   minWidth: '80px',
      //   width: '80px',
      //   maxWidth: '80px',
      // },
      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
  yellowFrame: {
    padding: theme.spacing(0.5, 2),
    border: '1px solid #fecc46',
    display: 'inline-block',
    borderRadius: '5px',
  },
  downloadButton: {
    width: '49px',
  },
  linkButton: {
    width: '49px',
  },
  collapseButton: {
    position: 'absolute',
    top: '22px',
    right: '18px',
    width: '36px',
    height: '36px',
  },
}));

const GMTable = ({ GMData }) => {
  const classes = useStyles();
  return (
    <TableContainer className={classes.tableContainer}>
      <Table aria-label="collapsible table" className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRow}>
            <TableCell align="center">序號</TableCell>
            <TableCell align="center">發布日期</TableCell>
            <TableCell align="center">檔案名稱</TableCell>
            <TableCell align="center">下載</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {GMData.map((item, index) => (
            <React.Fragment key={index}>
              <CustomizedRow row={item} />
            </React.Fragment>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const CustomizedRow = ({ row }) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  return (
    <TableRow className={classes.bodyRow}>
      <TableCell align="center">{row.SequenceNo}</TableCell>
      <TableCell align="center">
        {moment(row.UpdateTime).format('YYYY-MM-DD')}
      </TableCell>

      <TableCell align="center">{row.Name}</TableCell>
      <TableCell align="center">
        {row.FilePath && (
          <a
            href={`${process.env.URL}/${row.FilePath}`}
            target="_blank"
            rel="noreferrer noopener"
          >
            <IconButton className={classes.downloadButton}>
              <img src="/static/img/icons/download-icon.png" alt="下載圖示" />
            </IconButton>
          </a>
        )}
      </TableCell>
    </TableRow>
  );
};

export default GMTable;
