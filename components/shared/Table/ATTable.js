import React from 'react';
import Link from 'next/link';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import moment from 'moment';
const useStyles = makeStyles((theme) => ({
  tableContainer: {
    margin: '20px 0px 40px 0px',
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',
      '&:nth-child(1)': {
        minWidth: '60px',
        width: '60px',
        maxWidth: '60px',
      },
      '&:nth-child(2)': {
        minWidth: '200px',
        width: '200px',
        maxWidth: '200px',
      },
      '&:nth-child(3)': {
        // minWidth: '240px',
        // width: '240px',
        // maxWidth: '240px',
      },

      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
  yellowFrame: {
    padding: theme.spacing(0.5, 2),
    border: '1px solid #fecc46',
    display: 'inline-block',
    borderRadius: '5px',
  },
  downloadButton: {
    width: '49px',
  },
  linkButton: {
    width: '49px',
  },
  collapseButton: {
    position: 'absolute',
    top: '22px',
    right: '18px',
    width: '36px',
    height: '36px',
  },
  link: {
    color: 'rgba(0,0,0,0.7)',
    '&:hover': {
      color: 'rgba(37,92,153,0.9)',
    },
  },
}));

const ATTable = ({ ATData }) => {
  const classes = useStyles();
  return (
    <TableContainer className={classes.tableContainer}>
      <Table aria-label="collapsible table" className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRow}>
            <TableCell align="center">序號</TableCell>
            <TableCell align="center">發布日期</TableCell>
            <TableCell align="center">檔案名稱</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {ATData.map((item, index) => (
            <React.Fragment key={index}>
              <CustomizedRow row={item} />
            </React.Fragment>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const CustomizedRow = ({ row }) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  return (
    <TableRow className={classes.bodyRow}>
      <TableCell align="center">{row.Sequence}</TableCell>
      <TableCell align="center">
        {moment(row.UpdateTime).format('YYYY-MM-DD')}
      </TableCell>
      <TableCell align="left">
        <Link href={`/transportation/traffic-control/detail?id=${row.ID}`}>
          <a className={classes.link}>{row.Title}</a>
        </Link>
      </TableCell>
    </TableRow>
  );
};

export default ATTable;
