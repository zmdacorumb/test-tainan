import { makeStyles } from '@material-ui/core/styles';
// import Box from '@material-ui/core/Box';
// import Collapse from '@material-ui/core/Collapse';
// import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
// import Typography from '@material-ui/core/Typography';
// import { Divider } from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
  tableContainer: {
    margin: theme.spacing(5, 0),
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',
      '&:nth-child(1)': {
        minWidth: '200px',
        width: '200px',
        maxWidth: '200px',
      },
      // '&:nth-child(2)': {
      //   minWidth: '200px',
      //   width: '200px',
      //   maxWidth: '200px',
      // },
      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
  yellowFrame: {
    padding: theme.spacing(0.5, 2),
    border: '1px solid #fecc46',
    display: 'inline-block',
    borderRadius: '5px',
  },
  downloadButton: {
    width: '49px',
  },
  linkButton: {
    width: '49px',
  },
  collapseButton: {
    position: 'absolute',
    top: '22px',
    right: '18px',
    width: '36px',
    height: '36px',
  },
}));

const CATable = ({ OHData }) => {
  const classes = useStyles();
  return (
    <TableContainer className={classes.tableContainer}>
      <Table aria-label="collapsible table" className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRow}>
            <TableCell>日期</TableCell>
            <TableCell>內容</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {OHData.map((item, index) => (
            <React.Fragment key={index}>
              <CustomizedRow row={item} />
            </React.Fragment>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const CustomizedRow = ({ row }) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  const getChineseDateFormat = (date) => {
    const transferDate = new Date(date);
    return `${transferDate.getFullYear() - 1911}年${
      transferDate.getMonth() + 1
    }月${transferDate.getDate()}日`;
  };
  return (
    <TableRow className={classes.bodyRow}>
      <TableCell>
        {/* {moment(row.HistoryDate).add(-1911, 'years').format('yyyy-MM-DD')} */}
        {getChineseDateFormat(row.HistoryDate)}
      </TableCell>
      <TableCell>
        {row.Description.split('\n').map((item) => (
          <div>{item}</div>
        ))}
      </TableCell>
    </TableRow>
  );
};

export default CATable;
