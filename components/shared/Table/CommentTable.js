import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  confirmBtn: {
    padding: '2px 30px',
    color: '#fff',
    fontSize: '2rem',
    backgroundColor: '#00a49b',
    '&:hover': {
      backgroundColor: 'rgba(0,134,135,1.0)',
    },
    '& + &': {
      marginLeft: '20px',
    },
  },
  gray: {
    backgroundColor: '#aaa',
    cursor: 'not-allowed !important',
    '&:hover': {
      backgroundColor: '#aaa',
    },
  },
  clear: {
    backgroundColor: '#aaa',
    '&:hover': {
      backgroundColor: 'rgba(0,134,135,1.0)',
    },
  },
  address: {
    padding: '0 10px',
    '& + &': {
      marginLeft: '20px',
    },
  },
}));

const CommentTable = ({
  values,
  errors,
  handleChange,
  touched,
  selectMainCategory,
  selectSubCategory,
  handleClear,
}) => {
  const classes = useStyles();
  return (
    <div className="table-box">
      <div className="row">
        <span className="list-title">
          <span className="circle"></span>姓名
        </span>
        <input
          type="text"
          className="col-md"
          id="ApplicantName"
          name="ApplicantName"
          onChange={handleChange}
          value={values.ApplicantName}
        />
        {errors.ApplicantName && touched.ApplicantName && (
          <div className="form-validation col-12 col-md-2 my-2">
            {errors.ApplicantName}
          </div>
        )}
      </div>
      <div className="row other">
        <span className="list-title">
          <span className="circle"></span>稱謂
        </span>
        <div className="col-10 col-md radio-box">
          <input
            type="radio"
            name="ApplicantTitle"
            id="mis"
            value="女士"
            defaultChecked
          />
          <label htmlFor="mis">女士</label>
          <input
            type="radio"
            name="ApplicantTitle"
            id="sir"
            value="先生"
            onChange={handleChange}
          />
          <label htmlFor="sir">先生</label>
          <input
            type="radio"
            name="ApplicantTitle"
            id="dr"
            value="博士"
            onChange={handleChange}
          />
          <label htmlFor="dr">博士</label>
          <input
            type="radio"
            name="ApplicantTitle"
            id="student"
            value="同學"
            onChange={handleChange}
          />
          <label htmlFor="student">同學</label>
          <input
            type="radio"
            name="ApplicantTitle"
            id="notProvided"
            value="不提供"
            onChange={handleChange}
          />
          <label htmlFor="notProvided">不提供</label>
        </div>
      </div>
      <div className="row">
        <span className="list-title">
          <span className="circle"></span>Email
        </span>

        <input
          type="email"
          className="col-md"
          id="Email"
          name="Email"
          placeholder="(xxx@xxx.xxx)"
          value={values.Email}
          onChange={handleChange}
        />
        {errors.Email && touched.Email && (
          <div className="form-validation col-12 col-md-2 my-2">
            {errors.Email}
          </div>
        )}
      </div>
      <div className="row">
        <span className="list-title">
          <span className="circle"></span>聯絡電話
        </span>

        <input
          type="text"
          className="col-md"
          id="Phone"
          name="Phone"
          placeholder="(行動或市內電話 ex:0970123456 or 021123456)"
          // pattern="[+]{1}[0-9]{11,14}"
          value={values.Phone}
          onChange={handleChange}
        />
        {errors.Phone && touched.Phone && (
          <div className="form-validation col-12 col-md-2 my-2">
            {errors.Phone}
          </div>
        )}
      </div>
      <div className="row select">
        <span className="list-title">
          <span className="circle"></span>地址
        </span>
        <div className="city-selector-set d-flex">
          <div>
            <select
              id="county"
              className={`county ${classes.address}`}
              name="county"
              onChange={(e) => {
                if (e.target.value === '') setFieldValue('district', '');
                handleChange(e);
              }}
            ></select>
            <select
              id="district"
              className={`district ${classes.address}`}
              name="district"
              onChange={handleChange}
            ></select>
          </div>
          {errors.DetailAddress && touched.DetailAddress && (
            <div className="form-validation col-12 col-md-2 my-2">
              {errors.DetailAddress}
            </div>
          )}
        </div>
        <input
          type="text"
          className="input-adr"
          id="DetailAddress"
          name="DetailAddress"
          placeholder="(詳細地址)"
          value={values.DetailAddress}
          onChange={handleChange}
        />
      </div>
      <div className="row">
        <span className="list-title">
          <span className="circle"></span>訊息主旨
        </span>

        <input
          type="text"
          className="col-md"
          id="Title"
          name="Title"
          placeholder="(主旨)"
          value={values.Title}
          onChange={handleChange}
        ></input>
        {errors.Title && touched.Title && (
          <div className="form-validation col-12 col-md-2 my-2">
            {errors.Title}
          </div>
        )}
      </div>
      <div className="row other-textarea">
        <span className="list-title">
          <span className="circle"></span>反映內容
        </span>

        <textarea
          name="Description"
          id="Description"
          cols="30"
          rows="10"
          placeholder="(反映內容)"
          className="col-md"
          value={values.Description}
          style={{ verticalAlign: 'top' }}
          onChange={handleChange}
        />
        {errors.Description && touched.Description && (
          <div className="form-validation col-12 col-md-2 my-2">
            {errors.Description}
          </div>
        )}
      </div>
      {selectMainCategory === 'Fine' && (
        <div className="row">
          <span className="list-title">
            <span className="circle"></span>車號
          </span>

          <input
            type="text"
            className="col-md"
            id="PlateNumber"
            name="PlateNumber"
            // placeholder=""
            value={values.PlateNumber}
            onChange={handleChange}
          />
          {errors.PlateNumber && touched.PlateNumber && (
            <div className="form-validation col-12 col-md-2 my-2">
              {errors.PlateNumber}
            </div>
          )}
        </div>
      )}
      {selectMainCategory === 'Fine' && (
        <div className="TicketNumberOuter">
          <div>
            {/* {[1, 2, 3].map((item) => (
          <div>{item}</div>
        ))} */}

            <span className="list-title">
              <span className="circle"></span>罰單單號(
              {ticketNumberList.length}
              /100)
            </span>
            <button
              onClick={(e) => {
                e.preventDefault();
                setFieldValue('TicketNumbers', {
                  ...values.TicketNumbers,
                  ['TicketNumber' + ticketNumberIndex]: '',
                });
                handlerAddTicketNumber(e);
              }}
            >
              <i className="fas fa-plus"></i>
            </button>
          </div>
          <div className="TicketNumberGroup">
            {ticketNumberList.map((item) => (
              <div
                key={item}
                className="TicketNumberItem"
                id={`TicketNumber${item}`}
              >
                <input
                  type="text"
                  className="col-md"
                  id={item}
                  name={item}
                  placeholder="(最多新增100筆)"
                  value={values.TicketNumbers[item]}
                  onChange={(e) => handlerChangeTicketNumber(e, item)}
                />
                <button
                  onClick={(e) => {
                    e.preventDefault();
                    delete values.TicketNumbers[item];
                    setFieldValue('TicketNumbers', values.TicketNumbers);
                    handlerRemoveTicketNumber(e, item);
                  }}
                >
                  <i className="far fa-trash-alt"></i>
                </button>
              </div>
            ))}
          </div>

          {/* {errors.TicketNumber && touched.TicketNumber && (
        <div className="form-validation col-12 col-md-2 my-2">
          {errors.TicketNumber}
        </div>
      )} */}
        </div>
      )}

      <div className="row">
        <span className="list-title">
          <span className="circle"></span>是否公開
        </span>

        <input
          type="radio"
          name="IsPublic"
          value="true"
          id="IsPublicYes"
          disabled={values.SelectMainCategory === 'Fine' ? true : false}
          checked={values.IsPublic === 'true' ? true : false}
          onChange={(e) => {
            if (e.target.checked) setFieldValue('IsChiefMailbox', false);
            handleChange(e);
          }}
        />
        <label htmlFor="IsPublicYes">是</label>
        <input
          type="radio"
          name="IsPublic"
          value="false"
          id="IsPublicNo"
          disabled={values.SelectMainCategory === 'Fine' ? true : false}
          checked={values.IsPublic === 'true' ? false : true}
          onChange={handleChange}
        />
        <label htmlFor="IsPublicNo">否</label>
      </div>
      {!(selectMainCategory == 'Fine' && selectSubCategory == 1) && (
        <div className="row">
          <input
            type="checkbox"
            id="IsChiefMailbox"
            name="IsChiefMailbox"
            checked={values.IsChiefMailbox}
            onChange={(e) => {
              if (e.target.checked) setFieldValue('IsPublic', 'false');
              handleChange(e);
            }}
          />
          <label htmlFor="IsChiefMailbox" className="px-1">
            <span className="list-title">
              反應至首長信箱 (勾選將自動切換為不公開狀態)
            </span>
          </label>
        </div>
      )}
      <div className="row">
        <span className="list-title">相關網址</span>
        <input
          type="url"
          className="col-md col-lg-6"
          id="RelatedLinks"
          name="RelatedLinks"
          value={values.RelatedLinks}
          onChange={handleChange}
        />
      </div>
      <div className="row">
        <span className="list-title">檔案</span>
        <label htmlFor="file-data" className="label-file-data col-md">
          {/* <img src="/static/img/home/btn_upload.png" alt="選擇檔案" /> */}
          <input
            type="file"
            id="file-data"
            name="file-data"
            className="mb-3"
            onChange={(e) => handleChangeChoiceData(e)}
          />
          {/* <span className="file-src">{choiceData}</span> */}
        </label>
        <p className="file-p ">
          ＊每個上傳限制10Ｍ，最多11個支援格式支援格式.doc, .docx, .xls,.xlsx,
          .ppt, .pptx, .txt, .pdf, .jpg, .jpeg, .bmp, .gif, .png, .odt
        </p>
        {/* ＊每個上傳限制10Ｍ，最多10個支援格式支援格式.doc,.docx,.xls,.xlsx,.ppt,.pptx,.txt,.pdf,.jpg,.jpeg,.bmp,.gif,.png */}
      </div>
      <div className="row">
        <span className="list-title">影片</span>
        <label htmlFor="file-video" className="label-file-video col-md ">
          {/* <img src="/static/img/home/btn_upload.png" alt="" /> */}
          <input
            type="file"
            id="file-video"
            name="file-video"
            onChange={(e) => handleChangeChoiceData(e)}
          />
          {/* <span className="file-src">{choiceVideo}</span> */}
        </label>
        <p className="file-p">＊上傳影片限制35Ｍ，最多3個</p>
      </div>
      <div className="row">
        <span className="list-title captcha-title ">
          <span className="circle"></span>驗證碼
        </span>
        <input type="text" className="col-md-5 col-lg-2 captcha-input" />
        <img
          src="/static/img/home/Image 1.png"
          alt="驗證碼icon"
          className="num-img"
        />
        <button type="reset" className="button-style">
          <img src="/static/img/home/icon_reload.png" alt="重整驗證碼icon" />
        </button>
        <button type="" className="button-style">
          <img src="/static/img/home/icon_sound.png" alt="驗證碼語音icon" />
        </button>
      </div>
      <div className="note-list">
        <p>
          本系統依據「臺南市政府及所屬機關處理人民陳情作業要點」辦理人民陳情案件，不受理漫罵、非理性、匿名及情緒抒發或惡意檢舉之案件。
          <br />
          請您提供正確的個人基本資料（包含姓名、電話、地址與電子信箱等），以利系統有效寄發確認信、案件編號與回覆處理結果。
          <br />
          為落實個人資料保護及管理，填寫案件內容，請勿包含得以直接或間接方式識別個人之資料，本局保有進行該資料隱碼作業之權限。
          <br />
          系統自動發送「確認信」機制乃為確認您所提供之電子信箱無誤並防止遭他人冒用。如您於案件反映後，未收到確認信，請查詢您的電子信箱垃圾信件夾並進行確認。
          <br />
          請您協助回填隨同案件辦理結果所附之線上滿意度調查問卷，以利追蹤檢討並提升服務品質。
        </p>
        <div className="note-label-box">
          <label htmlFor="isReadOk" className="note-checkbox-box">
            <input
              type="checkbox"
              name="isReadOk"
              id="isReadOk"
              checked={values.isReadOk}
              onChange={(e) => {
                handleChange(e);
              }}
            />
            我已詳細閱讀並同意遵守
          </label>
        </div>
      </div>
      <div className="send">
        <Button
          variant="contained"
          className={`${classes.confirmBtn} ${classes.clear}`}
          onClick={handleClear}
        >
          清除
        </Button>
        <Button
          variant="contained"
          type="submit"
          className={`${classes.confirmBtn} ${
            values.isReadOk ? '' : classes.gray
          }`}
          onClick={(e) => {
            !values.isReadOk && e.preventDefault();
            // setIsOpen((prev) => !prev);
            handleScroll();
          }}
        >
          送出
        </Button>
      </div>
    </div>
  );
};

export default CommentTable;
