import React from 'react';
// import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
// import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import selectData from '../../../data/select-data';

const useStyles = makeStyles((theme) => ({
  tableContainer: {
    margin: theme.spacing(5, 0),
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',
      '&:nth-child(1)': {
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:nth-child(2)': {
        minWidth: '130px',
        width: '130px',
        maxWidth: '130px',
      },
      '&:nth-child(3)': {
        minWidth: '180px',
        width: '180px',
        maxWidth: '180px',
      },

      '&:nth-child(4)': {
        minWidth: '300px',
        position: 'relative',
        lineHeight: '26px',
        padding: '27px 54px 27px 18px',
      },
      '&:nth-child(5)': {
        minWidth: '200px',
        width: '200px',
        maxWidth: '200px',
      },
      '&:nth-child(6)': {
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
  yellowFrame: {
    padding: theme.spacing(0.5, 2),
    border: '1px solid #fecc46',
    display: 'inline-block',
    borderRadius: '5px',
  },
  downloadButton: {
    width: '49px',
  },
  formatButton: {
    width: '49px',
  },
  linkButton: {
    width: '49px',
  },
  collapseButton: {
    position: 'absolute',
    top: '22px',
    right: '18px',
    width: '36px',
    height: '36px',
  },
}));

const PolicyTable = ({ ODTData, title }) => {
  const classes = useStyles();
  const selectList = selectData.find((item) => item.title == title);

  return (
    <TableContainer className={classes.tableContainer}>
      <Table aria-label="collapsible table" className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRow}>
            <TableCell align="center">序號</TableCell>
            <TableCell align="center">發布日期</TableCell>
            <TableCell align="center">分類</TableCell>
            <TableCell align="center">檔案名稱</TableCell>
            <TableCell align="center">開放格式</TableCell>
            {/* <TableCell align="center">連結</TableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
          {ODTData.map((item, index) => (
            <React.Fragment key={index}>
              <CustomizedRow row={item} index={index} selectList={selectList} />
            </React.Fragment>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const CustomizedRow = ({ row, index, selectList }) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  return (
    <TableRow className={classes.bodyRow}>
      <TableCell align="center">{index + 1}</TableCell>
      <TableCell align="center">
        {moment(row.UpdataTime).format('YYYY-MM-DD')}
      </TableCell>
      <TableCell align="center">
        <Typography variant="span" className={classes.yellowFrame}>
          {selectList &&
            selectList.subNav &&
            selectList.subNav.find((item) => item.category == row.Category)
              ?.title}
        </Typography>
      </TableCell>
      <TableCell>
        {row.OpenDataName}
        <IconButton
          className={classes.collapseButton}
          onClick={() => setOpen(!open)}
        >
          {open ? (
            <img src="/static/img/icons/collapse-open.png" alt="已打開的圖示" />
          ) : (
            <img
              src="/static/img/icons/collapse-close.png"
              alt="已關閉的圖示"
            />
          )}
        </IconButton>
        <Collapse in={open} timeout="auto" unmountOnExit>
          {/* {row.Name} */}
          <TableContainer className={classes.insideTable}>
            {/* 尚未完成 */}
            {row.TableData && (
              <Table>
                {/* {row.TableData.row.map((itm, idx) => (
                  <TableRow className={classes.insideBodyRow}>
                    <TableCell align="center">{itm[0]}</TableCell>
                    <TableCell>{itm[1]}</TableCell>
                    <TableCell>
                      <IconButton>
                        <a
                          href={itm[2]}
                          target="_blank"
                          rel="noreferrer noopener"
                        >
                          <img
                            src="/static/img/home/ic_openlink_2.svg"
                            alt=""
                          />
                        </a>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))} */}
              </Table>
            )}
          </TableContainer>
        </Collapse>
      </TableCell>
      <TableCell align="center">
        {/* 尚未完成 */}
        {row.formatXML && (
          <IconButton className={classes.formatButton}>
            <img src="/static/img/icons/format-XML.png" alt="XML圖示" />
          </IconButton>
        )}
        {row.formatCSV && (
          <IconButton className={classes.formatButton}>
            <img src="/static/img/icons/format-CSV.png" alt="CSV圖示" />
          </IconButton>
        )}
        {row.formatJSON && (
          <IconButton className={classes.formatButton}>
            <img src="/static/img/icons/format-JSON.png" alt="JSON圖示" />
          </IconButton>
        )}
      </TableCell>
      {/* <TableCell align="center">
        {row.link && (
          <IconButton className={classes.linkButton}>
            <img src="/static/img/icons/link-icon.png" alt="連結圖示" />
          </IconButton>
        )}
      </TableCell> */}
    </TableRow>
  );
};

export default PolicyTable;
