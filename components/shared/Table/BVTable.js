import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import selectData from '../../../data/select-data';
const useStyles = makeStyles((theme) => ({
  tableContainer: {
    margin: '20px 0px 40px 0px',
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',
      '&:nth-child(1)': {
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:nth-child(2)': {
        minWidth: '160px',
        width: '160px',
        maxWidth: '160px',
      },
      '&:nth-child(3)': {
        minWidth: '240px',
        width: '240px',
        maxWidth: '240px',
      },
      '&:nth-child(4)': {
        minWidth: '240px',
        width: '240px',
        maxWidth: '240px',
      },
      '&:nth-child(5)': {
        minWidth: '300px',
        position: 'relative',
        lineHeight: '26px',
        padding: '27px 54px 27px 18px',
      },
      '&:nth-child(6)': {
        padding: '15.5px 16px',
        height: '80px',
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:nth-child(7)': {
        minWidth: '110px',
        width: '110px',
        maxWidth: '110px',
      },
      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
  yellowFrame: {
    padding: theme.spacing(0.5, 2),
    border: '1px solid #fecc46',
    display: 'inline-block',
    borderRadius: '5px',
  },
  downloadButton: {
    width: '49px',
  },
  linkButton: {
    width: '49px',
  },
  collapseButton: {
    position: 'absolute',
    top: '22px',
    right: '18px',
    width: '36px',
    height: '36px',
  },
}));

const BVTable = ({ BVData }) => {
  const classes = useStyles();
  return (
    <TableContainer className={classes.tableContainer}>
      <Table aria-label="collapsible table" className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRow}>
            <TableCell align="center">序號</TableCell>
            <TableCell align="center">分類</TableCell>
            <TableCell align="center">中文</TableCell>
            <TableCell align="center">英文</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {BVData.map((item, index) => (
            <React.Fragment key={index}>
              <CustomizedRow row={item} />
            </React.Fragment>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const CustomizedRow = ({ row }) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  const category = selectData.find((item) => item.title == '雙語詞彙').subNav;
  return (
    <TableRow className={classes.bodyRow}>
      <TableCell align="center">
        <div dangerouslySetInnerHTML={{ __html: row.SequenceNo }} />
      </TableCell>
      <TableCell align="center">{category[row.Type - 1].title}</TableCell>
      <TableCell align="center">
        <div dangerouslySetInnerHTML={{ __html: row.Chinese }} />
      </TableCell>
      <TableCell align="center">
        <div dangerouslySetInnerHTML={{ __html: row.English }} />
      </TableCell>
    </TableRow>
  );
};

export default BVTable;
