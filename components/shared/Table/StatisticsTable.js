import { makeStyles } from '@material-ui/core/styles';
// import Box from '@material-ui/core/Box';
// import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
  tableContainer: {
    margin: theme.spacing(5, 0),
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',
      '&:nth-child(1)': {
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:nth-child(2)': {
        minWidth: '130px',
        width: '130px',
        maxWidth: '130px',
      },
      '&:nth-child(3)': {
        minWidth: '180px',
        width: '180px',
        maxWidth: '180px',
      },

      '&:nth-child(4)': {
        minWidth: '300px',
        position: 'relative',
        lineHeight: '26px',
        padding: '27px 54px 27px 18px',
      },
      '&:nth-child(5)': {
        padding: '15.5px 16px',
        height: '80px',
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },

      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
  yellowFrame: {
    padding: theme.spacing(0.5, 2),
    border: '1px solid #fecc46',
    display: 'inline-block',
    borderRadius: '5px',
  },
  downloadButton: {
    width: '49px',
  },
  linkButton: {
    width: '49px',
  },
  collapseButton: {
    position: 'absolute',
    top: '22px',
    right: '18px',
    width: '36px',
    height: '36px',
  },
}));

const statisticsTable = ({ statisticsData, resData }) => {
  const classes = useStyles();
  return (
    <TableContainer className={classes.tableContainer}>
      <Table aria-label="collapsible table" className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRow}>
            <TableCell align="center">序號</TableCell>
            <TableCell align="center">發布日期</TableCell>
            <TableCell align="center">分類</TableCell>
            <TableCell align="center">檔案名稱</TableCell>
            <TableCell align="center">下載</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {resData &&
            resData.rows.length > 0 &&
            resData.rows.map((item, index) => (
              <React.Fragment key={index}>
                <CustomizedRow row={item} index={index} />
              </React.Fragment>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const CustomizedRow = ({ row, index }) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  return (
    <TableRow className={classes.bodyRow}>
      <TableCell align="center">{index + 1}</TableCell>
      <TableCell align="center">
        {moment(row.UpdateTime).format('YYYY-MM-DD')}
      </TableCell>
      <TableCell align="center">
        <Typography variant="span" className={classes.yellowFrame}>
          {row.Category == '1'
            ? '性別統計分析'
            : row.Category == '2'
            ? '年度計劃'
            : row.Category == '3'
            ? '性別統計指標'
            : row.Category == '4'
            ? '預告統計時間'
            : row.Category == '5'
            ? '臺南市政府交通局統計年報'
            : row.Category == '6'
            ? '臺南市車位概況報表'
            : row.Category == '7'
            ? '臺南市市區汽車客運業營運概況'
            : row.Category == '8'
            ? '統計通報'
            : row.Category == '9'
            ? '統計專題分析'
            : row.Category == '10'
            ? '辦理國家賠償事件統計'
            : row.Category == '11'
            ? '公務統計方案'
            : row.Category == '12'
            ? '預告統計資料發布時間'
            : null}
        </Typography>
      </TableCell>
      <TableCell>{row.FileName}</TableCell>
      <TableCell align="center">
        {row.FileUrl && (
          <IconButton className={classes.downloadButton}>
            <a href={process.env.URL + row.FileUrl} download>
              <img src="/static/img/icons/download-icon.png" alt="下載圖示" />
            </a>
          </IconButton>
        )}
      </TableCell>
    </TableRow>
  );
};

export default statisticsTable;
