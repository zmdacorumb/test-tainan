import { makeStyles } from '@material-ui/core/styles';
// import Box from '@material-ui/core/Box';
// import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import Link from 'next/link';
const useStyles = makeStyles((theme) => ({
  tableContainer: {
    margin: theme.spacing(5, 0),
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',
      '&:nth-child(1)': {
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:nth-child(2)': {
        minWidth: '130px',
        width: '130px',
        maxWidth: '130px',
      },
      '&:nth-child(3)': {
        minWidth: '180px',
        width: '180px',
        maxWidth: '180px',
      },

      '&:nth-child(4)': {
        minWidth: '300px',
        position: 'relative',
        lineHeight: '26px',
        padding: '27px 54px 27px 18px',
      },
      '&:nth-child(5)': {
        padding: '15.5px 16px',
        height: '80px',
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:nth-child(6)': {
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
  yellowFrame: {
    padding: theme.spacing(0.5, 2),
    border: '1px solid #fecc46',
    display: 'inline-block',
    borderRadius: '5px',
  },
  downloadButton: {
    width: '49px',
  },
  linkButton: {
    width: '49px',
  },
  collapseButton: {
    position: 'absolute',
    top: '22px',
    right: '18px',
    width: '36px',
    height: '36px',
  },
}));

const LianNengTable = ({ lianNengData, resData }) => {
  const classes = useStyles();
  return (
    <TableContainer className={classes.tableContainer}>
      <Table aria-label="collapsible table" className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRow}>
            <TableCell align="center">序號</TableCell>
            <TableCell align="center">發布日期</TableCell>
            <TableCell align="center">分類</TableCell>
            <TableCell align="center">檔案名稱</TableCell>
            <TableCell align="center">下載</TableCell>
            {/* <TableCell align="center">連結</TableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
          {resData &&
            resData.rows.map((item, index) => (
              <React.Fragment key={index}>
                <CustomizedRow row={item} index={index} />
              </React.Fragment>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const CustomizedRow = ({ row, index }) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  return (
    <TableRow className={classes.bodyRow}>
      <TableCell align="center">{index + 1}</TableCell>
      <TableCell align="center">
        {moment(row.UpdateTime).format('YYYY-MM-DD')}
      </TableCell>
      <TableCell align="center">
        <Typography variant="span" className={classes.yellowFrame}>
          {row.SubCategory == 1 ? '廉政' : row.SubCategory == 2 ? '一般' : null}
        </Typography>
      </TableCell>
      <TableCell>
        {row.IsAttached ? (
          <Link
            href={`/information/administrative-information/fileDetail?type=clear-gov&id=${row.ID}`}
          >
            <a className="theme-a">{row.Title}</a>
          </Link>
        ) : (
          <Link
            href={`/information/administrative-information/announcementDetail?type=clear-gov&id=${row.ID}`}
          >
            <a className="theme-a">{row.Title}</a>
          </Link>
        )}
      </TableCell>
      <TableCell align="center">
        {row.IsAttached && (
          <a href={`${process.env.URL}/api/downloadArchive?id=${row.ID}`}>
            <IconButton className={classes.downloadButton}>
              <img src="/static/img/icons/download-icon.png" alt="下載圖示" />
            </IconButton>
          </a>
        )}
      </TableCell>
      {/* <TableCell align="center">
        {row.HasLinks && (
          <IconButton className={classes.linkButton}>
            <img src="/static/img/icons/link-icon.png" alt="連結圖示" />
          </IconButton>
        )}
      </TableCell> */}
    </TableRow>
  );
};

export default LianNengTable;
