import { makeStyles } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';

import moment from 'moment';
const useStyles = makeStyles((theme) => ({
  tableContainer: {
    margin: '20px 0px 40px 0px',
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',
      '&:nth-child(1)': {
        // minWidth: '60px',
        // width: '60px',
        // maxWidth: '60px',
      },
      '&:nth-child(2)': {
        // minWidth: '200px',
        // width: '200px',
        // maxWidth: '200px',
      },
      '&:nth-child(3)': {
        position: 'relative',
      },

      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
  headCell1: {
    minWidth: '60px',
    width: '60px',
    maxWidth: '60px',
  },
  headCell2: {
    minWidth: '200px',
    width: '200px',
    maxWidth: '200px',
  },
  headCell3: {},
  insideTable: {
    marginTop: '12px',
  },
  insideHeadCell: {
    border: '2px solid #eeeeee',
    backgroundColor: '#fafafa',
    width: '80px',
    minWidth: '80px',
    maxWidth: '80px ',
  },
  insideBodyCell: {
    padding: '6px 0px',
    border: '2px solid #eeeeee !important',
    backgroundColor: 'white',
  },
  yellowFrame: {
    padding: theme.spacing(0.5, 2),
    border: '1px solid #fecc46',
    display: 'inline-block',
    borderRadius: '5px',
  },
  downloadButton: {
    width: '49px',
  },
  linkButton: {
    width: '49px',
  },
  collapseButton: {
    position: 'absolute',
    top: '10.5px',
    right: '18px',
    width: '36px',
    height: '36px',
  },
  link: {
    color: 'rgba(0,0,0,0.7)',
    '&:hover': {
      color: 'rgba(37,92,153,0.9)',
    },
  },
}));

const RSATable = ({ RSAData }) => {
  const classes = useStyles();
  return (
    <TableContainer className={classes.tableContainer}>
      <Table aria-label="collapsible table" className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRow}>
            <TableCell align="center" className={classes.headCell1}>
              序號
            </TableCell>
            <TableCell align="center" className={classes.headCell2}>
              發布日期
            </TableCell>
            <TableCell align="center" className={classes.headCell3}>
              檔案名稱
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {RSAData.map((item, index) => (
            <React.Fragment key={index}>
              <CustomizedRow row={item} />
            </React.Fragment>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const CustomizedRow = ({ row }) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  return (
    <TableRow className={classes.bodyRow}>
      <TableCell align="center">{row.SequenceNo}</TableCell>
      <TableCell align="center">
        {moment(row.UpdateTime).format('YYYY-MM-DD')}
      </TableCell>
      <TableCell align="left">
        {row.Title}
        <IconButton
          className={classes.collapseButton}
          onClick={() => setOpen(!open)}
        >
          {open ? (
            <img src="/static/img/icons/collapse-open.png" alt="已打開的圖示" />
          ) : (
            <img
              src="/static/img/icons/collapse-close.png"
              alt="已關閉的圖示"
            />
          )}
        </IconButton>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <TableContainer className={classes.insideTable}>
            {row.AttachedFiles && (
              <Table>
                <TableHead>
                  <TableRow className={classes.insideHeadRow}>
                    {Array(12)
                      .fill('')
                      .map((item, index) => (
                        <TableCell
                          align="center"
                          className={classes.insideHeadCell}
                        >
                          {index + 1}月份
                        </TableCell>
                      ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow className={classes.insideBodyRow}>
                    {Array(12)
                      .fill('')
                      .map((item, index) => (
                        <TableCell
                          align="center"
                          className={classes.insideBodyCell}
                        >
                          {row.AttachedFiles.find(
                            (searchItem, searchIndex) =>
                              searchItem.Description == String(index + 1)
                          ) && (
                            <a
                              href={`${process.env.URL}${
                                row.AttachedFiles.find(
                                  (searchItem, searchIndex) =>
                                    searchItem.Description == String(index + 1)
                                ).FilePath
                              }`}
                              target="_blank"
                              rel="noreferrer noopener"
                            >
                              <IconButton className={classes.downloadButton}>
                                <img
                                  src="/static/img/icons/download-icon.png"
                                  alt="下載圖示"
                                />
                              </IconButton>
                            </a>
                          )}
                        </TableCell>
                      ))}
                  </TableRow>
                </TableBody>
              </Table>
            )}
          </TableContainer>
        </Collapse>
      </TableCell>
    </TableRow>
  );
};

export default RSATable;
