import { makeStyles } from '@material-ui/core/styles';
// import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
// import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
// import Typography from '@material-ui/core/Typography';
import moment from 'moment';
const useStyles = makeStyles((theme) => ({
  tableContainer: {
    margin: theme.spacing(5, 0),
  },
  table: {
    border: '2px solid #f3f3f3',
  },
  headRow: {
    backgroundColor: '#fecc46',
    '& th': {
      borderRight: '2px solid white',
      fontSize: '1.8rem',
      borderBottom: 'unset',
      whiteSpace: 'nowrap',
      '&:last-child': {
        borderRight: 'unset',
      },
    },
  },
  bodyRow: {
    '& td': {
      fontSize: '1.8rem',
      borderBottom: 'unset',
      borderRight: '2px solid #f3f3f3',
      '&:nth-child(1)': {
        minWidth: '80px',
        width: '80px',
        maxWidth: '80px',
      },
      '&:nth-child(2)': {
        minWidth: '130px',
        width: '130px',
        maxWidth: '130px',
      },
      // '&:nth-child(3)': {
      //   minWidth: '150px',
      //   width: '150px',
      //   maxWidth: '150px',
      // },
      '&:nth-child(3)': {
        minWidth: '300px',
        position: 'relative',
        lineHeight: '26px',
        padding: '27px 54px 27px 18px',
        [theme.breakpoints.down('xs')]: {
          minWidth: '800px',
        },
      },
      // '&:nth-child(5)': {
      //   padding: '15.5px 16px',
      //   height: '80px',
      //   minWidth: '80px',
      //   width: '80px',
      //   maxWidth: '80px',
      // },
      '&:last-child': {
        borderRight: 'unset',
      },
    },
    '&:nth-child(odd)': {
      backgroundColor: '#fafafa',
    },
    '&:nth-child(even)': { backgroundColor: 'white' },
  },
  insideTable: {
    marginTop: '1rem',
  },
  insideBodyRow: {
    background: '#fff',
    margin: '5px',
    border: '3px solid #f3f3f3',
    '& td:nth-child(1)': {
      minWidth: '130px',
      width: '130px',
      maxWidth: '130px',
      borderRight: 'none',
    },
    '& td:nth-child(2)': {
      padding: '10px 54px 10px 18px',
      textAlign: 'right',
    },
    '& td:nth-child(3)': {
      minWidth: '80px',
      width: '80px',
      maxWidth: '80px',
      padding: '0',
    },
  },
  yellowFrame: {
    padding: theme.spacing(0.5, 2),
    border: '1px solid #fecc46',
    display: 'inline-block',
    borderRadius: '5px',
  },
  downloadButton: {
    width: '49px',
  },
  collapseButton: {
    position: 'absolute',
    top: '22px',
    right: '18px',
    width: '36px',
    height: '36px',
  },
}));

const RelatedLinkTable = ({ FAQData, resData }) => {
  const classes = useStyles();
  return (
    <TableContainer className={classes.tableContainer}>
      <Table aria-label="collapsible table" className={classes.table}>
        <TableHead>
          <TableRow className={classes.headRow}>
            <TableCell align="center">序號</TableCell>
            <TableCell align="center">發佈日期</TableCell>
            {/* <TableCell align="center">分類</TableCell> */}
            <TableCell align="center">標題</TableCell>
            {/* <TableCell align="center">下載</TableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
          {resData &&
            resData.rows.map((item, index) => (
              <React.Fragment key={index}>
                <CustomizedRow row={item} index={index} />
              </React.Fragment>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const CustomizedRow = ({ row, index }) => {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  return (
    <TableRow className={classes.bodyRow}>
      <TableCell align="center">{index + 1}</TableCell>
      <TableCell align="center">
        {moment(row.UpdataTime).format('YYYY-MM-DD')}
      </TableCell>
      {/* <TableCell align="center">
        <Typography variant="span" className={classes.yellowFrame}>
          {row.category}
        </Typography>
      </TableCell> */}

      <TableCell>
        {row.Name}
        <IconButton
          className={classes.collapseButton}
          onClick={() => setOpen(!open)}
        >
          {open ? (
            <img src="/static/img/icons/collapse-open.png" alt="已打開的圖示" />
          ) : (
            <img
              src="/static/img/icons/collapse-close.png"
              alt="已關閉的圖示"
            />
          )}
        </IconButton>
        <Collapse in={open} timeout="auto" unmountOnExit>
          {/* {row.Name} */}
          <TableContainer className={classes.insideTable}>
            {row.TableData && (
              <Table>
                {row.TableData.row.map((itm, idx) => (
                  <TableRow key={idx} className={classes.insideBodyRow}>
                    {/* <TableCell align="center">{itm[0]}</TableCell> */}
                    <TableCell>{itm[1]}</TableCell>
                    <TableCell>
                      <IconButton>
                        <a
                          href={itm[2]}
                          target="_blank"
                          rel="noreferrer noopener"
                        >
                          <img
                            src="/static/img/home/ic_openlink_2.svg"
                            alt=""
                          />
                        </a>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </Table>
            )}
          </TableContainer>
        </Collapse>
      </TableCell>
      {/* <TableCell align="center">
        {row.downloadLink && (
          <IconButton className={classes.downloadButton}>
            <img src="/static/img/icons/download-icon.png" alt="下載圖示" />
          </IconButton>
        )}
      </TableCell> */}
    </TableRow>
  );
};

export default RelatedLinkTable;
