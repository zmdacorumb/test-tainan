import useSWR from 'swr';

import NoData from '../Layout/MainContent/NoData';
// import { alternativeSetting } from '../../../data/slick-settings';
import { fetchData } from '../../../api/helper';

import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  paper: {
    width: '80vw',
    maxWidth: '900px',
    minHeight: '70vh',
  },
  flag: {
    background:
      'url(/static/img/bg/menu_title.png) center center / 100% 100% no-repeat',
    marginLeft: '60px',
    width: 'fit-content',
    padding: '8px 16px 24px 16px',
  },
  closeButton: {
    position: 'absolute',
    top: '6px',
    right: '8px',
  },
  dialogTitle: {
    fontSize: '2.4rem',
    padding: '8px  60px',
    [theme.breakpoints.down('sm')]: {
      padding: '8px 12px',
    },
  },
  divider: {
    backgroundColor: '#98eadd',
    height: '2px',
    width: 'calc(100% - 60px)',
    margin: '0px auto',
    border: 'none',
  },
  dialogContent: {
    margin: '20px 60px',
    fontSize: '1.6rem',
  },
  image: {
    margin: '20px 0px',
    width: '100%',
  },
  iconButton: {
    padding: '10px',
    width: '48px',
    position: 'absolute',
    bottom: '8px',
    right: '8px',
  },
}));

const AlternativeRoadDialog = (props) => {
  const classes = useStyles();
  const { setDialogDisplay, dialogID, dialogDisplay, dialogTitle } = props;
  const { data, error } = useSWR(`/api/QnA/${dialogID}`, fetchData);

  const handleClose = () => {
    setDialogDisplay(false);
  };
  console.log('dialog data', data);

  return (
    <Dialog
      classes={{ paper: classes.paper }}
      onClose={handleClose}
      aria-labelledby="alternative-road-dialog-title"
      open={dialogDisplay}
    >
      <div className={classes.flag}>常見問題</div>
      <IconButton
        aria-label="close"
        className={classes.closeButton}
        onClick={handleClose}
      >
        <CloseIcon />
      </IconButton>
      <div id="alternative-road-dialog-title" className={classes.dialogTitle}>
        {dialogTitle}
      </div>
      <hr className={classes.divider} />
      {data && <DialogContent data={data} />}
      {!data || (error && <NoData />)}
    </Dialog>
  );
};
const DialogContent = ({ data }) => {
  const classes = useStyles();

  return (
    <div className={classes.dialogContent}>
      <div>{data.Answer}</div>
      {data.AttachedFiles.map((item, index) => (
        <img
          className={classes.image}
          src={`${process.env.URL}${item.FilePath}`}
          alt="DialogPicture"
          key={index}
        />
      ))}
    </div>
  );
};
export default AlternativeRoadDialog;
