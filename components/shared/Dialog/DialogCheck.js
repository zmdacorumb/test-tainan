import { memo } from 'react';
import Router from 'next/router';
// material
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = makeStyles((theme) => ({
  justifyContentCenter: {
    justifyContent: 'center',
  },
  confirmBtn: {
    padding: '2px 30px',
    color: '#fff',
    fontSize: '2rem',
    backgroundColor: '#00a49b',
    '&:hover': {
      backgroundColor: 'rgba(0,134,135,1.0)',
    },
    '& + &': {
      marginLeft: '20px',
    },
  },
  gray: {
    backgroundColor: '#aaa',
    cursor: 'not-allowed !important',
    '&:hover': {
      backgroundColor: '#aaa',
    },
  },
  clear: {
    backgroundColor: '#aaa',
    '&:hover': {
      backgroundColor: 'rgba(0,134,135,1.0)',
    },
  },
  dialogTitle: {
    fontSize: '2rem',
    '& h2': {
      fontWeight: '700',
      fontSize: '2rem',
    },
  },
  dialogContentText: {
    fontSize: '1.8rem',
  },
}));

const DialogCheck = (props) => {
  const classes = useStyles();
  const { isOpen, setIsOpen, directUrl } = props;
  // dialog trigger

  const handlerConfirm = () => {
    setIsOpen((prev) => !prev);
    Router.push(directUrl);
  };
  return (
    <div>
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        maxWidth={'sm'}
        open={isOpen}
        onClose={() => setIsOpen((prev) => !prev)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title" className={classes.dialogTitle}>
          提交成功
        </DialogTitle>
        <DialogContent>
          <DialogContentText
            id="alert-dialog-description"
            className={classes.dialogContentText}
          >
            為確定為本人提交，案件在驗證後才會成立，請查看所屬信箱。
          </DialogContentText>
        </DialogContent>
        <DialogActions className={classes.justifyContentCenter}>
          {/* <Button onClick={() => setIsOpen((prev) => !prev)} color="primary">
            取消
          </Button> */}
          <Button
            onClick={handlerConfirm}
            color="primary"
            className={classes.confirmBtn}
            // autoFocus
          >
            確定
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default memo(DialogCheck);
