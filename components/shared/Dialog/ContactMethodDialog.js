import useSWR from 'swr';
import Slider from 'react-slick';
import { useEffect, useState, useMemo } from 'react';

import NoData from '../Layout/MainContent/NoData';
import { alternativeSetting } from '../../../data/slick-settings';
import { postData } from '../../../api/helper';
import BlueTitleTag from '../Layout/MainContent/BlueTitleTag';

import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  paper: {
    width: '80vw',
    maxWidth: '900px',
    minHeight: '70vh',
  },
  flag: {
    background:
      'url(/static/img/bg/menu_title.png) center center / 100% 100% no-repeat',
    marginLeft: '60px',
    width: 'fit-content',
    padding: '8px 16px 24px 16px',
  },
  closeButton: {
    position: 'absolute',
    top: '6px',
    right: '8px',
  },
  dialogTitle: {
    fontSize: '2.4rem',
    padding: '8px  60px',
    [theme.breakpoints.down('sm')]: {
      padding: '8px 12px',
    },
  },
  divider: {
    backgroundColor: '#98eadd',
    height: '2px',
    width: 'calc(100% - 60px)',
    margin: '0px auto',
    border: 'none',
  },
  imageBox: {
    margin: '40px 20px',
    height: '400px',
    textAlign: 'center',
  },
  image: {
    height: '100%',
  },
  iconButton: {
    padding: '10px',
    width: '48px',
    position: 'absolute',
    bottom: '8px',
    right: '8px',
  },
  dialogContent: {
    borderTop: '2px solid #98eadd',
    padding: '20px 40px',
    margin: '0px 20px',
    [theme.breakpoints.down('sm')]: {
      padding: '20px 12px',
      margin: '0px ',
    },
  },
  typography: { marginBottom: '20px' },
}));

const AlternativeRoadDialog = (props) => {
  const classes = useStyles();
  const { setDialogDisplay, contactID, dialogDisplay, dialogTitle } = props;

  const postObj = useMemo(
    () => ({
      ID: contactID,
    }),
    [contactID]
  );
  const { data, error } = useSWR(
    [`/api/TeamIntro/ContactInfo`, postObj],
    postData
  );

  const handleClose = () => {
    setDialogDisplay(false);
  };
  console.log('data', data);

  return (
    <Dialog
      classes={{ paper: classes.paper }}
      onClose={handleClose}
      aria-labelledby="alternative-road-dialog-title"
      open={dialogDisplay}
    >
      <div className={classes.flag}>聯絡地址</div>
      <IconButton
        aria-label="close"
        className={classes.closeButton}
        onClick={handleClose}
      >
        <CloseIcon />
      </IconButton>
      <div id="alternative-road-dialog-title" className={classes.dialogTitle}>
        {dialogTitle}
      </div>
      {/* <hr className={classes.divider} /> */}
      {data && <DialogContent data={data.data.rows[0]} />}
      {(!data || error) && <NoData />}
    </Dialog>
  );
};
const DialogContent = ({ data }) => {
  const classes = useStyles();

  return (
    <div className={classes.dialogContent}>
      <BlueTitleTag title="地址" small />
      <p className={classes.typography}>{data.Address}</p>
      <BlueTitleTag title="交通資訊連結" small />
      <p>
        搭乘本市公共運輸系統：可透過
        <a
          href="https://2384.tainan.gov.tw/NewTNBusWeb/TransferTransport.aspx"
          target="_blank"
          rel="noreferrer noopener"
        >
          大臺南公車
        </a>
        網站 <strong>[ 公共運輸轉乘查詢 ]</strong> 功能查詢 (請先複製上方地址)。
      </p>
      <p className={classes.typography}>
        自行開車：可透過
        <a
          href=" http://tntcc.tainan.gov.tw/TainanTraffic/login.do"
          target="_blank"
          rel="noreferrer noopener"
        >
          臺南市即時交通資訊網
        </a>
        查詢交通管制資訊。
      </p>
      <BlueTitleTag title="地圖導航" small />
      <p>
        電腦：請選擇 <strong>[ 路線 ]</strong> 或
        <strong>[ 顯示詳細地圖 ]</strong> 顯示導航資訊。
      </p>
      <p>
        手機：請選擇 <strong>[ 顯示詳細地圖 ]</strong> 顯示導航資訊。
      </p>
      <iframe
        src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyBk4tyu3TeWntF2Iz_iG3Heqcj5DLzKAoQ&q=${data.Address}&language=zh-TW`}
        width="100%"
        height="450"
        frameBorder="0"
        style={{
          maxWidth: '600px',
          display: 'block',
          border: 0,
          margin: '30px auto',
        }}
        allowFullScreen=""
        aria-hidden="false"
        tabIndex="0"
      />
    </div>
  );
};
export default AlternativeRoadDialog;
