import useSWR from 'swr';
import Slider from 'react-slick';

import NoData from '../Layout/MainContent/NoData';
import { alternativeSetting } from '../../../data/slick-settings';
import { fetchData } from '../../../api/helper';

import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  paper: {
    width: '80vw',
    maxWidth: '900px',
    minHeight: '70vh',
  },
  flag: {
    background:
      'url(/static/img/bg/menu_title.png) center center / 100% 100% no-repeat',
    marginLeft: '60px',
    width: 'fit-content',
    padding: '8px 16px 24px 16px',
  },
  closeButton: {
    position: 'absolute',
    top: '6px',
    right: '8px',
  },
  dialogTitle: {
    fontSize: '2.4rem',
    padding: '8px  60px',
    [theme.breakpoints.down('sm')]: {
      padding: '8px 12px',
    },
  },
  divider: {
    backgroundColor: '#98eadd',
    height: '2px',
    width: 'calc(100% - 60px)',
    margin: '0px auto',
    border: 'none',
  },
  imageBox: {
    margin: '40px 20px',
    height: '400px',
    textAlign: 'center',
  },
  image: {
    height: '100%',
  },
  iconButton: {
    padding: '10px',
    width: '48px',
    position: 'absolute',
    bottom: '8px',
    right: '8px',
  },
}));

const AlternativeRoadDialog = (props) => {
  const classes = useStyles();
  const { setDialogDisplay, roadID, dialogDisplay, dialogTitle } = props;
  const { data, error } = useSWR(`/api/ReliefRoad/${roadID}`, fetchData);

  const handleClose = () => {
    setDialogDisplay(false);
  };
  console.log('data', data);

  return (
    <Dialog
      classes={{ paper: classes.paper }}
      onClose={handleClose}
      aria-labelledby="alternative-road-dialog-title"
      open={dialogDisplay}
    >
      <div className={classes.flag}>{dialogTitle}</div>
      <IconButton
        aria-label="close"
        className={classes.closeButton}
        onClick={handleClose}
      >
        <CloseIcon />
      </IconButton>
      <div id="alternative-road-dialog-title" className={classes.dialogTitle}>
        {dialogTitle}施工期間交通改道路線
      </div>
      <hr className={classes.divider} />
      {data && <DialogContent data={data} />}
      {(!data || error) && <NoData />}
    </Dialog>
  );
};
const DialogContent = ({ data }) => {
  const classes = useStyles();
  if (data.AttachedFiles.length == 1) {
    return (
      <>
        <div className={classes.imageBox}>
          <img
            className={classes.image}
            src={`${process.env.URL}${data.AttachedFiles[0].FilePath.replace(
              '_thumb',
              ''
            )}`}
            alt="交通替代圖"
          />
        </div>
        <a href={data.MapUrl} target="_blank" rel="noreferrer noopener">
          <IconButton className={classes.iconButton}>
            <img src="/static/img/icons/map.png" alt="地圖" />
          </IconButton>
        </a>
      </>
    );
  } else if (data.AttachedFiles.length > 1) {
    return (
      <>
        <Slider {...alternativeSetting}>
          {data.AttachedFiles.map((item, index) => (
            <div className={classes.imageBox} key={index}>
              <img
                className={classes.image}
                src={`${process.env.URL}${item.FilePath.replace('_thumb', '')}`}
                alt="交通替代圖"
              />
            </div>
          ))}
        </Slider>
        <IconButton className={classes.iconButton}>
          <img src="/static/img/icons/map.png" alt="地圖" />
        </IconButton>
      </>
    );
  }
  return <NoData />;
};
export default AlternativeRoadDialog;
