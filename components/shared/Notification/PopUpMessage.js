import moment from 'moment';

class Notification extends React.Component {
  state = {
    message: null,
    id: null,
    showChatbot: false,

    chat_array: [
      {
        type: 1,
        message: '您好！很高興為您服務。',
        time: moment(new Date()).format('HH:mm'),
      },
    ],
  };

  // 滾動至底部
  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: 'smooth' });
  };

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  // componentDidMount = () => {
  //   var hub = $.connection.broadcastHub;
  //   this.setState({ hub: hub });
  //   $.connection.hub.url = 'https://yilan-api.champtron.com/signalr';
  //   $.connection.hub.start({ jsonp: true }).done(function () {
  //     hub.server.register();
  //     console.log('Registration on going');
  //   });
  //   hub.client.onRegisterComplete = function () {
  //     console.log('Registration completed');
  //   };
  //   hub.client.onMessage = (message) => {
  //     const new_array = [...this.state.chat_array];
  //     console.log('message', message);
  //     new_array.push({
  //       type: 1,
  //       message: message,
  //       time: moment(new Date()).format('HH:mm'),
  //     });
  //     this.setState({ chat_array: new_array });
  //   };

  //   // hub.client.refreshToken = function () {
  //   //   hub.server.refresh(getToken());
  //   // };
  // };

  handleChat = (message) => {
    console.log('M', message);
    if (message != undefined && message != '') {
      this.state.hub.server.query(message);
      const new_array = [...this.state.chat_array];
      new_array.push({
        type: 2,
        message: message,
        time: moment(new Date()).format('HH:mm'),
      });
      this.setState({ chat_array: new_array, enter_chat: '' });
    }
  };

  handleInpChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      console.log('enter press here! ');
      this.handleChat(this.state.enter_chat);
    }
  };

  render() {
    const { showChatbot, chat_array } = this.state;
    const handleShow = () => {
      this.setState({ showChatbot: !showChatbot });
    };
    return (
      <>
        <a
          href={`/personal-message/detail?id=${this.state.id}`}
          className={
            this.state.message ? 'notification active' : 'notification'
          }
          style={{ color: '#fff', fontSize: '2.3rem' }}
        >
          新訊息：{this.state.message}
        </a>

        <div className="chatbot-service">
          <img
            src="/static/img/chatbot/chatbot-client.png"
            alt="機器人icon"
            className=""
            onClick={handleShow}
          />
        </div>

        <div
          className={showChatbot ? 'chatbot-box d-block' : 'chatbot-box d-none'}
        >
          <div className={'chatbot-box'}>
            <img
              src="/static/img/chatbot/chatbot-client.png"
              alt="機器人icon"
              className="chatbot-icon"
            />
            <div className="head">
              <div className="title">
                <div className="close" onClick={handleShow}>
                  ✕
                </div>
                臺南市政府交通局
              </div>
              <div className="subtitle">Chat</div>
            </div>
            <div className="body">
              {chat_array.map((item) =>
                item.type == 1 ? (
                  <div className="service" key={item.message}>
                    <img src="/static/img/chatbot/chatbot-box.png" alt="" />
                    <div className="message">{item.message}</div>
                    <div className="time pt-3 pl-2">{item.time}</div>
                  </div>
                ) : item.type == 2 ? (
                  <div className="client" key={item.message}>
                    <div className="time">{item.time}</div>
                    <div className="message">{item.message}</div>
                  </div>
                ) : null
              )}
              <div
                style={{ float: 'left', clear: 'both' }}
                ref={(el) => {
                  this.messagesEnd = el;
                }}
              ></div>
            </div>
            <div className="input-box">
              <input
                type="text"
                name="enter_chat"
                onChange={this.handleInpChange}
                placeholder="請輸入文字"
                onKeyPress={this.handleKeyPress}
                value={this.state.enter_chat}
                // value={'hello'}
              />
              <button
                //onEnter={() => this.handleChat(this.state.enter_chat)}
                onClick={() => this.handleChat(this.state.enter_chat)}
              >
                <img
                  src="/static/img/chatbot/enter-button.png"
                  alt="傳送訊息"
                />
              </button>
            </div>
            <div className="base"></div>
          </div>
        </div>
      </>
    );
  }
}

export default Notification;
