import ChartSliderSection from './ChartSliderSection';
import ImageSliderSection from './ImageSliderSection';

import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  container: {
    textAlign: 'center',
    width: '100%',
    minHeight: '50vh',
    background: "url('/static/img/bg/bg05.png') repeat-y center center",
    backgroundSize: '100%',
  },
  box: { maxWidth: '1200px', margin: '0px auto', padding: '40px 0px' },
}));
const ChartImageSliderSection = ({resData}) => {
  const classes = useStyles();
  return (
    <Container className={classes.container} maxWidth={false}>
      <Box className={classes.box}>
        <ChartSliderSection />
        <ImageSliderSection infoGraph={resData?.InfoGraph}/>
      </Box>
    </Container>
  );
};
export default ChartImageSliderSection;
