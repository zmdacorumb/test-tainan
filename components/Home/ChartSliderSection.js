import { useState } from 'react';
import Slider from 'react-slick';

// chart
import BarChartComponent from '../chart/BarChartComponent';
import LineChartComponent from '../chart/LineChartComponent';
import PieChartComponent from '../chart/PieChartComponent';
// slider
import {
  // serviceBannerSettings,
  analysisBannerSettings,
} from '../../data/slick-settings';
// material ui
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  CSContainer: {
    marginBottom: '120px',
  },
  cardOuter: {
    backgroundColor: '#eafbff',
    borderRadius: '10px',
    boxShadow: '0 0 10px 0 #666',
    width: '100%',
    boxSizing: 'border-box',
    overflow: 'auto',
  },
}));

const ChartSliderSection = () => {
  const classes = useStyles();
  const [groupSwitch, setGroupSwitch] = useState(0);

  return (
    <div className={`${classes.CSContainer} homeSlider`}>
      <Slider {...analysisBannerSettings}>
        <div className={classes.cardOuter}>
          <BarChartComponent />
        </div>
        <div className={classes.cardOuter}>
          <LineChartComponent />
        </div>
        <div className={classes.cardOuter}>
          <PieChartComponent />
        </div>
      </Slider>
    </div>
  );
};
export default ChartSliderSection;
