import { useRouter } from 'next/router';

import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import { tabletLinkGroup } from '../../data/home-data';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  container: {
    width: '100%',
    margin: theme.spacing(4, 'auto'),
    padding: theme.spacing(0, 4),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(0, 2),
    },
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(0),
    },
  },
  trafficTitle: {
    color: 'rgba(0,0,0,0.7)',
    fontSize: '3.2rem',
    fontWeight: '700',
  },
  trafficDescription: { color: 'rgba(0,0,0,0.4)', fontSize: '1.8rem' },
  groupTitleBox: {
    zIndex: '2',
    top: '30px',
    position: 'relative',
    backgroundColor: '#386136',
    display: 'inline-flex',
    borderRadius: '5px',
    padding: theme.spacing(1.6, 2),
  },
  groupIcon: { height: '22px', margin: '0px 8px 0px 0px' },
  typography: { fontSize: '1.6rem', color: 'white' },
  list: {
    padding: theme.spacing(3, 0, 1, 0),
    border: '1px solid rgba(83,140,80,0.5)',
    borderRadius: '5px',
  },
  listItem: {
    whiteSpace: 'nowrap',
    display: '-webkit-box',
    WebkitBoxOrient: 'vertical',
    WebkitLineClamp: '1',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    wordBreak: 'keep-all',
    color: '#323c4b',
    margin: theme.spacing(1.5, 1),
    width: 'calc(100% - 16px)',
    backgroundColor: 'rgba(212,245,241,0.5)',
    borderRadius: '20px',
    '&:hover,&.Mui-focusVisible': {
      backgroundColor: 'rgba(190,220,216,0.5)',
    },
  },
  listItemMore: {
    width: 'fit-content',
    margin: '8px 8px 0px auto',
    color: '#386136',
    backgroundColor: 'white',
    textAlign: 'right',
  },
}));
const TabletLinkSection = ({ resData }) => {
  const classes = useStyles();
  const router = useRouter();

  const enterRouter = (path) => (e) => {
    if (e.keyCode != 13) return;
    router.push(path);
  };

  return (
    <Grid
      container
      className={classes.container}
      justify="space-between"
      spacing={2}
    >
      
      <Grid item xs={12}>
        <div className={classes.trafficTitle}>交通服務</div>
        <div className={classes.trafficDescription}>需要什麼交通服務呢？</div>
      </Grid>
      {tabletLinkGroup.map((item) => (
        <Grid item xs={6} sm={4} md={3} key={item.text}>
          <Box className={classes.groupTitleBox}>
            <img
              src={item.img}
              alt="連結群組圖示"
              className={classes.groupIcon}
            />
            <Typography className={classes.typography} component="div">
              {item.text}
            </Typography>
          </Box>
          {/* Issue：內容局部來自api，局部來自前端寫靜態(互動專區、交通圖資) */}
          {/* 控制方式為 home-data中 tabletLinkGroup 的 value */}
          <List className={classes.list}>
            {item.value
              ? resData[item.value].slice(0, 4).map((innerItem) => (
                  <a
                    key={innerItem.ID}
                    href={
                      innerItem.Links ||
                      `${item.path}/detail?id=${innerItem.ID}`
                    }
                    target="_blank"
                    rel="noreferrer noopener"
                  >
                    <ListItem
                      button
                      className={classes.listItem}
                      onKeyDown={enterRouter(innerItem.link)}
                    >
                      {innerItem.Title}
                    </ListItem>
                  </a>
                ))
              : item.sub.map((innerItem) => (
                  <a
                    key={innerItem.text}
                    href={innerItem.link}
                    target="_blank"
                    rel="noreferrer noopener"
                  >
                    <ListItem
                      button
                      className={classes.listItem}
                      onKeyDown={enterRouter(innerItem.link)}
                    >
                      {innerItem.text}
                    </ListItem>
                  </a>
                ))}
            {/* 如果資料超出四項，則有More的按鈕 */}
            {item.value &&
              resData[item.value].slice(4, 5).map((innerItem, innerIndex) => (
                <a
                  key={innerIndex}
                  href={item.path}
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  <ListItem
                    button
                    className={`${classes.listItem} ${classes.listItemMore}`}
                    onKeyDown={enterRouter('/other')}
                  >
                    more
                  </ListItem>
                </a>
              ))}
          </List>
        </Grid>
      ))}
    </Grid>
  );
};
export default TabletLinkSection;
