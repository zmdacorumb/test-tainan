import Slider from 'react-slick';

import PrevArrow from '../shared/Button/PrevArrow';
import NextArrow from '../shared/Button/NextArrow';

import { fiveBannerSettings } from '../../data/slick-settings';

// extra slick settings
const settings = {
  ...fiveBannerSettings,
  nextArrow: <NextArrow />,
  prevArrow: <PrevArrow />,

  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

const QuickLinks = ({ externalLinkData }) => {
  return (
    <section id="quick-links" className="external-links">
      <Slider {...settings}>
        {externalLinkData
          ? externalLinkData.map((externalLink) => (
              <div key={externalLink.id} className="external-link-wrap">
                <div className="external-link">
                  <a
                    href={externalLink.link}
                    target="_blank"
                    rel="noreferrer noopener"
                  >
                    <img src={externalLink.imgUrl} alt={externalLink.title} />
                  </a>
                </div>
              </div>
            ))
          : null}
      </Slider>
    </section>
  );
};

export default QuickLinks;
