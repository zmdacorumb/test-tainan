import { useState } from 'react';
import Link from 'next/link';
// material ui
import { makeStyles } from '@material-ui/core/styles';
import { Tabs } from '@material-ui/core';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  marginXAuto: {
    margin: '0 auto 0',
  },
  articleContainer: {
    width: '100%',
    maxWidth: '1200px',
    margin: 'auto',
    padding: '20px 0px 80px 0px',
  },
  facebookOuter: {
    borderRadius: '10px',
    boxShadow: '0 0 4px 0 #333',
  },
  tabs: {
    flexGrow: 1,
    // backgroundColor: theme.palette.background.paper,
  },
  tabsIndex: {
    letterSpacing: '6px',
    position: 'relative',
    zIndex: '1',
    fontSize: '1.8rem',
    borderRadius: '10px 10px 0 0',
    padding: '0 60px',
    boxShadow: '0 0 4px 0 #333',
    marginTop: '5px',
    color: '#333333',
    opacity: '1',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.6rem',
      padding: '0 30px',
    },
    [theme.breakpoints.down('xs')]: {
      padding: '0 15px',
    },
  },
  tabs0: {
    backgroundImage:
      'linear-gradient( 90deg,rgba(222,251,128,1.0),rgba(209,245,137,1.0))',
    marginLeft: '5px',
  },
  tabs1: {
    backgroundImage:
      'linear-gradient( 90deg,rgba(245,215,63,1.0),rgba(236,191,45,1.0))',
    marginLeft: '20px',
  },
  tabsPanel: {
    marginLeft: '5px',
    position: 'relative',
    backgroundColor: '#aaa',
    borderRadius: '0 10px 10px 10px',
    boxShadow: '0 0 4px 0 #333',
    minHeight: '400px',
  },
  tabs0Panel: {
    backgroundImage:
      'linear-gradient( 90deg,rgba(222,251,128,1.0),rgba(175,228,162,1.0))',
  },
  tabs1Panel: {
    backgroundImage:
      'linear-gradient( 90deg,rgba(255,242,83,1.0),rgba(222,152,14,1.0))',
  },
  relative: {
    position: 'relative',
  },
  tabsImg: {
    boxShadow: '0px 0px 7px 0 rgba(0,0,0,0.3)',
    marginTop: '15px',
    width: '100%',
  },
  tabsDescriptionOuter: {
    display: 'flex',
    flexDirection: 'column',
    padding: '0 20px 0 40px',
    minHeight: '300px',
    [theme.breakpoints.down('xs')]: {
      padding: '0px',
      marginTop: '20px',
    },
  },
  tabsDescriptionTitle: {
    fontSize: '2.6rem !important',
    color: '#251e1c !important',
    height: '30px',
    overflow: 'hidden',
    display: '-webkit-box',
    WebkitBoxOrient: 'vertical',
    WebkitLineClamp: '1',
    textOverflow: 'ellipsis',
  },
  tabsDescription: {
    height: '273px',
    overflow: 'hidden',
    display: '-webkit-box',
    WebkitBoxOrient: 'vertical',
    WebkitLineClamp: '9',
    textOverflow: 'ellipsis',
    padding: '0px',
    margin: '20px 0px 30px 0px !important',
    fontSize: '1.6rem !important',
    fontWeight: '500 !important',
    lineHeight: '3rem !important',
    letterSpacing: '1px',
    color: '#251e1c',
    [theme.breakpoints.down('sm')]: {
      height: '205px',
      WebkitLineClamp: '7',
    },
    [theme.breakpoints.down('xs')]: {
      height: '155px',
      WebkitLineClamp: '5',
    },
  },
  tabsButton: {
    fontWeight: '400',
    backgroundColor: '#9b8978',
    color: '#fff',
    fontSize: '1.2rem',
    display: 'block',
    alignSelf: 'flex-end',
    marginTop: 'auto',
  },
  facebookIframe: {},
}));

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const AdditionalSection = ({ reWidth, resData }) => {
  const [value, setValue] = useState(0);
  const classes = useStyles();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <section className="additional-section mt-0">
      <div className="bg-shadow-03 pt-5">
        <img
          src="/static/img/bg/bg06.png"
          width="100%"
          className="img-static-03"
        />
        <Grid container className={classes.articleContainer} spacing={5}>
          <Grid
            item
            xs={11}
            sm={11}
            md={8}
            lg={7}
            className={classes.marginXAuto}
          >
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="simple tabs example"
              scrollButtons="off"
            >
              <Tab
                label="出版品"
                className={[classes.tabsIndex, classes.tabs0].join(' ')}
                {...a11yProps(0)}
              />
              <Tab
                label="文創品"
                className={[classes.tabsIndex, classes.tabs1].join(' ')}
                {...a11yProps(1)}
              />
            </Tabs>
            <TabPanel
              value={value}
              index={0}
              className={[classes.tabsPanel, classes.tabs0Panel].join(' ')}
            >
              <Grid container>
                <Grid item xs={12} sm={5}>
                  <img
                    src={`${process.env.URL}${resData.Publication[0].Images[0].FilePath}`}
                    className={classes.tabsImg}
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={7}
                  className={classes.tabsDescriptionOuter}
                >
                  <h3 className={`mt-4 ${classes.tabsDescriptionTitle}`}>
                    {resData.Publication[0].Title}
                  </h3>
                  <p className={classes.tabsDescription}>
                    {resData.Publication[0].Description}
                  </p>
                  <Link href="/information/app-products/publication">
                    <Button variant="contained" className={classes.tabsButton}>
                      更多出版商品
                    </Button>
                  </Link>
                </Grid>
              </Grid>
            </TabPanel>
            <TabPanel
              value={value}
              index={1}
              className={[classes.tabsPanel, classes.tabs1Panel].join(' ')}
            >
              <Grid container>
                <Grid item xs={12} sm={5}>
                  <img
                    src={`${process.env.URL}${resData.CultureCreative[0].AttachedFiles[0].FilePath}`}
                    className={classes.tabsImg}
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={7}
                  className={classes.tabsDescriptionOuter}
                >
                  <h3 className={`mt-4 ${classes.tabsDescriptionTitle}`}>
                    {resData.CultureCreative[0].Name}
                  </h3>
                  <p className={classes.tabsDescription}>
                    {resData.CultureCreative[0].Remark}
                  </p>
                  <Link href="/information/app-products/cultural-creative">
                    <Button variant="contained" className={classes.tabsButton}>
                      更多文創商品
                    </Button>
                  </Link>
                </Grid>
              </Grid>
            </TabPanel>
          </Grid>

          <Grid
            item
            xs={11}
            sm={11}
            md={8}
            lg={5}
            className={[classes.relative, classes.marginXAuto].join(' ')}
          >
            <div
              className={`text-center ${classes.marginXAuto}`}
              style={{
                width: reWidth.width,
                paddingTop: '53px',
              }}
            >
              <iframe
                className={classes.facebookOuter}
                src={`https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTainanTransportation%2F&tabs=timeline&width=${reWidth.width}&height=${reWidth.height}&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=157238544339565`}
                width={reWidth.width}
                height={reWidth.height}
                scrolling="no"
                frameBorder="0"
                // allowTransparency="true"
                allow="encrypted-media"
              ></iframe>
            </div>
          </Grid>
        </Grid>
      </div>
    </section>
  );
};
export default AdditionalSection;
