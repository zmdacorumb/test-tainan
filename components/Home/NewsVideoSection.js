import { useState, useEffect, Fragment } from 'react';
import Slider from 'react-slick';
import Link from 'next/link';
import moment from 'moment';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Skeleton from '@material-ui/lab/Skeleton';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import Box from '@material-ui/core/Box';
import Hidden from '@material-ui/core/Hidden';

import {
  // infoNewsSettings,
  videosBannerSettings,
} from '../../data/slick-settings';
// import { newsData, videoData } from '../../data/home-data';
import TabletLinkSection from './TabletLinkSection';
import unitData from '../../data/unit-data';

const newsCategory = ['動態消息', '交通管制', '徵才公告', '政府採購'];

const useStyles = makeStyles((theme) => ({
  container: {
    textAlign: 'center',
    width: '100%',
    minHeight: '25vh',
    background: "url('/static/img/bg/bg01.png') repeat-y center center",
    backgroundSize: '100%',
    position: 'relative',
    marginTop: '-10vw',
    zIndex: '10',
  },
  gridContainer: {
    maxWidth: '1600px',
    margin: '0px auto',
    padding: '50px 0px 80px 0px',
  },
  newsSection: {
    padding: '0px 60px',
    [theme.breakpoints.down('sm')]: {
      padding: '0px 40px',
    },
    [theme.breakpoints.down('sm')]: {
      padding: '0px 30px',
    },
    [theme.breakpoints.down('xs')]: {
      padding: '0px 20px',
    },
  },
  newsTitle: {
    userSelect: 'none',
    textAlign: 'left',
    color: '#386136',
    marginBottom: '20px',
    fontSize: '4rem',
  },
  tabIndicator: {
    display: 'none',
  },
  newsTab: {
    lineHeight: '20px',
    minWidth: 'unset',
    backgroundColor: '#538c50',
    color: 'white',
    margin: '0px 10px 0px 0px',
    borderRadius: '5px 5px 0px 0px',
    fontSize: '1.8rem',
    fontWeight: '400',
    letterSpacing: '1px',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.4rem',
    },
  },
  listItem: {
    padding: theme.spacing(0.75, 1),
    [theme.breakpoints.down('xs')]: {
      flexWrap: 'wrap',
    },
  },
  dateCategoryGrid: {
    display: 'flex',
    flex: '1 0 0',
    justifyContent: 'center',
    alignItems: 'center',
    maxWidth: '220px',
    flexWrap: 'wrap',
    [theme.breakpoints.down('xs')]: {
      maxWidth: '100px',
    },
  },
  itemDate: {
    fontSize: '1.6rem',
    color: '#386136',
    fontWeight: '500',
    flex: '0 0 100px',
    lineHeight: '3.2rem',
    textAlign: 'center',
    marginRight: '10px',
    [theme.breakpoints.down('xs')]: {
      fontSize: '1.4rem',
      flex: '0 0 80px',
      margin: '0px',
    },
    [theme.breakpoints.up('lg')]: {
      textAlign: 'left',
      marginRight: '0px',
    },
  },
  itemCategory: {
    fontSize: '1.6rem',
    backgroundColor: '#538c50',
    color: 'white',
    maxWidth: '120px',
    textAlign: 'center',
    borderRadius: '5px',
    lineHeight: '3.2rem',
    flex: '0 0 100px',
    [theme.breakpoints.down('xs')]: {
      flex: '0 0 80px',
      fontSize: '1.4rem',
      backgroundColor: 'rgb(237, 243, 237)',
      color: '#386136',
    },
  },
  titleGrid: {
    display: 'flex',
    alignItems: 'center',
    flex: '1 0 0',
  },
  itemTitle: {
    fontSize: '1.6rem',
    color: '#386136',
    fontWeight: '400',
    paddingLeft: '10px',
    [theme.breakpoints.down('xs')]: {
      fontWeight: '400',
    },
  },
  skeleton: { margin: '0px 24px 0px 0px' },
  tabsDivider: {
    backgroundColor: '#386136',
  },
  calendarButton: {
    // color: 'white',
    fontSize: '1.6rem',
    borderRadius: '50px',
    [theme.breakpoints.down('xs')]: {
      fontWeight: '400',
    },
  },
  newsButtonSection: {
    margin: theme.spacing(2, 0),
  },
  videoSection: {
    padding: '80px 60px 0px 60px',
    [theme.breakpoints.down('xs')]: {
      padding: '20px',
    },
  },
  videoBox: {
    backgroundColor: '#def6f2',
    padding: theme.spacing(3, 5),
    borderRadius: '10px',
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(3, 2),
    },
  },
  videoTitle: {
    color: '#21513a',
    fontSize: '2.2rem',
    textAlign: 'left',
    marginBottom: theme.spacing(2),
  },
  videoImage: {
    width: '100%',
  },
}));

const NewsVideoSection = ({ portalRef, resData }) => {
  const classes = useStyles();
  const [newsSwitch, setNewsSwitch] = useState(0);
  const handleSwitchNews = (event, newValue) => {
    setNewsSwitch(newValue);
  };
  console.log('resData', resData);

  const unitIDCorrespondingName = (unitID) => {
    let uniDIDUpper = unitID.toUpperCase();
    return unitData.find((item) => item.id == uniDIDUpper)?.title || '無此單位';
  };
  return (
    <Container className={classes.container} maxWidth={false}>
      <Button
        component="a"
        href="#C"
        accessKey="C"
        name="C"
        title="主要內容區塊"
        className="accesskey-content"
      >
        :::
      </Button>
      <Hidden lgUp>
        {/* 僅平板、手機才會出現的按鈕 */}
        <TabletLinkSection resData={resData} />
      </Hidden>
      <Grid container className={classes.gridContainer}>
        {/* 最新消息 */}
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          lg={6}
          className={classes.newsSection}
        >
          <Typography className={classes.newsTitle} component="div">
            最新消息
          </Typography>
          <Tabs
            value={newsSwitch}
            onChange={handleSwitchNews}
            aria-label="news tabs"
            variant="fullWidth"
            classes={{
              root: classes.newsTabs,
              indicator: classes.tabIndicator,
            }}
          >
            <Tab
              label="動態消息"
              className={classes.newsTab}
              {...a11yProps(0)}
            />

            <Tab
              label="交通管制"
              className={classes.newsTab}
              {...a11yProps(1)}
            />
            <Tab
              label="徵才公告"
              className={classes.newsTab}
              {...a11yProps(2)}
            />
            <Tab
              label="政府採購"
              className={classes.newsTab}
              {...a11yProps(3)}
            />
          </Tabs>
          <Divider className={classes.tabsDivider} />
          {/* <TabPanel value={newsSwitch} index={0}> */}
          <TabPanel value={newsSwitch} index={newsSwitch}>
            <List>
              {resData?.Announcement[`${newsSwitch}`]?.Announcements?.map(
                (item, index) => (
                  <Fragment key={index}>
                    <Link
                      href={`/information/announcement/dynamic-news/detail?id=${item.ID}`}
                    >
                      <ListItem button className={classes.listItem}>
                        <Grid container>
                          <Grid
                            item
                            xs={3}
                            sm={3}
                            className={classes.dateCategoryGrid}
                          >
                            <ListItemText
                              primary={moment(
                                item.Announcement.CreateTime
                              ).format('YYYY/MM/DD')}
                              className={classes.itemDate}
                              disableTypography
                            />
                            <ListItemText
                              primary={unitIDCorrespondingName(
                                item.Announcement.PublishUnit
                              )}
                              className={classes.itemCategory}
                              disableTypography
                            />
                          </Grid>
                          <Grid
                            xs={9}
                            sm={9}
                            item
                            className={classes.titleGrid}
                          >
                            <ListItemText
                              primary={item.Announcement.Title}
                              className={classes.itemTitle}
                              disableTypography
                            />
                          </Grid>
                        </Grid>
                      </ListItem>
                    </Link>
                    <Divider />
                  </Fragment>
                )
              )}
            </List>
          </TabPanel>

          {/* {[newsSwitch].map((item,idx) => (
            <TabPanel value={newsSwitch} index={item} key={idx}>
              {newsData.map((item, index) => (
                <Fragment key={index}>
                  <ListItem button className={classes.listItem}>
                    <Grid container item xs={3} sm={3} lg={4}>
                      <Skeleton
                        variant="text"
                        width={80}
                        height={40}
                        animation="wave"
                        className={classes.skeleton}
                      />
                      <Skeleton
                        variant="text"
                        width={80}
                        height={40}
                        animation="wave"
                        className={classes.skeleton}
                      />
                    </Grid>
                    <Grid xs={9} sm={9} lg={8} item>
                      <Skeleton variant="text" height={40} animation="wave" />
                    </Grid>
                  </ListItem>
                  <Divider />
                </Fragment>
              ))}
            </TabPanel>
          ))} */}

          <Grid
            container
            justify="space-between"
            className={classes.newsButtonSection}
          >
            <Button
              variant="contained"
              color="secondary"
              className={classes.calendarButton}
              startIcon={<EventAvailableIcon />}
            >
              活動行事曆
            </Button>
            <Button variant="contained" color="primary">
              More
            </Button>
          </Grid>
        </Grid>
        {/* video */}
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          lg={6}
          className={classes.videoSection}
        >
          <Box className={classes.videoBox}>
            <Slider {...videosBannerSettings}>
              {resData?.Video?.map((item, index) => (
                <Fragment key={item.ID}>
                  <Typography className={classes.videoTitle} component="div">
                    {item.VideoName}
                  </Typography>
                  {item.VideoUrl ? (
                    <iframe
                      width="100%"
                      src={`https://www.youtube.com/embed/${item.VideoUrl}`}
                      frameBorder="0"
                      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                      allowFullScreen
                      className="index-iframe"
                    ></iframe>
                  ) : (
                    <Skeleton variant="rect" height={300} animation="wave" />
                  )}
                </Fragment>
              ))}
            </Slider>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
};
const a11yProps = (index) => {
  return {
    id: `news-tab-${index}`,
    'aria-controls': `news-tabpanel-${index}`,
  };
};
const TabPanel = (props) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`news-tabpanel-${index}`}
      aria-labelledby={`news-tab-${index}`}
      {...other}
    >
      {value === index && children}
    </div>
  );
};
export default NewsVideoSection;
