import { useState, useEffect, useRef } from 'react';
// import Slider from 'react-slick';
// import Link from 'next/link';

import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import AOS from 'aos';

import { linkGroup, interactiveTrafficLink } from '../../data/home-data';

const useStyles = makeStyles((theme) => ({
  backgroundBox: {
    position: 'relative',
  },
  monotoneBuilding01: {
    position: 'absolute',
    zIndex: '5',
    width: '75%',
    right: '5%',
    opacity: '0.6',
  },
  monotoneBuilding02: {
    position: 'absolute',
    zIndex: '5',
    top: '80vh',
    width: '75%',
    left: '4.3%',
    opacity: '0.6',
  },
  greenWhiteSplit: {
    width: '100%',
  },
  boxContainer: {
    position: 'relative',
    background: "url('/static/img/bg/bg03.png') repeat-y center center",
    backgroundSize: '100%',
    padding: '0px',
  },
  linkContainer: {
    width: '100%',
    maxWidth: '1600px',
    margin: '0px auto',
    padding: '0px 86px',
    position: 'relative',
    top: '-200px',
  },
  gridItem: {
    position: 'relative',
  },
  linkGroupButton: {
    fontWeight: '400',
    zIndex: '15',
    borderRadius: '10px',
    display: 'flex',
    flexWrap: 'wrap',
    width: '190px',
    height: '190px',
    fontSize: '2.6rem',
    color: '#3e3a39',
    backgroundColor: 'white',
    border: '6px solid transparent',
    '&:hover': {
      backgroundColor: '#e8e8e8',
    },
    '& .text': {
      position: 'absolute',
      bottom: '8px',
      left: '50%',
      transform: 'translateX(-50%)',
      width: '100%',
    },
    [theme.breakpoints.down('lg')]: {
      width: '160px',
      height: '160px',
      fontSize: '2.2rem',
    },
  },
  greenBorder: {
    border: '6px solid #386136',
    boxShadow: 'none',
  },
  selectedTriangle: {
    zIndex: '14',
    borderRadius: '10px',
    position: 'absolute',
    display: 'block',
    top: '88%',
    left: '50%',
    transform: 'translate(-50%,-52%) rotate(45deg)',
    content: '""',
    width: '40px',
    height: '40px',
    backgroundColor: '#386136',
  },
  linkGroupIcon: {
    width: '88px',
    margin: '0px 0 35px 0',
    [theme.breakpoints.down('md')]: {
      width: '70px',
    },
  },
  subLinkContainer: {
    width: '80%',
    maxWidth: '1300px',
    margin: theme.spacing(4, 'auto'),
    backgroundColor: 'rgba(56,97,54,0.5)',
    padding: theme.spacing(2, 4),
    borderRadius: '12px',
    position: 'relative',
    top: '-200px',
  },
  subLinkButton: {
    fontWeight: '400',
    color: '#323c4b',
    padding: theme.spacing(1, 3),
    borderRadius: '50px',
    width: '100%',
    zIndex: '20',
    backgroundColor: '#b3e9dd',
    fontSize: '1.8rem',
    '&:hover': {
      backgroundColor: 'white',
    },
  },
  imageSize: {
    height: '100%',
    width: '100%',
  },
  // ITL (interactiveTrafficLink)
  ITLContainer: {
    width: '80%',
    maxWidth: '1300px',
    margin: theme.spacing(-40, 'auto', 0, 'auto'),
    padding: theme.spacing(2, 4),
    position: 'relative',
    top: '300px',
  },
  ITLSubContainer: {
    margin: '0px',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center',
    },
    [theme.breakpoints.down('md')]: {
      margin: '40px 0px',
    },
  },
  ITLCategory: {
    marginBottom: '-24px',
    fontSize: '4rem',
  },
  greenBlueSplit: {
    width: '100%',
  },
}));
const DesktopLinkSection = ({ resData }) => {
  const classes = useStyles();
  const [groupSwitch, setGroupSwitch] = useState(0);
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  }, []);

  return (
    <>
      <Box className={classes.backgroundBox}>
        <img
          data-aos-offset="600"
          data-aos-duration="600"
          data-aos="zoom-in-up"
          // data-aos-once="true"
          src="/static/img/bg/building-01.png"
          className={classes.monotoneBuilding01}
          alt="monotoneBuildingBackground"
        />
        <img
          data-aos="fade-right"
          data-aos-offset="600"
          data-aos-duration="600"
          // data-aos-once="true"
          src="/static/img/bg/building-02.png"
          className={classes.monotoneBuilding02}
          alt="monotoneBuildingBackground"
        />
        <img
          src="/static/img/bg/bg02.png"
          className={classes.greenWhiteSplit}
          alt="greenWhiteSplit"
        />
      </Box>
      <Box className={classes.boxContainer}>
        <Grid
          className={classes.linkContainer}
          container
          spacing={4}
          justify="center"
        >
          {linkGroup.map((item, index) => (
            <Grid item key={index} className={classes.gridItem}>
              <Button
                startIcon={
                  <img
                    src={item.img}
                    alt="linkGroupButtonIcon"
                    className={classes.imageSize}
                  />
                }
                classes={{
                  contained: `${classes.linkGroupButton} ${
                    index == groupSwitch ? classes.greenBorder : ''
                  }`,
                  label: classes.linkGroupLabel,
                  startIcon: classes.linkGroupIcon,
                }}
                variant="contained"
                onClick={() => setGroupSwitch(index)}
              >
                <span className="text">{item.text}</span>
              </Button>
              {index == groupSwitch && (
                <div className={classes.selectedTriangle} />
              )}
            </Grid>
          ))}
        </Grid>
        {/* 中間六個大按鈕(公共、停車、違規、自行車、事故、友善)，可以切換的，資料來自api */}
        <Grid container className={classes.subLinkContainer} spacing={2}>
          {resData[linkGroup[groupSwitch].value].map((item, index) => (
            <Grid key={index} item xs={12} sm={6} md={4} lg={3}>
              <a
                href={
                  item.Links ||
                  `${linkGroup[groupSwitch].path}/detail?id=${item.ID}`
                }
                target="_blank"
                rel="noreferrer noopener"
              >
                <Button variant="contained" className={classes.subLinkButton}>
                  {item.Title}
                </Button>
              </a>
            </Grid>
          ))}
        </Grid>
        {/* Issue：下方的兩大區塊按鈕，直接外連的，目前沒資料，前台寫靜態的 */}
        <Grid container className={classes.ITLContainer} spacing={0}>
          {interactiveTrafficLink.map((item) => (
            <Grid
              className={classes.ITLSubContainer}
              container
              item
              spacing={6}
              key={item.category}
              xs={12}
              sm={12}
              md={12}
              lg={6}
            >
              <Grid item xs={12}>
                <Typography className={classes.ITLCategory} component="div">
                  {item.category}
                </Typography>
              </Grid>
              {item.sub.map((innerItem, index) => (
                <Grid item key={index}>
                  {/* <Link href={innerItem.link}> */}
                  <a
                    href={innerItem.link}
                    target="_blank"
                    rel="noreferrer noopener"
                  >
                    <Button
                      startIcon={
                        <img
                          src={innerItem.img}
                          alt="linkGroupButtonIcon"
                          className={classes.imageSize}
                        />
                      }
                      classes={{
                        contained: classes.linkGroupButton,
                        label: classes.linkGroupLabel,
                        startIcon: classes.linkGroupIcon,
                      }}
                      variant="contained"
                    >
                      <span className="text">{innerItem.text}</span>
                    </Button>
                    {/* </Link> */}
                  </a>
                </Grid>
              ))}
            </Grid>
          ))}
        </Grid>
      </Box>
      <Box className={classes.backgroundBox}>
        <img
          src="/static/img/bg/bg04.png"
          className={classes.greenBlueSplit}
          alt="greenBlueSplit"
        />
      </Box>
    </>
  );
};
export default DesktopLinkSection;
