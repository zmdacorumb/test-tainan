import { useState } from 'react';
import Slide from 'react-slick';
import { homeTopBannerSettings } from '../../data/slick-settings';

import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme) => ({
  bannerSection: {
    width: '100%',
    minHeight: '25vh',
    background: "url('/static/img/bg/bg01.png') repeat-y center center",
    backgroundSize: '100%',
    position: 'relative',
  },
  containerCustom: {
    width: '100%',
    maxWidth: '91.5% !important',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  wrapperImage: {
    width: '100%',
    marginTop: '-24vw',
    position: 'relative',
    zIndex: '5',
    pointerEvents: 'none',
    [theme.breakpoints.down('md')]: {
      marginTop: '-26vw',
    },
    [theme.breakpoints.down('sm')]: {
      marginTop: '-37vw',
    },
    [theme.breakpoints.down('xs')]: {
      marginTop: '-40vw',
    },
  },
}));

const BannerSection = ({ resData }) => {
  const classes = useStyles();
  return (
    <section className={classes.bannerSection}>
      <div className={classes.containerCustom}>
        <Banner banner={resData.Banner} />
      </div>
      <Box position="relative">
        <img
          src="/static/img/bg/banner-wrapper.png"
          className={classes.wrapperImage}
        />
      </Box>
    </section>
  );
};
const Banner = ({ banner }) => {
  const [anim, setAnim] = useState(false);
  // console.log('banner',banner)
  return (
    <section id="home-banner">
      <Slide
        {...homeTopBannerSettings}
        beforeChange={() => setAnim(true)}
        afterChange={() => setAnim(false)}
      >
        {banner
          ? banner.map((item) => (
              <div className="banner-wrap" key={item.ID}>
                {item.LinkUrl ? (
                  <a
                    href={item.LinkUrl}
                    style={{ cursor: 'pointer' }}
                    target="_blank"
                    rel="noreferrer noopener"
                  >
                    <div
                      className="banner-image"
                      style={{
                        background: `url(${process.env.URL}${item.ImgPath}) no-repeat top center`,
                        backgroundSize: 'contain',
                        height: '65vw',
                      }}
                    ></div>
                  </a>
                ) : (
                  <div
                    className="banner-image"
                    style={{
                      background: `url(${process.env.URL}${item.ImgPath}) no-repeat top center`,
                      backgroundSize: 'contain',
                      height: '65vw',
                    }}
                  ></div>
                )}
              </div>
            ))
          : null}
      </Slide>

      <div id="menu-container">
        <div
          className={anim == true ? 'menu-reveal anim' : 'menu-reveal'}
          id="reveal1"
        ></div>
        <div
          className={anim == true ? 'menu-reveal anim' : 'menu-reveal'}
          id="reveal2"
        ></div>
        <div
          className={anim == true ? 'menu-reveal anim' : 'menu-reveal'}
          id="reveal3"
        ></div>

        <div id="menu-content"></div>
      </div>
    </section>
  );
};
export default BannerSection;
