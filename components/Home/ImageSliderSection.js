import { useState } from 'react';
import Slider from 'react-slick';
import Lightbox from 'react-image-lightbox';

// chart
// import BarChartComponent from '../chart/BarChartComponent';
// import LineChartComponent from '../chart/LineChartComponent';
// import PieChartComponent from '../chart/PieChartComponent';
// slider
import {
  // serviceBannerSettings,
  analysisBannerSettings,
} from '../../data/slick-settings';
// material ui
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  cardOuter: {
    backgroundColor: '#eafbff',
    borderRadius: '10px',
    boxShadow: '0 0 10px 0 #666',
    width: '100%',
    boxSizing: 'border-box',
    overflow: 'auto',
  },
}));

const ChartSliderSection = ({ infoGraph }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [photoIndex, setPhotoIndex] = useState(0);
  const classes = useStyles();
  // console.log('infoGraph',infoGraph)
  // console.log('photoIndex',photoIndex)
  return (
    <div className="homeSlider">
      <Slider {...analysisBannerSettings}>
        {infoGraph?.map((item, index) => (
          <div
            className={classes.cardOuter}
            key={item.ID}
            onClick={() => {
              setIsOpen(true);
              setPhotoIndex(index);
            }}
          >
            <img
              src={`${process.env.URL}${item.ImagePath}`}
              width="100%"
              art={item.Title}
            />
          </div>
        ))}
      </Slider>
      {isOpen && (
        <Lightbox
          mainSrc={process.env.URL + infoGraph[photoIndex].ImagePath}
          // nextSrc={infoGraph[(photoIndex + 1) % infoGraph.length]}
          // prevSrc={infoGraph[(photoIndex + infoGraph.length - 1) % infoGraph.length]}
          onCloseRequest={() => setIsOpen(false)}
          onMovePrevRequest={() =>
            setPhotoIndex({
              photoIndex:
                (photoIndex + infoGraph.length - 1) % infoGraph.length,
            })
          }
          onMoveNextRequest={() =>
            setPhotoIndex({
              photoIndex: (photoIndex + 1) % infoGraph.length,
            })
          }
        />
      )}
    </div>
  );
};
export default ChartSliderSection;
