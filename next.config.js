// NEXTJS CONFIG

module.exports = {
  env: {
    URL: 'https://tainan-traffic.champtron.com/',
    // herokuDomain: 'https://dc-tainan-transportation.herokuapp.com/',
    // URL: 'http://192.168.0.24:65432', // api address
  },

  // FOR POLYFILL DO NOT REMOVE
  webpack: function (cfg) {
    const originalEntry = cfg.entry;
    cfg.entry = async () => {
      const entries = await originalEntry();

      if (
        entries['main.js'] &&
        !entries['main.js'].includes('./client/polyfills.js')
      ) {
        entries['main.js'].unshift('./client/polyfills.js');
      }

      return entries;
    };

    return cfg;
  },
};
