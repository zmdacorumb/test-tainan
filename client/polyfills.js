// Run and load file at the very beginning (even before React and Next.js core)

// import includes from 'core-js/library/fn/string/virtual/includes';
// import repeat from 'core-js/library/fn/string/virtual/repeat';
// import assign from 'core-js/library/fn/object/assign';
// import find from 'core-js/fn/array/find';

// import 'core-js/es7/array';

// // polyfills

// // includes
// String.prototype.includes = includes;
// // repeat
// String.prototype.repeat = repeat;
// // find
// if (!Array.find) {
//   Array.find = find;
// }
// // assign
// Object.assign = assign;

// console.log('...polyfills loaded');
