import { createMuiTheme } from '@material-ui/core/styles';

//zeplin 的選單顏色
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#538c50',
    },
    secondary: {
      main: '#fb990e',
      contrastText: 'white',
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      vlg: 1400,
      xl: 1920,
    },
  },
});

export default theme;
