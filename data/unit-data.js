const unit = [
  {
    title: '資訊部',
    id: '92B706A2-5F1F-4C43-9073-25D250EF46D0',
  },
  {
    title: '人事室',
    id: '682F5FA7-BADB-44ED-AAB6-FF2022FACC45',
  },
  {
    title: '公共運輸處',
    id: '73C20C9F-7972-46C2-9B32-440AC3E48B35',
  },
  {
    title: '公共關係室',
    id: '583CF429-CC10-4095-8EAC-80E60951BBD0',
  },
  {
    title: '交通工程科',
    id: 'B94B5B84-03C6-48FF-97BA-486E8C516144',
  },
  {
    title: '車鑑會',
    id: 'CCBA693D-987E-4BAD-9918-F3758C1517D7',
  },
  {
    title: '其他',
    id: 'E653DDA1-9EF1-4680-93B7-F42623E46395',
  },
  {
    title: '政風室',
    id: '8EAC8071-38D8-426E-835F-745289415106',
  },
  {
    title: '秘書室',
    id: '3538D3B6-6B2B-4456-BB35-4BF270DDF872',
  },
  {
    title: '停車管理處',
    id: 'A3CD068E-A564-4B0D-95F5-FE3F840D01F4',
  },
  {
    title: '捷運工程處',
    id: '2AB23F52-B33F-4FC9-A8AF-3CC3C3781FAB',
  },
  {
    title: '裁決中心',
    id: '3495874D-2763-462F-BA2F-BEB9D8927A18',
  },
  {
    title: '會計室',
    id: 'A4FCA2B6-8983-4579-92CB-A0E1FA81DB08',
  },
  {
    title: '運輸管理科',
    id: 'CD963105-F3EA-4D81-99D4-2562B3081F93',
  },
  {
    title: '綜合規劃科',
    id: '88C9D9E7-9BD5-475F-84B5-C6687BAA151D',
  },
  {
    title: '網站管理',
    id: 'B8582AF6-1EA0-49AF-AEA7-3E746DC4434E',
  },
  // 2020/11/02更新新的，以上舊的保留，新的為Boris匯入對方網站的部門資料
  {
    title: '局長室',
    id: 'A634362D-8D9D-4770-9664-D348C23BD658',
  },
  {
    title: '副局長室',
    id: 'E412DBD9-32CF-4696-9686-D856FD4D9C1C',
  },
  {
    title: '主任祕書室',
    id: '3EF5B94F-D186-4D86-88DB-653E180396FC',
  },
  {
    title: '專門委員室',
    id: '6E0F21EB-2EC9-40FB-AEAB-31F82C9288A3',
  },
  {
    title: '民治市政中心服務處',
    id: '7FFB9D42-61A9-444E-84C6-0910658C5256',
  },
  {
    title: '臺南市停車管理處停車管理課',
    id: '3986F33B-2CF4-45DE-99CE-066CBC872267',
  },
  {
    title: '臺南市政府交通局交通工程科管制股',
    id: '8F62401E-9F30-4105-B2FD-10CB45FD1864',
  },
  {
    title: '臺南市政府交通局交通工程科維護股',
    id: '臺南市政府交通局交通工程科管制股',
  },
  {
    title: '人事室',
    id: '964750A5-C5DE-4EA4-8FAB-1748BAD7A4E7',
  },
  {
    title: '交通工程科',
    id: '9AA44B25-A520-465B-928E-1A1B943D20FA',
  },
  {
    title: '臺南市停車管理處',
    id: 'E57B6DBB-288E-4550-B6E5-1D8049A5CC74',
  },
  {
    title: '公共運輸處',
    id: 'D056CCA9-0DF7-49AA-A934-26C77BDBCEAA',
  },
  {
    title: '臺南市政府交通局綜合規劃科運輸規劃股',
    id: '849881BC-BD0B-4EBE-9BB1-2B7660BF746C',
  },
  {
    title: '臺南市政府交通局交通工程科維護股',
    id: '1029CE26-B72C-491F-9063-30E3BD70859F',
  },
  {
    title: '臺南市政府交通局交通工程科維護股',
    id: '6D0B225D-6DD6-478E-9B27-34884E74C0E0',
  },
  {
    title: '臺南市政府交通局綜合規劃科運輸規劃股',
    id: '98D4DF37-C2CB-44BE-B51E-34D246FAE577',
  },
  {
    title: '臺南市交通事件裁決中心案件管理股',
    id: 'E6FF951B-B5B6-4CCB-BF91-35FC75078C81',
  },
  {
    title: '臺南市捷運工程處工程管理課',
    id: '877DF3FC-AE3D-41E8-BE16-3B43282F6053',
  },
  {
    title: '裁決中心',
    id: '0FFF80D6-CCB8-4F5A-95C7-43A3F3B8AF3A',
  },
  {
    title: '臺南市交通事件裁決中心申訴積催股',
    id: 'C8FDDA07-A171-421A-A743-516C44CF7905',
  },
  {
    title: '臺南市公共運輸處運輸設施課',
    id: '1F31047F-F694-496D-8F77-530529B83773',
  },
  {
    title: '車鑑會',
    id: 'D1ACC674-06BB-483B-AE51-5CDEE31BCA22',
  },
  {
    title: '運輸管理科',
    id: '1BC31699-5F76-47E2-BA7A-6877075AC9FF',
  },
  {
    title: '臺南市捷運工程處籌備處 (107.8.23註銷)',
    id: 'F3D6BB18-F873-4821-A846-69FF492D1B30',
  },
  {
    title: '臺南市政府交通局交通工程科管制股',
    id: '6DC520A1-EFFD-4E6D-A484-6A958D7FE3E8',
  },
  {
    title: '臺南市捷運工程處綜合規劃課',
    id: 'D781C8C8-A1D0-43D1-AE4B-6BF37CA3914D',
  },
  {
    title: '停車管理處',
    id: 'DB142D8A-4B77-4DC8-B18B-7229B4560C6E',
  },
  {
    title: '臺南市政府交通局綜合規劃科交通行政股',
    id: 'DFF8F1EA-40CF-4111-A528-7918D03CE09C',
  },
  {
    title: '臺南市政府交通局綜合規劃科交通行政股',
    id: '4E909DF1-F86A-422D-8C9A-7F332052A384',
  },
  {
    title: '臺南市停車管理處停車營運課',
    id: '2E2F241C-4A6D-4334-AA12-832405186BCF',
  },
  {
    title: '綜合規劃科',
    id: '3AA29D44-85DC-4287-9FC1-83A9ED47D346',
  },
  {
    title: '其它',
    id: '20ABBE6E-EAD8-43D1-A431-84C6242ACF21',
  },
  {
    title: '臺南市交通事件裁決中心違規裁罰股',
    id: 'A9711097-8FF9-4127-989A-86CCAA7896F4',
  },
  {
    title: '秘書室',
    id: '47537382-8F61-414F-9EA9-9179926224E6',
  },
  {
    title: '臺南市交通事件裁決中心',
    id: 'AE1C6A3E-502E-44F6-B000-9713A004AF7C',
  },
  {
    title: '臺南市政府交通局運輸管理科管理股',
    id: 'B4E48243-6E58-4F8F-B50F-9F4D1F93DC56',
  },
  {
    title: '臺南市政府交通局',
    id: '52895DE3-7D5E-43D4-B32C-A3717B516825',
  },
  {
    title: '會計室',
    id: '283F3667-BF1A-4794-B697-A3FC66C639DD',
  },
  {
    title: '臺南市公共運輸處秘書室',
    id: '39FD3E62-1DF2-4B99-9061-A45170BF006A',
  },
  {
    title: '臺南市捷運工程處',
    id: '0FEC3E8C-56BC-411E-97BC-BBD24C913F04',
  },
  {
    title: '公共關係室',
    id: '236D74C9-B324-48AC-B80C-C2D0F37BFBD2',
  },
  {
    title: '臺南市公共運輸處運輸稽查課',
    id: '06816F0B-5A58-497F-8FE7-CDA89C98A2FC',
  },
  {
    title: '臺南市停車管理處企劃工程課',
    id: '1CF54AE2-F7DA-4CAE-A4FA-DBD859F7EE6F',
  },
  {
    title: '臺南市公共運輸處秘書辦公室',
    id: 'ECA0F3F6-6141-4BAF-A83C-DD0829A0466E',
  },
  {
    title: '臺南市政府交通局綜合規劃科交通行政股',
    id: 'AB93A18E-3162-4FA3-A618-DEEEFAB5805C',
  },
  {
    title: '網站管理',
    id: '833BAD0F-50BE-4332-80F9-E1D42D1964B0',
  },
  {
    title: '臺南市公共運輸處營運管理課',
    id: '49C639D3-40F8-493D-A4AA-EDA073BB3B9F',
  },
  {
    title: '政風室',
    id: '59D3C8C1-0535-4CA8-A1C5-F3BBD7782B83',
  },
  {
    title: '捷運工程處',
    id: '00DEABE7-6369-4565-B031-F992F10B3880',
  },
];
export default unit;
