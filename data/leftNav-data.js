// Nav List page
export const leftNav = [
  {
    title: '申辦專區',
    path: '/application',
    subNav: [
      {
        title: '表單下載',
        path: '/application/download-list',
      },
      {
        title: '線上申辦',
        path: '/application/online-service',
      },
    ],
  },
  {
    title: '交通服務',
    path: '/transportation',
    subNav: [
      {
        title: '公共運輸',
        path: '/transportation/public-transport',
      },
      {
        title: '停車拖吊',
        path: '/transportation/parking-trailer',
      },
      {
        title: '違規裁罰',
        path: '/transportation/violation-penalty',
      },
      {
        title: '道安宣導',
        path: '/transportation/road-safety-advocacy',
      },
      {
        title: '自行車',
        path: '/transportation/bicycle',
      },
      {
        title: '替代道路專區',
        path: '/transportation/alternative-road',
      },
      {
        title: '友善交通',
        path: '/transportation/friendly-traffic',
      },
      {
        title: '遙控無人機',
        path: '/transportation/remote-control-drone',
      },
      {
        title: '行車事故鑑定',
        path: '/transportation/driving-accident-identification',
      },
      {
        title: '交通管制',
        path: '/transportation/traffic-control',
      },
    ],
  },

  {
    title: '互動意見',
    path: '/interactive-opinions',
    subNav: [
      {
        title: '意見反映',
        path: '/interactive-opinions/feedback',
      },
      {
        title: '臉友交流',
        path: '/interactive-opinions/fb-communication',
      },
      {
        title: '公共政策平臺',
        externalLink: 'https://tainan.join.gov.tw/policies/index',
        // path: '/interactive-opinions/public-policy-platform',
      },
      {
        title: '臺南市民服務平臺',
        externalLink: 'https://1999.tainan.gov.tw/',
        // path: '/interactive-opinions/sentiment-system',
      },
      {
        title: '常見問題',
        path: '/interactive-opinions/frequently-asked-question',
      },
    ],
  },

  {
    title: '資訊公開',
    path: '/information',
    subNav: [
      {
        title: '公告訊息',
        path: '/information/announcement/dynamic-news',
      },
      {
        title: '計劃、研究及法規',
        path: '/information/plans-reports-law',
      },
      {
        title: '開放資料',
        path: '/information/open-data',
      },
      {
        title: '交通法規',
        path: '/information/traffic-rules',
      },
      {
        title: '統計資料',
        path: '/information/statistical-data',
      },
      {
        title: '公開徵信/支付與接受補助',
        path: '/information/donate-info/recruit',
        subNav: [
          {
            title: '公開徵信',
            path: '/information/donate-info/recruit',
          },
          {
            title: '支付與接受補助',
            path: '/information/donate-info/allowance',
          },
        ],
      },
      {
        title: '行政資訊',
        path: '/information/administrative-information/personnel',
        subNav: [
          {
            title: '人事園地',
            path: '/information/administrative-information/personnel',
          },
          {
            title: '會計園地',
            path: '/information/administrative-information/accounting',
          },
          {
            title: '政風園地',
            path: '/information/administrative-information/political',
          },
          {
            title: '廉能園地',
            path: '/information/administrative-information/lianNeng',
          },
        ],
      },
      {
        title: 'APP/出版與文創品',
        path: '/information/app-products/app',
        subNav: [
          {
            title: 'APP',
            path: '/information/app-products/app',
          },
          {
            title: '出版品',
            path: '/information/app-products/publication',
          },
          {
            title: '文創品',
            path: '/information/app-products/cultural-creative',
          },
        ],
      },
      {
        title: '影音平台',
        path: '/information/videos',
      },
      {
        title: '相關連結',
        path: '/information/related-links',
      },
      {
        title: '政府公開資訊彙整',
        path: '/information/government-public-information-compilation',
      },
      {
        title: '性別主流化專區',
        path: '/information/gender-mainstreaming',
      },
    ],
  },
  {
    title: '團隊介紹',
    path: '/team',
    subNav: [
      {
        title: '首長簡介',
        path: '/team/chief-introduction',
      },
      {
        title: '專業執掌',
        path: '/team/professional-responsibilities',
      },
      {
        title: '組織沿革',
        path: '/team/organizational-history',
      },
      {
        title: '聯絡方式',
        path: '/team/contact-method',
      },
    ],
  },
  {
    title: '其他',
    path: '/other',
    subNav: [
      { title: '本局各單位通訊方式', path: '/other/agency-communication' },
      { title: '隱私權及安全政策', path: '/other/privacy-security-policy' },
      { title: '保有及管理個人資料', path: '/other/personal-data' },
      {
        title: '政府網站資料開放宣告',
        path: '/other/government-website-information',
      },
      { title: '內部控制聲明書', path: '/other/internal-control-statement' },
      { title: '雙語詞彙', path: '/other/bilingual-vocabulary' },
      { title: '網站導覽', path: '/other/site-map' },
    ],
  },
];
