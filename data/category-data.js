export const categoryList = [
  {
    id: 'Fine',
    category: '裁罰相關',
    subCategoryList: [
      { id: 1, category: '裁罰申訴' },
      { id: 2, category: '一般' },
    ],
  },
  {
    id: 'PublicTransport',
    category: '公共運輸',
    subCategoryList: [
      { id: 1, category: '計程車' },
      { id: 2, category: '站牌路線' },
      { id: 3, category: '公車司機' },
      { id: 4, category: '班表時刻' },
      { id: 5, category: '公共自行車' },
      { id: 6, category: '轉運站' },
      { id: 7, category: '先進運輸' },
      { id: 8, category: '其它' },
    ],
  },
  {
    id: 'Tow',
    category: '停車拖吊',
    subCategoryList: [
      { id: 1, category: '停車格' },
      { id: 2, category: '停車費' },
      { id: 3, category: '紅黃線' },
      { id: 4, category: '停車場' },
      { id: 5, category: '其它' },
    ],
  },
  {
    id: 'Sign',
    category: '號誌標線',
    subCategoryList: [],
  },
  {
    id: 'Plainning',
    category: '交通規劃',
    subCategoryList: [
      { id: 1, category: '替代道路' },
      { id: 2, category: '無人機' },
      { id: 3, category: '其它' },
    ],
  },
  {
    id: 'Accident',
    category: '事故鑑定',
    subCategoryList: [
      { id: 1, category: '鑑定' },
      { id: 2, category: '覆議' },
      { id: 3, category: '其它' },
    ],
  },
  {
    id: 'Other',
    category: '其它',
    subCategoryList: [],
  },
];
