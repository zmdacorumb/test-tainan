import { useState } from 'react';
const SvgMenuWeb = ({}) => {
  const [scheduleStatus, setscheduleStatus] = useState(2);
  return (
    <>
      <div className="svg-box d-none d-md-block">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
          id="圖層_1"
          x="0px"
          y="0px"
          viewBox="0 0 358.1 51"
          className="svg-menu-1"
        >
          <path
            id="step_done"
            className="st0 active"
            d="M2.5,50.5c-1.1,0-2-0.9-2-2v-46c0-1.1,0.9-2,2-2h337.1l17.9,25l-17.9,25H2.5z"
          />
          <rect x="138.8" y="13.9" className="st1" width="75.6" height="23.1" />
          <text
            transform="matrix(1 0 0 1 138.7589 35.0525)"
            className="st2 st3 st4"
          >
            已送審
          </text>
        </svg>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
          id="圖層_1"
          x="0px"
          y="0px"
          viewBox="0 0 714.1 51"
          className="svg-menu-2"
        >
          <path
            id="step_undo"
            className={`st0 ${
              scheduleStatus == 2 ? 'active' : scheduleStatus == 3 && 'active'
            }`}
            d="M631.7,50.5H2.5c-1.1,0-2-0.9-2-2v-46c0-1.1,0.9-2,2-2h693.1l17.9,25l-17.9,25H631.7z"
          />
          <rect x="496.2" y="13.9" className="st1" width="75.6" height="23.1" />
          <text
            transform="matrix(1 0 0 1 496.199 35.0525)"
            className="st2 st3 st4"
          >
            已分派
          </text>
        </svg>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
          id="圖層_1"
          x="0px"
          y="0px"
          viewBox="0 0 1071.1 51"
          className="svg-menu-3"
        >
          <path
            id="step_undo"
            className={`st0 ${scheduleStatus == 3 && 'active'}`}
            d="M988.2,50.5H2c-1.1,0-2-0.9-2-2v-46c0-1.1,0.9-2,2-2h1050.1l17.9,25l-17.9,25H988.2z"
          />
          <rect x="852.5" y="13.9" className="st1" width="75.6" height="23.1" />
          <text
            transform="matrix(1 0 0 1 852.5323 35.0525)"
            className="st2 st3 st4"
          >
            已回覆
          </text>
        </svg>
      </div>
      <div className="svg-box svg-box-mobile d-block d-md-none">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
          id="圖層_1"
          x="0px"
          y="0px"
          viewBox="0 0 108.1 35"
          className="svg-menu-mobile-1"
        >
          <path
            id="step_done"
            className="st0 active"
            d="M2.6,34.5c-1.1,0-2-0.9-2-2v-30c0-1.1,0.9-2,2-2h68.6c0,0,0,0,0.1,0h24.3l12.1,17l-12.1,17H2.6z  "
          />
          <rect x="28.9" y="8.8" className="st1" width="50.4" height="17.4" />
          <text transform="matrix(1 0 0 1 28.8565 22.8806)" className="st2 st5">
            已送審
          </text>
        </svg>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
          id="圖層_1"
          x="0px"
          y="0px"
          viewBox="0 0 108.1 35"
          className="svg-menu-mobile-2"
        >
          <path
            id="step_done"
            className={`st0 ${
              scheduleStatus == 2 ? 'active' : scheduleStatus == 3 && 'active'
            }`}
            d="M2.6,34.5c-1.1,0-2-0.9-2-2v-30c0-1.1,0.9-2,2-2h68.6c0,0,0,0,0.1,0h24.3l12.1,17l-12.1,17H2.6z  "
          />
          <rect x="28.9" y="8.8" className="st1" width="50.4" height="17.4" />
          <text transform="matrix(1 0 0 1 28.8566 22.8806)" className="st2 st5">
            已分派
          </text>
        </svg>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
          id="圖層_1"
          x="0px"
          y="0px"
          viewBox="0 0 108.1 35"
          className="svg-menu-mobile-3"
        >
          <path
            id="step_done"
            className={`st0 ${scheduleStatus == 3 && 'active'}`}
            d="M2.6,34.5c-1.1,0-2-0.9-2-2v-30c0-1.1,0.9-2,2-2h68.6c0,0,0,0,0.1,0h24.3l12.1,17l-12.1,17H2.6z  "
          />
          <rect x="28.9" y="8.8" className="st1" width="50.4" height="17.4" />
          <text transform="matrix(1 0 0 1 28.8566 22.8806)" className="st2 st5">
            已回覆
          </text>
        </svg>
      </div>
    </>
  );
};
export default SvgMenuWeb;
