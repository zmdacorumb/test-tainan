const centralizedConsoleLog = {
  isDev: true,
  log(...arg) {
    if (!this.isDev) return;
    console.log(...arg);
  },
};
export default centralizedConsoleLog;
