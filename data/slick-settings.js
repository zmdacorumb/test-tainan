import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import AirportShuttleIcon from '@material-ui/icons/AirportShuttle';


const SlickButtonFix = ({ currentSlide, slideCount, children, ...props }) => (
  <span {...props}>{children}</span>
);

// Settings for One Banner
export const oneBannerSettings = {
  arrows: false,
  dots: true,
  infinite: true,
  autoplay: false,
  speed: 1500,
  slidesToShow: 1,
  slidesToScroll: 1,
};

// Settings for Three Banners
export const threeBannerSettings = {
  arrows: false,
  dots: true,
  infinite: true,
  autoplay: false,
  speed: 1500,
  slidesToShow: 3,
  slidesToScroll: 3,
  swipeToSlide: true,
};

// Settings for Five Banners
export const fiveBannerSettings = {
  arrows: false,
  dots: true,
  infinite: true,
  autoplay: false,
  speed: 1500,
  slidesToShow: 5,
  slidesToScroll: 1,
  swipeToSlide: true,
};

export const homeTopBannerSettings = {
  arrows: false,
  dots: true,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 7000,
  fade: false,
  speed: 750,
  slidesToShow: 1,
  slidesToScroll: 1,
  className: 'home-siler-dots',
  appendDots: (dots) => (
    <div>
      <ul
        style={{
          listStyleType: 'none',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        {dots}
      </ul>
    </div>
  ),
  customPaging: (i) => <IconButton />,
};

//Settings for Videos Banner at Home
export const videosBannerSettings = {
  arrows: false,
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  className: 'videos-siler-dots',
  appendDots: (dots) => (
    <Grid
      container
      justify="space-around"
      style={{ margin: '16px 0px 0px 0px' }}
    >
      <ul
        style={{
          margin: '0px 0px 8px 0px',
          flexWrap: 'wrap',
          listStyleType: 'none',
          display: 'flex',
          position: 'relative',
        }}
      >
        {dots}
      </ul>
      <Button
        variant="contained"
        color="primary"
        style={{ margin: '0px 0px 8px 0px' }}
        href={'/information/videos'}
        target='_blank'
        className='index-more-button'
      >
        More
      </Button>
    </Grid>
  ),
  customPaging: (i) => <Button className="slick-dots-button" />,
};

//Settings for Service Banner at Home
export const serviceBannerSettings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 1,
  className: 'service-arrows-triangle',
  prevArrow: (
    <button type="button" className="slick-prev">
      <img src="/static/img/icons/silder-arrow-service-left.png" alt="" />
    </button>
  ),
  nextArrow: (
    <button type="button" className="slick-next">
      <img src="/static/img/icons/silder-arrow-service-right.png" alt="" />
    </button>
  ),
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
      },
    },
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      },
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

//Settings for Analysis Banner at Home
export const analysisBannerSettings = {
  arrows: false,
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  className: 'analysis-siler-dots',
  appendDots: (dots) => (
    <div>
      <ul
        style={{
          listStyleType: 'none',
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        {dots}
      </ul>
    </div>
  ),
  customPaging: (i) => <button></button>,
};

//Settings for info Img Banner at Home
export const infoImgBannerSettings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  className: 'infoImg-arrows-triangle',
  prevArrow: (
    <button type="button" className="slick-prev">
      <img src="/static/img/icons/silder-arrow-infoimg-left.png" alt="" />
    </button>
  ),
  nextArrow: (
    <button type="button" className="slick-next">
      <img src="/static/img/icons/silder-arrow-infoimg-right.png" alt="" />
    </button>
  ),
};

//Settings for footer info at Home
export const footerInfosSettings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  className: 'footerinfo-arrows-triangle',
  prevArrow: (
    <SlickButtonFix>
      <IconButton>
        <i className="fas fa-caret-left" />
      </IconButton>
    </SlickButtonFix>
  ),
  nextArrow: (
    <SlickButtonFix>
      <IconButton>
        <i className="fas fa-caret-right" />
      </IconButton>
    </SlickButtonFix>
  ),
  appendDots: (dots) => (
    <div>
      <ul
        style={{
          listStyleType: 'none',
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        {dots}
      </ul>
    </div>
  ),
  customPaging: (i) => <IconButton />,
};

export const alternativeSetting = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  className: 'alternative-arrows-triangle',
  prevArrow: (
    <SlickButtonFix>
      <IconButton>
        <i className="fas fa-caret-left" />
      </IconButton>
    </SlickButtonFix>
  ),
  nextArrow: (
    <SlickButtonFix>
      <IconButton>
        <i className="fas fa-caret-right" />
      </IconButton>
    </SlickButtonFix>
  ),
  appendDots: (dots) => (
    <div>
      <ul
        style={{
          listStyleType: 'none',
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        {dots}
      </ul>
    </div>
  ),
  customPaging: (i) => <IconButton />,
};

export const infoNewsSettings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  className: 'infoNews-arrows-triangle',
  prevArrow: (
    <button type="button" className="slick-prev">
      <img src="/static/img/icons/path_left.png" alt="" />
    </button>
  ),
  nextArrow: (
    <button type="button" className="slick-next">
      <img src="/static/img/icons/path_right.png" alt="" />
    </button>
  ),
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
      },
    },
  ],
};
export const productSettings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  className: 'product-arrows-triangle',
  arrows: false,
  appendDots: (dots) => (
    <div>
      <ul
        style={{
          listStyleType: 'none',
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        {dots}
      </ul>
    </div>
  ),
};
