const footerInfoCard = [
  {
    authorityUnit: '臺南市政府交通局',
    name: '臺南市車輛行車事故鑑定委員會',
    postalCode: '70141',
    address: '臺南市府連東路87號',
    phone: '06-2006969',
    fax: '06-2002135',
  },
  {
    authorityUnit: '臺南市政府交通局',
    name: '臺南市公共運輸處',
    postalCode: '70056',
    address: '臺南市中西區永華路一段36號4樓',
    phone: '06-2230330',
    fax: '06-2230331',
  },
  {
    authorityUnit: '臺南市政府交通局',
    name: '臺南市停車管理處',
    postalCode: '700',
    address: '臺南市中西區南門路241號',
    phone: '06-2141798',
    fax: '06-2136992',
  },
  {
    authorityUnit: '臺南市政府交通局',
    name: '臺南市捷運工程處',
    postalCode: '70801',
    address: '臺南市安平區永華路二段6號4樓',
    phone: '06-2991111',
    fax: '06-2958829',
  },
];

export default footerInfoCard;
