export const newsData = [
  {
    date: '109/03/20',
    category: '新聞稿',
    title: '河樂廣場增闢路外機車停車場 改善周邊交通環境',
    href: '/',
  },
  {
    date: '109/03/09',
    category: '綜合規劃',
    title: '中西區河樂廣場停車管制措施',
    href: '/',
  },
  {
    date: '109/03/03',
    category: '運輸管理',
    title: '網站伺服器配合網路架構調整作業公告',
    href: '/',
  },
  {
    date: '109/02/25',
    category: '新聞稿',
    title: '紅單報報 權益抱抱',
    href: '/',
  },
  {
    date: '109/02/20',
    category: '運輸管理',
    title: '臺南市自駕公車通過經濟部審查 下半年完成測試後上路載客',
    href: '/',
  },
  {
    date: '109/02/10',
    category: '綜合規劃',
    title: '「臺南市機車租賃業者汰換電動機車座談會」 媒介廠商推廣綠',
    href: '/',
  },
  {
    date: '109/02/01',
    category: '運輸管理',
    title: '8座太陽能電子紙智慧站牌上線啟用',
    href: '/',
  },
];

export const videoData = [
  {
    title: '大臺南公車輕旅行 - 行銷短片 (觀光篇)',
    video: '/static/img/home/video.png',
    href: '#',
  },
  {
    title: '大臺南南公車輕旅行 - 行銷短片 (觀光篇)2',
    // video: '/static/img/home/video.png',
    href: '#',
  },
  {
    title: '大臺南公車輕旅行 - 行銷短片 (觀光篇)3',
    // video: '/static/img/home/video.png',
    href: '#',
  },
  {
    title: '大臺南公車輕旅行 - 行銷短片 (觀光篇)4',
    // video: '/static/img/home/video.png',
    href: '#',
  },
  {
    title: '大臺南公車輕旅行 - 行銷短片 (觀光篇)5',
    // video: '/static/img/home/video.png',
    href: '#',
  },
  {
    title: '大臺南公車輕旅行 - 行銷短片 (觀光篇)6',
    // video: '/static/img/home/video.png',
    href: '#',
  },
  {
    title: '大臺南公車輕旅行 - 行銷短片 (觀光篇)7',
    // video: '/static/img/home/video.png',
    href: '#',
  },
  {
    title: '大臺南公車輕旅行 - 行銷短片 (觀光篇)8',
    // video: '/static/img/home/video.png',
    href: '#',
  },
  {
    title: '大臺南公車輕旅行 - 行銷短片 (觀光篇)9',
    // video: '/static/img/home/video.png',
    href: '#',
  },
  {
    title: '大臺南公車輕旅行 - 行銷短片 (觀光篇)10',
    // video: '/static/img/home/video.png',
    href: '#',
  },
  {
    title: '大臺南公車輕旅行 - 行銷短片 (觀光篇)11',
    // video: '/static/img/home/video.png',
    href: '#',
  },
];

export const linkGroup = [
  {
    img: '/static/img/home/group01.png',
    text: '公共運輸',
    value: 'PublicTransport',
    path: `${process.env.herokuDomain}transportation/public-transport`,
  },
  {
    img: '/static/img/home/group02.png',
    text: '停車拖吊',
    value: 'Tow',
    path: `${process.env.herokuDomain}transportation/parking-trailer`,
  },
  {
    img: '/static/img/home/group03.png',
    text: '違規裁罰',
    value: 'Fine',
    path: `${process.env.herokuDomain}transportation/violation-penalty`,
  },
  {
    img: '/static/img/home/group04.png',
    text: '自行車',
    value: 'Bicycle',
    path: `${process.env.herokuDomain}transportation/bicycle`,
  },
  {
    img: '/static/img/home/group05.png',
    text: '事故鑑定',
    value: 'AccInves',
    path: `${process.env.herokuDomain}transportation/driving-accident-identification`,
  },
  {
    img: '/static/img/home/group06.png',
    text: '友善交通',
    value: 'FriendlyTraffic',
    path: `${process.env.herokuDomain}transportation/friendly-traffic`,
  },
];

export const interactiveTrafficLink = [
  {
    category: '互動專區',
    sub: [
      {
        text: '意見互動',
        link: `${process.env.herokuDomain}interactive-opinions/feedback`,
        img: '/static/img/home/interactive01.png',
      },
      {
        text: '公共政策平臺',
        link: 'https://tainan.join.gov.tw/policies/index',
        img: '/static/img/home/interactive02.png',
      },
      {
        text: '市民服務平臺',
        link: 'https://1999.tainan.gov.tw/',
        img: '/static/img/home/interactive03.png',
      },
      {
        text: '常見問題',
        link: `${process.env.herokuDomain}interactive-opinions/frequently-asked-question`,
        img: '/static/img/home/interactive04.png',
      },
    ],
  },
  {
    category: '交通圖資',
    sub: [
      {
        text: '公共運輸規劃',
        link:
          'https://2384.tainan.gov.tw/NewTNBusWeb/transferInfo.html?Lang=cht',
        img: '/static/img/home/traffic01.png',
      },
      {
        text: '即時交通資訊',
        link: 'https://tntcc.tainan.gov.tw/TainanTraffic/login.do',
        img: '/static/img/home/traffic02.png',
      },
      {
        text: '預測剩餘格位',
        link:
          'https://parkingforecast.tainan.gov.tw:20443/prediction/ParkingPrediction',
        img: '/static/img/home/traffic03.png',
      },
      {
        text: '即時停車格位',
        link: 'https://parkmap.tainan.gov.tw/api/map',
        img: '/static/img/home/traffic04.png',
      },
    ],
  },
];

export const tabletLinkGroup = [
  {
    text: '公共運輸',
    img: '/static/img/home/tablet01.png',
    value: 'PublicTransport',
    path: `${process.env.herokuDomain}transportation/public-transport`,
    sub: [
      { text: '公車動態資訊網', link: '#' },
      { text: '公車輕旅行', link: '#' },
      { text: '公車路線圖', link: '#' },
      { text: '計程車業者資訊', link: '#' },
      { text: '捷運優先路線圖', link: '#' },
    ],
  },
  {
    text: '停車拖吊',
    img: '/static/img/home/tablet02.png',
    value: 'Tow',
    path: `${process.env.herokuDomain}transportation/parking-trailer`,
    sub: [
      { text: '違規拖吊查詢', link: '#' },
      { text: '停車繳費查詢', link: '#' },
      { text: '列印繳費通知單', link: '#' },
      { text: '交通罰緩查詢', link: '#' },
      { text: '月票購買須知', link: '#' },
      { text: '停車申訴信箱', link: '#' },
    ],
  },
  {
    text: '違規裁罰',
    img: '/static/img/home/tablet03.png',
    value: 'Fine',
    path: `${process.env.herokuDomain}transportation/violation-penalty`,
    sub: [
      { text: '交通違規查詢', link: '#' },
      { text: '違規罰緩繳納', link: '#' },
      { text: '窗口櫃檯等待人數', link: '#' },
      { text: '歸責駕駛人作業', link: '#' },
      { text: '申辦項目與檢核', link: '#' },
      { text: '違規申訴信箱', link: '#' },
      { text: '違規裁罰應備證件', link: '#' },
      { text: '電子書', link: '#' },
      { text: '全國裁決監理單位', link: '#' },
    ],
  },
  {
    text: '自行車',
    img: '/static/img/home/tablet04.png',
    value: 'Bicycle',
    path: `${process.env.herokuDomain}transportation/bicycle`,
    sub: [
      { text: '自行車道資訊', link: '#' },
      { text: 'T-Bike 公共自行車', link: '#' },
    ],
  },
  {
    text: '事故鑑定',
    img: '/static/img/home/tablet05.png',
    value: 'AccInves',
    path: `${process.env.herokuDomain}transportation/driving-accident-identification`,
    sub: [
      { text: '鑑定案件申請', link: '#' },
      { text: '鑑定案件查詢', link: '#' },
      { text: '覆議案件申請', link: '#' },
    ],
  },
  {
    text: '友善交通',
    img: '/static/img/home/tablet06.png',
    value: 'FriendlyTraffic',
    path: `${process.env.herokuDomain}transportation/friendly-traffic`,
    sub: [
      { text: '親子', link: '#' },
      { text: '孕婦', link: '#' },
      { text: '綠能', link: '#' },
      { text: '無障礙', link: '#' },
    ],
  },
  {
    text: '互動專區',
    img: '/static/img/home/tablet07.png',
    sub: [
      {
        text: '意見互動',
        link: `${process.env.herokuDomain}interactive-opinions/feedback`,
      },
      {
        text: '公共政策平臺',
        link: 'https://tainan.join.gov.tw/policies/index',
      },
      { text: '市民服務平臺', link: 'https://1999.tainan.gov.tw/' },
      {
        text: '常見問題',
        link: `${process.env.herokuDomain}interactive-opinions/frequently-asked-question`,
      },
    ],
  },
  {
    text: '交通圖資',
    img: '/static/img/home/tablet08.png',
    sub: [
      {
        text: '公共運輸規劃',
        link:
          'https://2384.tainan.gov.tw/NewTNBusWeb/transferInfo.html?Lang=cht',
      },
      {
        text: '即時交通資訊',
        link: 'https://tntcc.tainan.gov.tw/TainanTraffic/login.do',
      },
      {
        text: '預測剩餘格位',
        link:
          'https://parkingforecast.tainan.gov.tw:20443/prediction/ParkingPrediction',
      },
      { text: '即時停車格位', link: 'https://parkmap.tainan.gov.tw/api/map' },
    ],
  },
];
