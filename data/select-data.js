const selectData = [
  {
    title: '支付與接受補助',
    subNav: [
      { title: '接受補助計畫收支報告表', category: 1 },
      { title: '對民間團體補(捐)助', category: 2 },
      { title: '支出明細表', category: 3 },
      { title: '其他', category: 4 },
    ],
  },
  {
    title: '常見問題',
    subNav: [
      {
        title: '公共運輸',
        category: 1,
      },
      {
        title: '交通工程',
        category: 2,
      },
      {
        title: '停車管理',
        category: 3,
      },
      {
        title: '無人機管理',
        category: 4,
      },
      {
        title: '違規裁罰',
        category: 5,
      },
      {
        title: '綜合規劃',
        category: 6,
      },
    ],
  },
  {
    title: '雙語詞彙',
    subNav: [
      {
        title: '智慧型運輸系統',
        category: 1,
      },
      {
        title: '電信',
        category: 2,
      },
      {
        title: '軌道',
        category: 3,
      },
      {
        title: '公路',
        category: 4,
      },
      {
        title: '法規',
        category: 5,
      },
      {
        title: '一般',
        category: 6,
      },
      {
        title: '交通',
        category: 7,
      },
      {
        title: '交通號誌',
        category: 8,
      },
      {
        title: '組織',
        category: 9,
      },
      {
        title: '其他',
        category: 10,
      },
    ],
  },
  {
    title: '線上申辦',
    subNav: [
      {
        title: '交通工程',
        category: 'Engineering',
      },
      {
        title: '綜合規劃',
        category: 'Plaining',
      },
      {
        title: '停車管理',
        category: 'Parking',
      },
    ],
  },
  {
    title: '動態消息',
    subNav: [
      {
        title: '公告總覽',
        category: 1,
      },
      {
        title: '綜合規劃',
        category: 2,
      },
      {
        title: '運輸管理',
        category: 3,
      },
    ],
  },
  {
    title: '交通管制',
    subNav: [
      {
        title: '交通管制或道路施工',
        category: 1,
      },
      {
        title: '大客車禁行路段',
        category: 2,
      },
      {
        title: '無人機禁飛區堿',
        category: 3,
      },
    ],
  },
  {
    title: '徵才公告',
    subNav: [
      {
        title: '徵才公告',
        category: 1,
      },
      {
        title: '錄取通知',
        category: 2,
      },
    ],
  },
  {
    title: '政府採購',
    subNav: [
      {
        title: '招標資訊',
        category: 1,
      },
      {
        title: '決標資訊',
        category: 2,
      },
    ],
  },
  {
    title: '活動行事曆',
    subNav: [
      {
        title: '公告總覽',
        category: 1,
      },
      {
        title: '綜合規劃',
        category: 2,
      },
      {
        title: '運輸管理',
        category: 3,
      },
    ],
  },
  {
    title: '新聞稿',
    subNav: [
      {
        title: '公告總覽',
        category: 1,
      },
      {
        title: '綜合規劃',
        category: 2,
      },
      {
        title: '運輸管理',
        category: 3,
      },
    ],
  },
  {
    title: '澄清專區',
    subNav: [
      {
        title: '公告總覽',
        category: 1,
      },
      {
        title: '綜合規劃',
        category: 2,
      },
      {
        title: '運輸管理',
        category: 3,
      },
    ],
  },
  {
    title: '防疫專區',
    subNav: [
      {
        title: '公告總覽',
        category: 1,
      },
      {
        title: '綜合規劃',
        category: 2,
      },
      {
        title: '運輸管理',
        category: 3,
      },
    ],
  },
  {
    title: '計劃、研究及法規',
    subNav: [
      {
        title: '施政年度計畫',
        category: 'Annual',
      },
      {
        title: '施政成果',
        category: 'Result',
      },
      {
        title: '施政中程計畫',
        category: 'Mid-Term',
      },
      {
        title: '研究報告',
        category: 'Report',
      },
    ],
  },
  {
    title: '開放資料',
    subNav: [
      {
        title: '自行車',
        category: 1,
      },
      {
        title: '停車管理',
        category: 2,
      },
      {
        title: '交通號誌',
        category: 3,
      },
      {
        title: '公共運輸',
        category: 4,
      },
    ],
  },
  {
    title: '統計資料',
    subNav: [
      {
        title: '性別統計分析',
        category: 1,
      },
      {
        title: '年度計劃',
        category: 2,
      },
      {
        title: '性別統計指標',
        category: 3,
      },
      {
        title: '預告統計時間',
        category: 4,
      },
      {
        title: '臺南市政府交通局統計年報',
        category: 5,
      },
      {
        title: '臺南市車位概況報表',
        category: 6,
      },
      {
        title: '臺南市市區汽車客運業營運概況',
        category: 7,
      },
      {
        title: '統計通報',
        category: 8,
      },
      {
        title: '統計專題分析',
        category: 9,
      },
      {
        title: '辦理國家賠償事件統計',
        category: 10,
      },
      {
        title: '公務統計方案',
        category: 11,
      },
      {
        title: '預告統計資料發布時間',
        category: 12,
      },
    ],
  },
  {
    title: '人事園地',
    subNav: [
      {
        title: '員工協助方案專區',
        category: 1,
      },
      {
        title: '人事公告',
        category: 2,
      },
    ],
  },
  {
    title: '會計園地',
    subNav: [
      {
        title: '公務預算',
        category: 1,
      },
      {
        title: '基金預算',
        category: 2,
      },
      {
        title: '公務月報',
        category: 3,
      },
      {
        title: '基金月報',
        category: 4,
      },
    ],
  },
  {
    title: '政風宣導',
    subNav: [
      {
        title: '活動及推廣',
        category: 1,
      },
      {
        title: '網路宣傳',
        category: 2,
      },
    ],
  },
  {
    title: '廉能專區',
    subNav: [
      {
        title: '廉政',
        category: 1,
      },
      {
        title: '一般',
        category: 2,
      },
    ],
  },
  {
    title: '相關連結',
    subNav: [
      {
        title: '市府官網',
        category: 1,
      },
      {
        title: '所屬機關官網',
        category: 2,
      },
      {
        title: '業務主題網站',
        category: 3,
      },
      {
        title: '交通相關網站',
        category: 4,
      },
      {
        title: '其他連結',
        category: 5,
      },
    ],
  },
  {
    title: '性別主流化專區',
    subNav: [
      {
        title: '性別平等工作小組會議',
        category: 1,
      },
      {
        title: '性別主流化',
        category: 2,
      },
      {
        title: '性別統計指標',
        category: 3,
      },
      {
        title: '性別統計分析',
        category: 4,
      },
    ],
  },
];
export default selectData;
