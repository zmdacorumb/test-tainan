export const PTData = [
  {
    Title: '公車動態資訊網',
    Links: '/transportation/public-transport',
  },
  {
    Title: '公車輕旅行',
    // Links: '/transportation/public-transport',
    link: '/transportation/public-transport',
  },
  {
    Title: '公車路線圖',
    Links: '/transportation/public-transport',
  },
  {
    Title: '轉運站資訊',
    Links: '/transportation/public-transport',
  },
  {
    Title: '計程車業者資訊',
    Links: '/transportation/public-transport',
  },
  {
    Title: '捷運優先路網圖',
    Links: '/transportation/public-transport',
  },
];
