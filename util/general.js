// trimming text to the trimCount argument and adds ellipses at the end
export const trimText = (string, trimCount) => {
  return string.length > trimCount
    ? `${string.slice(0, trimCount)}...`
    : string;
};

// formats date
export const formatDate = (date) => {
  return date
    .slice(0, 10)
    .split('-')
    .join('.');
};
