import qs from 'qs';

import api from './api';

/**
 * Standard GET request.
 * @param endpoint - API request endpoint
 */
export const fetchData = async (endpoint) => {
  let response = [];

  try {
    response = await api.get(endpoint);
  } catch (err) {
    console.log('Error caught with GET request:', err);
    return err;
  }

  console.log('Response Data:', response);

  return response.data;
};

/**
 * Standard POST request.
 * @param endpoint - API request endpoint
 * @param payload - JS Object body to POST to server
 */
export const postData = async (endpoint, payload, simple) => {
  // console.log('Post Obj:', payload);
  // console.log('simple', simple);
  let response = [];

  // if backend needs querystring
  const postObj = qs.stringify(payload);
  // UNCOMMENT if backend needs JSON, COMMENT above
  // const postObj = JSON.stringify(payload);

  // console.log('Post Obj after stringify:', postObj);

  try {
    response = await api.post(endpoint, postObj);
  } catch (err) {
    console.log('Error caught with POST request:', err);
    return err;
  }
  if (simple) {
    return response.data;
  }

  console.log('Post Response:', response);
  return response;
};

// 有上傳檔案時
export const postFileData = async (endpoint, payload) => {
  let response = [];
  console.log('payload', payload);
  const postData = new FormData();
  Object.keys(payload).forEach((key) => {
    // if matching value is an array, break it down and add individually

    if (Array.isArray(payload[key]) && typeof payload[key][0] == 'object') {
      payload[key].forEach((item, index) => {
        Object.keys(item).forEach((innerItem, innerIndex) => {
          console.log(
            '1',
            `${key}[${index}].${innerItem}`,
            item[`${innerItem}`]
          );
          postData.append(
            `${key}[${index}].${innerItem}`,
            item[`${innerItem}`]
          );
        });
      });
    } else if (
      Array.isArray(payload[key]) &&
      typeof payload[key][0] !== 'string'
    ) {
      payload[key].forEach((item, index) => {
        console.log('2');
        // append each item key and value dynamically
        postData.append(
          `${key}[${index}].${Object.keys(item)}`,
          item[`${Object.keys(item)}`]
        );
      });
    } else if (key === 'TicketNumber') {
      payload[key].forEach((item, index) => {
        console.log('3');
        // append each item key and value dynamically
        postData.append(`${key}[${index}]`, item);
      });
    } else {
      console.log('4');
      postData.append(key, payload[key]);
    }
  });

  console.log('Post Form Data:', postData);

  try {
    response = await api.post(endpoint, postData);
  } catch (err) {
    console.log('Error caught with POST request:', err);
  }

  // console.log('Response of post file data:', response);

  return response;
};
